﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using WOOPPlusCloud2;
using WOOPPlusCloud2.Models;

[assembly: OwinStartup(typeof(Startup))]

namespace WOOPPlusCloud2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createSystemRoles();
        }
        // In this method we will create default User roles and Admin user for login    
        private void createSystemRoles()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Admin Role and creating a default Admin User     
            if (!roleManager.RoleExists("Admin"))
            {

                // first we create Admin rool    
                IdentityRole role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website                   

                ApplicationUser user = new ApplicationUser();
                user.UserName = "technical@checkupsmed.com";
                user.Email = "technical@checkupsmed.com";

                string userPassword = "Coachesdeh@2472";

                IdentityResult checkUser = UserManager.Create(user, userPassword);

                //Add default User to Role Admin    
                if (checkUser.Succeeded)
                {
                    IdentityResult result = UserManager.AddToRole(user.Id, "Admin");

                }
            }

            // creating Creating Manager role     
            if (!roleManager.RoleExists("Manager"))
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Manager";
                roleManager.Create(role);

            }

            // creating Creating Employee role     
            if (!roleManager.RoleExists("Employee"))
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Employee";
                roleManager.Create(role);

            }
            // creating Creating Clinical role     
            if (!roleManager.RoleExists("Clinical"))
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Clinical";
                roleManager.Create(role);

            }
        }
    }
}