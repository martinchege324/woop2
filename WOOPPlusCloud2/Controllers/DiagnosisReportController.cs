﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    public class DiagnosisReportController : Controller
    {
        // GET: DiagnosisReport
        [Authorize(Roles = "Admin,Clinical,Manager")]
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        //[OutputCache(Duration = 480, VaryByParam = "none")]
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public ActionResult DiagnosisReportView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                DateTime fromDate;
                DateTime toDate;
                DateTime today = DateTime.Now;
                int invoiceFlag;
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    fromDate = new DateTime(today.Year, today.Month, 1);
                    toDate = DateTime.Today;
                }
                else 
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                    
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");

                List<DiagnosisReport_Result> diagnosis = _entities.DiagnosisReport(fromDate, toDate).ToList();
                
                
                if (!String.IsNullOrEmpty(fc["insuranceDropDown"])) 
                {

                    diagnosis = diagnosis.Where(c => c.INSURANCECODE == fc["insuranceDropDown"]).ToList();
                   
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"])) 
                {
                    diagnosis = diagnosis.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                   
                }
                if (!String.IsNullOrEmpty(fc["tagDropDown"]))
                {
                    diagnosis = diagnosis.Where(c => c.TAG == fc["tagDropDown"]).ToList();

                }

                if (!String.IsNullOrEmpty(fc["packageDropDown"]))
                {
                    diagnosis = diagnosis.Where(c => c.benefit_package_desc == fc["packageDropDown"]).ToList();
                  
                }
                if (!String.IsNullOrEmpty(fc["visitsDropDown"]))
                {
                    diagnosis = diagnosis.Where(c => c.VISITTYPE == fc["visitsDropDown"]).ToList();
                    
                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    diagnosis = diagnosis.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["invoiceDropDown"]))
                {
                    if (fc["invoiceDropDown"] == "finalized")
                    {
                        invoiceFlag = 1;
                    }
                    else
                        invoiceFlag = 0;

                    diagnosis = diagnosis.Where(c => c.invoice_closed == invoiceFlag).ToList();
                }


                return View(diagnosis);
                
            }
            
        }

        [OutputCache(Duration = 480, VaryByParam = "none")]
        public async Task<JsonResult> getInsurance()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                return Json(await _entities.payment_accounts.Select(c=>new {c.payment_account_id, c.code, c.payment_account_desc}).OrderBy(c=>c.code).ToListAsync().ConfigureAwait(false), JsonRequestBehavior.AllowGet);
            }

        }

        [OutputCache(Duration = 480, VaryByParam = "none")]
        public async Task<JsonResult> getTag()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                return Json(await _entities.tags.Select(c => new { c.tag_id, c.tag_description}).OrderBy(c => c.tag_description).ToListAsync().ConfigureAwait(false), JsonRequestBehavior.AllowGet);
            }

        }

        [OutputCache(Duration = 480, VaryByParam = "none")]
        public async Task <JsonResult> getScheme(string insurance)
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                if (!String.IsNullOrEmpty(insurance))
                {
                    Guid insuranceID =  _entities.payment_accounts.Where(c=>c.code == insurance).Select(c => c.payment_account_id).FirstOrDefault();
                    return Json(await _entities.patient_payment_account_types.Where(c=>c.payment_account_id == insuranceID).Select(c => new { c.payment_account_id, c.scheme_code, c.patient_payment_account_type_desc }).OrderBy(c => c.scheme_code).ToListAsync().ConfigureAwait(false), JsonRequestBehavior.AllowGet);
                } 
                else 
                {
                    return Json(await _entities.patient_payment_account_types.Select(c => new { c.payment_account_id, c.scheme_code, c.patient_payment_account_type_desc }).OrderBy(c => c.scheme_code).ToListAsync().ConfigureAwait(false), JsonRequestBehavior.AllowGet);
                }

                
            }

        }

        [OutputCache(Duration = 480, VaryByParam = "none")]
        public  async Task<JsonResult> getPackage()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {

                return Json(await _entities.benefits_package.Select(c=>new { c.benefit_package_desc, c.benefit_package_id }).OrderBy(c=>c.benefit_package_desc).ToListAsync().ConfigureAwait(false), JsonRequestBehavior.AllowGet);
            }

        }
        
        [OutputCache(Duration = 480, VaryByParam = "none")]
        public async Task<JsonResult> getVisit()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {

                var visit = await _entities.center_visit_types.Select(c=>new{c.active, c.visit_type_id, c.visit_type_desc}).Where(c=>c.active == 1).OrderBy(c=>c.visit_type_desc).ToListAsync().ConfigureAwait(false);
               
                return Json(visit, JsonRequestBehavior.AllowGet);
            }

        }
    }
}