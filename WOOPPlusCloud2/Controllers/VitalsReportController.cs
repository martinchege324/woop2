﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;
using System.IO;
using ClosedXML.Excel;

namespace WOOPPlusCloud2.Controllers
{

    [Authorize(Roles = "Admin,Clinical,Manager")]
    //[OutputCache(Duration = 480, VaryByParam = "none")]
    [WhitespaceFilter]
    [CompressFilter]
    [ETag]
    public class VitalsReportController : Controller
    {
        // GET: VitalsReport
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult VitalsReportView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels())
            {
                DateTime fromDate;
                DateTime toDate;
                DateTime today = DateTime.Now;
                int invoiceFlag;
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    fromDate = new DateTime(today.Year, today.Month, 1);
                    toDate = DateTime.Today;
                }
                else
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
                List<VitalsReport_Result> vitals = _entities.VitalsReport(fromDate, toDate).ToList();
                if (!String.IsNullOrEmpty(fc["insuranceDropDown"]))
                {
                    vitals = vitals.Where(c => c.INSURANCECODE == fc["insuranceDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    vitals = vitals.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    vitals = vitals.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["packageDropDown"]))
                {
                    vitals = vitals.Where(c => c.benefit_package_desc == fc["packageDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["visitsDropDown"]))
                {
                    vitals = vitals.Where(c => c.VISITTYPE == fc["visitsDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    vitals = vitals.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["invoiceDropDown"]))
                {
                    if (fc["invoiceDropDown"] == "finalized")
                    {
                        invoiceFlag = 1;
                    }
                    else
                        invoiceFlag = 0;

                    vitals = vitals.Where(c => c.invoice_closed == invoiceFlag).ToList();
                }

                return View(vitals);

            }
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult BodyCompositionView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels())
            {
                DateTime fromDate;
                DateTime toDate;
                DateTime today = DateTime.Now;
                int invoiceFlag;
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    fromDate = new DateTime(today.Year, today.Month, 1);
                    toDate = DateTime.Today;
                }
                else
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
                List<VitalsReport_Result> vitals = _entities.VitalsReport(fromDate, toDate).ToList();
                if (!String.IsNullOrEmpty(fc["insuranceDropDown"]))
                {
                    vitals = vitals.Where(c => c.INSURANCECODE == fc["insuranceDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    vitals = vitals.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    vitals = vitals.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["packageDropDown"]))
                {
                    vitals = vitals.Where(c => c.benefit_package_desc == fc["packageDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["visitsDropDown"]))
                {
                    vitals = vitals.Where(c => c.VISITTYPE == fc["visitsDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    vitals = vitals.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["invoiceDropDown"]))
                {
                    if (fc["invoiceDropDown"] == "finalized")
                    {
                        invoiceFlag = 1;
                    }
                    else
                        invoiceFlag = 0;

                    vitals = vitals.Where(c => c.invoice_closed == invoiceFlag).ToList();
                }

                return View(vitals);

            }

        }
        public byte[] ExportToExcel<T>(IEnumerable<T> data, string worksheetTitle, List<string[]> titles)
        {
            XLWorkbook wb = new XLWorkbook(); //create workbook
            IXLWorksheet ws = wb.Worksheets.Add(worksheetTitle); //add worksheet to workbook

            ws.Cell(1, 1).InsertData(titles); //insert titles to first row

            if (data != null && data.Count() > 0)
            {
                //insert data to from second row on
                ws.Cell(2, 1).InsertData(data);
            }

            //save file to memory stream and return it as byte array
            using (MemoryStream ms = new MemoryStream())
            {
                wb.SaveAs(ms);

                return ms.ToArray();
            }
        }
    }
}