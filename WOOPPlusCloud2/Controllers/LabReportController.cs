﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    public class LabReportController : Controller
    {
        // GET: LabReport
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [Authorize(Roles = "Admin,Clinical,Manager")]
        //[OutputCache(Duration = 480, VaryByParam = "none")]
        [WhitespaceFilter]
        [CompressFilter]
        //[ETag]
        public ActionResult LabReportView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                
                //f.assignDate(fc);
                DateTime fromDate;
                DateTime toDate;
          
                int invoiceFlag;
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    toDate = DateTime.Today;
                    fromDate = toDate.AddDays(-7);
                }
                else
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
                List<LabReport_Result> labs = _entities.LabReport(fromDate, toDate).ToList();
                if (!String.IsNullOrEmpty(fc["insuranceDropDown"]))
                {
                    labs = labs.Where(c => c.INSURANCECODE == fc["insuranceDropDown"]).ToList();
                }
             
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    labs = labs.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["packageDropDown"]))
                {
                    labs = labs.Where(c => c.benefit_package_desc == fc["packageDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["visitsDropDown"]))
                {
                    labs = labs.Where(c => c.VISITTYPE == fc["visitsDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    labs = labs.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["invoiceDropDown"]))
                {
                    if (fc["invoiceDropDown"] == "finalized")
                    {
                        invoiceFlag = 1;
                    }
                    else
                        invoiceFlag = 0;

                    labs = labs.Where(c => c.invoice_closed == invoiceFlag).ToList();
                }


                return View(labs);
            }
                
        }
    }
}