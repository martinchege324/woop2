﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ViewModels;
using WOOPPlusCloud2.ActionFilters;

namespace WOOPPlusCloud2.Controllers
{
    [Authorize]
    [CompressFilter]
    //[ETag]
    public class HomeController : Controller
    {
        ProcedureModels entities = new ProcedureModels();
        private decimal? currentRevenue;
        private decimal? targetRevenue;
        private double? TAT;
        private decimal? currentCost;
        private double? currentMargin;


        //declare the delegates for the procedures
        private delegate List<SupplierTransProc_Result> SupplierSPDelegate();
        private delegate List<FinancialProc_Result> FinancialSPDelegate();
        private delegate List<CompleteMovementProc_Result> CompleteMovementSPDelegate();
        private delegate List<DisplayTargets_Result> DisplayTargetsSPDelegate();
        private delegate List<labTATs_Result> labTATsSPDelegate();
        private delegate List<AgingProc_Result> AgingSPDelegate();

        [CompressFilter]
        // declare the synchronous methods
        
        private List<SupplierTransProc_Result> Suppliers()
        {
            using (ProcedureModels entities = new ProcedureModels()) 
            {
                List<SupplierTransProc_Result> suppliers = entities.SupplierTransProc().ToList();
                IEnumerable<SupplierTransProc_Result> data = suppliers.AsEnumerable();
                List<SupplierPaymentsProc_Result> supplierPayments = entities.SupplierPaymentsProc().ToList();
                IEnumerable<SupplierPaymentsProc_Result> paymentData = supplierPayments.AsEnumerable();
                ViewBag.TotalPayable = $"{data.Sum(c => c.total) - paymentData.Sum(c => c.supplier_payment_amount):#,###0}";
                return suppliers;
            }
           
        }
        private List<FinancialProc_Result> Financial()
        {
            using (ProcedureModels entities = new ProcedureModels()) 
            {
                DateTime today = DateTime.Now;
                DateTime fromDate = new DateTime(today.Year, today.Month, 1);
                DateTime toDate = DateTime.Today;
                List<FinancialProc_Result> financials = entities.FinancialProc(fromDate, toDate).ToList();
                IEnumerable<FinancialProc_Result> revenue = financials.AsEnumerable();
                currentRevenue = revenue.Sum(c => c.TOTALSELLINGPRICE);
                currentCost = revenue.Sum(c => c.TOTALCOST);
                ViewBag.Revenue = string.Format("{0:#,###0}", currentRevenue);
                ViewBag.ChartRevenue = currentRevenue;
                ViewBag.Savings = revenue.Sum(c => c.SAVINGS) != 0 && revenue.Sum(c => c.TOTALRETAILPRICE) != 0
                    ? string.Format("{0:#,###0}",
                    revenue.Sum(c => c.SAVINGS) / revenue.Sum(c => c.TOTALRETAILPRICE) * 100)
                    : string.Format("{0:#,###0}", 0);

                ViewBag.Cost = string.Format("{0:#,###0}", currentCost);
                //Previous Month Metrics for comparisons
                DateTime previousDate = fromDate.AddMonths(-1);
                IEnumerable<FinancialProc_Result> previousFinances = entities.FinancialProc(previousDate, fromDate).ToList().AsEnumerable();
                decimal? previousRevenue = previousFinances.Sum(c => c.TOTALSELLINGPRICE);
                decimal? performance = currentRevenue - previousRevenue;
                ViewBag.Performance = performance / previousRevenue * 100;
                //costs comparisons
                decimal? previousCosts = previousFinances.Sum(c => c.TOTALCOST);
                decimal? costPerformance = currentCost - previousCosts;
                ViewBag.CostPerformance = costPerformance / previousCosts * 100;
                //Revenue and Costs List
                DateTime firstDay;
                DateTime nextDay;
                firstDay = DateTime.Now.Month == 1 ? new DateTime(DateTime.Now.Year, 1, 1).AddMonths(-3) : new DateTime(DateTime.Now.Year, 1, 1);

                nextDay = firstDay.AddMonths(1);
                List<FinancialProc_Result> sales = entities.FinancialProc(firstDay, toDate).ToList();
                List<decimal?> selling = new List<decimal?>();
                List<decimal?> costs = new List<decimal?>();
                foreach (FinancialProc_Result item in sales)
                {
                    List<FinancialProc_Result> summary = entities.FinancialProc(firstDay, nextDay).ToList();
                    decimal? totalSales = summary.AsEnumerable().Sum(c => c.TOTALSELLINGPRICE) / 1000000;
                    selling.Add(totalSales);
                    decimal? totalCosts = summary.AsEnumerable().Sum(c => c.TOTALCOST) / 1000000;
                    costs.Add(totalCosts);
                    firstDay = firstDay.AddMonths(1);
                    nextDay = nextDay.AddMonths(1);
                    if (firstDay.Month == DateTime.Today.AddMonths(1).Month)
                        break;
                }

                ViewBag.SalesChart = selling;
                ViewBag.CostsChart = costs;
                return financials;
            }
                
          
        }
        [CompressFilter]
        private List<CompleteMovementProc_Result> Stocks()
        {
            using (ProcedureModels entities = new ProcedureModels()) 
            {
                DateTime today = DateTime.Now;
                DateTime fromDate = new DateTime(today.Year, today.Month, 1);
                DateTime toDate = DateTime.Today;
                DateTime previousDate = fromDate.AddMonths(-1);
                List<CompleteMovementProc_Result> stocks = entities.CompleteMovementProc(fromDate, toDate).ToList();
                IEnumerable<CompleteMovementProc_Result> margin = stocks.AsEnumerable();
                currentMargin = margin.Sum(c => c.MARGIN);
                ViewBag.Margin = $"{margin.Sum(c => c.MARGIN):#,###0}";
                ViewBag.MarginP = $"{margin.Sum(c => c.MARGIN) / margin.Sum(c => c.TOTALCOSTS) * 100:#,###0}";
                //margin comparisons
                List<CompleteMovementProc_Result> previousStocks = entities.CompleteMovementProc(previousDate, fromDate).ToList();
                IEnumerable<CompleteMovementProc_Result> marginDifference = previousStocks.AsEnumerable();
                double? previousMargin = marginDifference.Sum(c => c.MARGIN);
                double? marginPerformance = currentMargin - previousMargin;
                ViewBag.MarginPerformance = marginPerformance / previousMargin * 100;

                return stocks;
            }
               
           
        }
        [CompressFilter]
        private List<DisplayTargets_Result> Targets()
        {
            List<DisplayTargets_Result> targets = entities.DisplayTargets().ToList();
            TAT = targets.Where(c => c.target_id == 2).ToList().AsEnumerable().Sum(c => c.target_value);
            ViewBag.TAT = TAT;
            int? targetCosts = targets.Where(c => c.target_id == 1).ToList().AsEnumerable().Sum(c => c.target_value);
            ViewBag.TargetCosts = targetCosts;
            targetRevenue = targets.Where(c => c.target_id == 3).ToList().AsEnumerable().Sum(c => c.target_value);
            ViewBag.TargetRevenue = targetRevenue;
            return targets;
        }
        [CompressFilter]
        private List<labTATs_Result> labTATs()
        {
            using (
                ProcedureModels entities = new ProcedureModels()) 
            {
                DateTime today = DateTime.Now;
                DateTime fromDate = new DateTime(today.Year, today.Month, 1);
                DateTime toDate = DateTime.Today;
                List<labTATs_Result> labs = entities.labTATs().Where(c => c.date > fromDate).ToList();
                ViewBag.LABTAT = $"{labs.Average(c => c.DateDiff):#,###0}";
                return labs;
            }
           
        }
        [CompressFilter]
        private List<AgingProc_Result> Aging()
        {
            using (ProcedureModels entities = new ProcedureModels()) 
            {
                DateTime today = DateTime.Now;
                DateTime fromDate = new DateTime(today.Year, today.Month, 1);
                DateTime toDate = DateTime.Today;
                DateTime previousDate = fromDate.AddMonths(-1);
                //Aged Receivables 
                List<AgingProc_Result> aging = entities.AgingProc()
                    .Where(c => c.invoice_closed_date >= fromDate && c.invoice_closed_date <= toDate).Where(c => c.payment_account_desc != "WHOLE SALE").Where(c => c.payment_account_desc != "HUB").ToList();
                decimal? currentAging = aging.AsEnumerable().Sum(c => c.running_balance) * -1;
                ViewBag.Aging = $"{currentAging:#,###0}";
                //Aged Receivables Comparisons 
                List<AgingProc_Result> previousAging = entities.AgingProc()
                    .Where(c => c.invoice_closed_date >= previousDate && c.invoice_closed_date <= fromDate).ToList();
                decimal? prevAging = previousAging.AsEnumerable().Sum(c => c.running_balance) * -1;
                decimal? agingPerformance = currentAging - prevAging;
                ViewBag.AgingPerformance = agingPerformance / prevAging * 100;
                return aging;
            }
             
        }
        // wraps the method in a task and returns the task.
        public Task<List<SupplierTransProc_Result>> SuppliersAsync()
        {
            SupplierSPDelegate caller = new SupplierSPDelegate(Suppliers);
            return Task.Factory.FromAsync(caller.BeginInvoke, caller.EndInvoke, null);
        }
        public Task<List<FinancialProc_Result>> FinancialAsync()
        {
            FinancialSPDelegate caller = new FinancialSPDelegate(Financial);
            return Task.Factory.FromAsync(caller.BeginInvoke, caller.EndInvoke, null);
        }
        public Task<List<CompleteMovementProc_Result>> StocksAsync()
        {
            CompleteMovementSPDelegate caller = new CompleteMovementSPDelegate(Stocks);
            return Task.Factory.FromAsync(caller.BeginInvoke, caller.EndInvoke, null);
        }
        public Task<List<DisplayTargets_Result>> TargetsAsync()
        {
            DisplayTargetsSPDelegate caller = new DisplayTargetsSPDelegate(Targets);

            return Task.Factory.FromAsync(caller.BeginInvoke, caller.EndInvoke, null);
        }
        public Task<List<labTATs_Result>> labTATsAsync()
        {
            labTATsSPDelegate caller = new labTATsSPDelegate(labTATs);
            return Task.Factory.FromAsync(caller.BeginInvoke, caller.EndInvoke, null);
        }
        public Task<List<AgingProc_Result>> AgingAsync()
        {
            AgingSPDelegate caller = new AgingSPDelegate(Aging);
            return Task.Factory.FromAsync(caller.BeginInvoke, caller.EndInvoke, null);
        }
        //Home Action Method
       
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        
        [WhitespaceFilter]
        [CompressFilter]
        //[ETag]
        [OutputCache(Duration = int.MaxValue, VaryByParam = "none")]
        public async Task<ActionResult> _Index() 
        {

            DateTime today = DateTime.Now;
            DateTime fromDate = new DateTime(today.Year, today.Month, 1);
            DateTime toDate = DateTime.Today;
            //Total Payables
            Task<List<SupplierTransProc_Result>> suppliers = SuppliersAsync();
            //Aging
            Task<List<AgingProc_Result>> aging = AgingAsync();
            //Total Revenue & Savings 
            Task<List<FinancialProc_Result>> financials = FinancialAsync();
            //Stocks
            //Task<List<CompleteMovementProc_Result>> stocks = StocksAsync();
            //Targets Set by the User
            Task<List<DisplayTargets_Result>> targets = TargetsAsync();
            //Lab Targets
            Task<List<labTATs_Result>> labs = labTATsAsync();
            //Parallel calls
            await suppliers.ConfigureAwait(false);
            await aging.ConfigureAwait(false);
            List<FinancialProc_Result> finance = await financials.ConfigureAwait(false);
            //List<CompleteMovementProc_Result> stock = await stocks;
            List<DisplayTargets_Result> target = await targets.ConfigureAwait(false);
            List<labTATs_Result> lab = await labs.ConfigureAwait(false);


            //Progress Bar on Home Page

            IEnumerable<FinancialProc_Result> revenue = finance.AsEnumerable();
            ViewBag.CostCSS = revenue.Sum(c => c.SAVINGS) != 0 && revenue.Sum(c => c.TOTALRETAILPRICE) != 0
                ? revenue.Sum(c => c.SAVINGS) / revenue.Sum(c => c.TOTALRETAILPRICE) * 100 /
                             target.Where(c => c.target_id == 1).AsEnumerable().Sum(c => c.target_value) * 100
                : 0;
            ViewBag.RevenueCSS = revenue.Sum(c => c.TOTALSELLINGPRICE) / targetRevenue * 100;
            ViewBag.TATCSS = TAT / lab.Average(c => c.DateDiff) * 100;
            //Get last three months if month is January
            IEnumerable<string> lastThreeMonths = Enumerable.Range(0, 3)
                              .Select(i => DateTime.Now.AddMonths(i - 3))
                              .Select(date => date.ToString("MMMM"));

            //Total Costs, Revenue and Time
            string[] monthNames = new CultureInfo("en-US").DateTimeFormat.MonthNames;
            List<string> list = new List<string>();
            if (today.Month == 1)
            {
                list.AddRange(lastThreeMonths);
            }

            foreach (string month in monthNames)

                list.Add(month);

            ViewBag.Month = list;

            //List<CompleteMovementProc_Result> mostDrugs = stock.OrderByDescending(c => c.DISPENSED).Take(5).ToList();
            //Stocks
            Task<List<CompleteMovementProc_Result>> stocks = StocksAsync();
            List<CompleteMovementProc_Result> stock = await stocks.ConfigureAwait(false);
            List<CompleteMovementProc_Result> mostDrugs = stock.OrderByDescending(c => c.DISPENSED).Take(5).ToList();
            return PartialView("_Index", mostDrugs);
        }
        
        /// <summary>
        ///     Gets various usage statistics.
        ///     These statistics include: total sms sent, sms balance, sms delivery rate, cluster turnover and most recent
        ///     clustered patient.
        /// </summary>
        /// <returns>JSON payload containing all the statistics</returns>
        /// 
        [CompressFilter]
        //[ETag]
        public ActionResult Stats()
        {
            statsModel s;

            DateTime yesterday = DateTime.Today.AddDays(-7);
            DateTime twoDaysAgo = DateTime.Today.AddDays(-14);
            DateTime fourDaysAgo = DateTime.Today.AddDays(-21);
            DateTime sixDaysAgo = DateTime.Today.AddDays(-28);


            using (ZiDiModels entities = new ZiDiModels())
            {
                IQueryable<VisitViewModel> visits = from v in entities.visit_cycle
                                                    join pr in entities.patient_registration on v.patient_id equals pr.patient_id
                                                    join c in entities.center_visit_types on v.visit_type equals c.visit_type_id
                                                    select new VisitViewModel { VisitCycles = v, PatientRegistrations = pr, CenterVisitType = c };


                long cluster1Total;
                long cluster2Total;
                long cluster3Total;
                long cluster4Total;


                //Calculate the different cluster stats for today, yesterday, 2 days ago and 4 days ago.

                cluster1Total = visits.Where(x => x.VisitCycles.cycle_created_time >= yesterday)
                    .Where(x => x.VisitCycles.concierge == 1).Count();
                cluster2Total = visits.Where(x => x.VisitCycles.cycle_created_time >= yesterday)
                    .Where(x => x.VisitCycles.concierge == 4).Count();
                cluster3Total = visits.Where(x => x.VisitCycles.cycle_created_time >= yesterday)
                    .Where(x => x.VisitCycles.concierge == 2).Count();
                cluster4Total = visits.Where(x => x.VisitCycles.cycle_created_time >= yesterday)
                    .Where(x => x.VisitCycles.concierge == 3).Count();
                clusterStats cs2 = new clusterStats("Week 4", cluster1Total, cluster2Total, cluster3Total, cluster4Total);


                cluster1Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= twoDaysAgo &&
                                x.VisitCycles.cycle_created_time < yesterday).Where(x => x.VisitCycles.concierge == 1)
                    .Count();
                cluster2Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= twoDaysAgo &&
                                x.VisitCycles.cycle_created_time < yesterday).Where(x => x.VisitCycles.concierge == 4)
                    .Count();
                cluster3Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= twoDaysAgo &&
                                x.VisitCycles.cycle_created_time < yesterday).Where(x => x.VisitCycles.concierge == 2)
                    .Count();
                cluster4Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= twoDaysAgo &&
                                x.VisitCycles.cycle_created_time < yesterday).Where(x => x.VisitCycles.concierge == 3)
                    .Count();
                clusterStats cs3 = new clusterStats("Week 3", cluster1Total, cluster2Total, cluster3Total, cluster4Total);

                cluster1Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= fourDaysAgo &&
                                x.VisitCycles.cycle_created_time < twoDaysAgo).Where(x => x.VisitCycles.concierge == 1)
                    .Count();
                cluster2Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= fourDaysAgo &&
                                x.VisitCycles.cycle_created_time < twoDaysAgo).Where(x => x.VisitCycles.concierge == 4)
                    .Count();
                cluster3Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= fourDaysAgo &&
                                x.VisitCycles.cycle_created_time < twoDaysAgo).Where(x => x.VisitCycles.concierge == 2)
                    .Count();
                cluster4Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= fourDaysAgo &&
                                x.VisitCycles.cycle_created_time < twoDaysAgo).Where(x => x.VisitCycles.concierge == 3)
                    .Count();
                clusterStats cs4 = new clusterStats("Week 2", cluster1Total, cluster2Total, cluster3Total, cluster4Total);

                cluster1Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= sixDaysAgo &&
                                x.VisitCycles.cycle_created_time < fourDaysAgo).Where(x => x.VisitCycles.concierge == 1)
                    .Count();
                cluster2Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= sixDaysAgo &&
                                x.VisitCycles.cycle_created_time < fourDaysAgo).Where(x => x.VisitCycles.concierge == 4)
                    .Count();
                cluster3Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= sixDaysAgo &&
                                x.VisitCycles.cycle_created_time < fourDaysAgo).Where(x => x.VisitCycles.concierge == 2)
                    .Count();
                cluster4Total = visits
                    .Where(x => x.VisitCycles.cycle_created_time >= sixDaysAgo &&
                                x.VisitCycles.cycle_created_time < fourDaysAgo).Where(x => x.VisitCycles.concierge == 3)
                    .Count();
                clusterStats cs5 = new clusterStats("Week 1", cluster1Total, cluster2Total, cluster3Total, cluster4Total);

                List<clusterStats> clusterStatsList = new List<clusterStats> { cs5, cs4, cs3, cs2 };
                long finalCluster1Total = cluster1Total;
                long finalCluster2Total = cluster2Total;
                long finalCluster3Total = cluster3Total;
                long finalCluster4Total = cluster4Total;

                s = new statsModel(clusterStatsList,
                    finalCluster1Total, finalCluster2Total, finalCluster3Total, finalCluster4Total);
            }


            return Json(s, JsonRequestBehavior.AllowGet);
        }
    }

    /// <summary>
    ///     Model for the statistics required by the dashboard page.
    /// </summary>
    public class statsModel
    {
        public List<clusterStats> clusterStatsList;

        public statsModel(List<clusterStats> cs, long fc1, long fc2, long fc3, long fc4)
        {
            clusterStatsList = cs;
            finalCluster1Total = fc1;
            finalCluster2Total = fc2;
            finalCluster3Total = fc3;
            finalCluster4Total = fc4;
        }

        public long finalCluster1Total { get; set; }
        public long finalCluster2Total { get; set; }
        public long finalCluster3Total { get; set; }
        public long finalCluster4Total { get; set; }
    }

    /// <summary>
    ///     Model for cluster statistics for the bar chart for cluster turnover.
    /// </summary>
    /// 
    [CompressFilter]
    public class clusterStats
    {
        //private ZiDiModels entities = new ZiDiModels();

        public clusterStats(string p, float c1, float c2, float c3, float c4)
        {
            periodLabel = p;
            cluster1 = c1;
            cluster2 = c2;
            cluster3 = c3;
            cluster4 = c4;
        }

        public string periodLabel { get; set; }

        public float cluster1 { get; set; }

        public float cluster2 { get; set; }

        public float cluster3 { get; set; }
        public float cluster4 { get; set; }

    }
}