﻿using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ActionFilters;
namespace WOOPPlusCloud2.Controllers
{
    [Authorize]
    [OutputCache(Duration = 480, VaryByParam = "none")]

    public class MorbidityController : Controller
    {
        // GET: Morbidity
        private readonly ProcedureModels _entities = new ProcedureModels();
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public ActionResult MorbidityView()
        {
            //DateTime time = DateTime.Today.AddMonths(-1).Date;
            System.Collections.Generic.IEnumerable<MorbidityProc_Result> morbidity = _entities.MorbidityProc().ToList().Take(100);
            return View(morbidity);
        }
    }
}