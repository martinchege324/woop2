﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;

using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    [Authorize(Roles = "Admin,Clinical,Manager")]

    [WhitespaceFilter]
    [CompressFilter]
    //[ETag]
    public class PricesController : Controller
    {
        //Date Variables
        DateTime fromDate;
        DateTime toDate;
        DateTime today = DateTime.Now;
        
        //function to return lab prices
        private List<LabPrices_Result> LabList(FormCollection form) 
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                if (String.IsNullOrEmpty(form["fromDate"]) && String.IsNullOrEmpty(form["toDate"]))
                {
                    toDate = DateTime.Today;
                    fromDate = toDate.AddYears(-1);
                }
                else
                {
                    fromDate = DateTime.Parse(form["fromDate"]).Date;
                    toDate = DateTime.Parse(form["toDate"]).Date;
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");

                List<LabPrices_Result> labPrices = _entities.LabPrices(fromDate, toDate).OrderBy(c=>c.SAMPLE).ToList();
                if (!String.IsNullOrEmpty(form["activeDropDown"]))
                {
                    labPrices = labPrices.Where(c => c.Active == int.Parse(form["activeDropDown"])).ToList();
                }
                if (!String.IsNullOrEmpty(form["branchDropDown"]))
                {
                    labPrices = labPrices.Where(c => c.CENTER == form["branchDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(form["sampleDropDown"]))
                {
                    labPrices = labPrices.Where(c => c.SAMPLE == form["sampleDropDown"]).ToList();
                }

                return labPrices;
            }
        }

        //get sample dropdown lists

        public JsonResult getSamples() 
        {
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                toDate = DateTime.Today;
                fromDate = toDate.AddYears(-1);
                var samples = _entities.LabPrices(fromDate, toDate).Select(c=>new {c.SAMPLE, c.Active}).GroupBy(c=>c.SAMPLE).Select(c=>c.First()).OrderBy(c=>c.SAMPLE).ToList();
                return Json(samples, JsonRequestBehavior.AllowGet);
            }
               
        }

        public JsonResult getServices()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                toDate = DateTime.Today;
                fromDate = toDate.AddYears(-1);
                var services = _entities.ServicePrices(fromDate, toDate).Select(c => new { c.CATEGORY, c.Active }).GroupBy(c => c.CATEGORY).Select(c => c.First()).OrderBy(c => c.CATEGORY).ToList();
                return Json(services, JsonRequestBehavior.AllowGet);
            }

        }
        //function to return service prices
        private List<ServicePrices_Result> ServiceList(FormCollection form)
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                if (String.IsNullOrEmpty(form["fromDate"]) && String.IsNullOrEmpty(form["toDate"]))
                {
                    toDate = DateTime.Today;
                    fromDate = toDate.AddYears(-1);
                }
                else
                {
                    fromDate = DateTime.Parse(form["fromDate"]).Date;
                    toDate = DateTime.Parse(form["toDate"]).Date;
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
                List<ServicePrices_Result> servicePrices = _entities.ServicePrices(fromDate, toDate).OrderBy(c=>c.CATEGORY).ToList();
                if (!String.IsNullOrEmpty(form["activeDropDown"]))
                {
                    servicePrices = servicePrices.Where(c => c.Active == int.Parse(form["activeDropDown"])).ToList();
                }
                if (!String.IsNullOrEmpty(form["branchDropDown"]))
                {
                    servicePrices = servicePrices.Where(c => c.CENTER == form["branchDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(form["serviceDropDown"]))
                {
                    servicePrices = servicePrices.Where(c => c.CATEGORY == form["serviceDropDown"]).ToList();
                }
                return servicePrices;
            }
        }

        //function to return drug prices
        private List<DrugPrices_Result> DrugList(FormCollection form)
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                List<DrugPrices_Result> drugPrices = new List<DrugPrices_Result>();
                if (!String.IsNullOrEmpty(form["activeDropDown"]))
                {
                    drugPrices = _entities.DrugPrices().Where(c => c.Active == int.Parse(form["activeDropDown"])).ToList();
                }
                else 
                {
                    drugPrices = _entities.DrugPrices().Where(c=>c.Active == 1).ToList();
                }
               
               
                if (!String.IsNullOrEmpty(form["branchDropDown"]))
                {
                    drugPrices = drugPrices.Where(c => c.CENTER == form["branchDropDown"]).ToList();
                }
               
                return drugPrices;
            }
        }


        // GET: Prices
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult LabPrices(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            List<LabPrices_Result> userView = LabList(fc);
            if (!String.IsNullOrEmpty(fc["groupDropDown"]))
            {
                if (int.Parse(fc["groupDropDown"]) == 0) 
                {
                    return View("LabPricesUngrouped",userView);
                }
                
            }

            return View(userView);
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult DrugPrices(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            List<DrugPrices_Result> userView = DrugList(fc);
            return View(userView);
        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult ServicePrices(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            List<ServicePrices_Result> userView = ServiceList(fc);
            if (!String.IsNullOrEmpty(fc["groupDropDown"]))
            {
                if (int.Parse(fc["groupDropDown"]) == 0)
                {
                    return View("ServicePricesUngrouped", userView);
                }

            }
            return View(userView);
        }
    }
}