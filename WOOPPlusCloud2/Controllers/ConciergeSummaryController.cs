﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ViewModels;
using WOOPPlusCloud2.ActionFilters;

namespace WOOPPlusCloud2.Controllers
{
    [Authorize]
    [OutputCache(Duration = 480, VaryByParam = "none")]
    public class ConciergeSummaryController : Controller
    {

        private delegate List<ConciergeOrders_Result> ConciergeOrdersSPDelegate();
        private delegate List<Reminders_Result> RemindersSPDelegate();


        private List<ConciergeOrders_Result> Concierge()
        {
            using (ProcedureModels _entities = new ProcedureModels()) 
            {

                //Concierge Summary
                List<ConciergeOrders_Result> list = new List<ConciergeOrders_Result>();
                List<ConciergeOrders_Result> dispatchesTATs = _entities.ConciergeOrders().Where(c => DateTime.Parse(c.OrderDate.ToString()).Date > DateTime.Today.Date.AddDays(-7)).ToList();
                List<ConciergeOrders_Result> dispatches = dispatchesTATs.GroupBy(c => c.deposit_id).Select(c => c.First()).OrderBy(c => c.OrderDate).ToList();
                List<ConciergeOrders_Result> groupedDispatches = dispatches.GroupBy(c => DateTime.Parse(c.OrderDate.ToString()).Date).Select(c => c.First()).ToList();

                foreach (ConciergeOrders_Result item in groupedDispatches)
                {
                    //concierge list
                    ConciergeOrders_Result newItem = new ConciergeOrders_Result
                    {
                        OrderDate = item.OrderDate
                    };
                    List<ConciergeOrders_Result> filtereddispatches = dispatchesTATs.Where(c => c.deposit_id == item.deposit_id).ToList();
                    List<ConciergeOrders_Result> filtereddispatchTATs = dispatches.Where(c => c.OrderDate.Value.Date == item.OrderDate.Value.Date).ToList();
                    List<ConciergeOrders_Result> filteredTotals = dispatchesTATs.Where(c => c.OrderDate.Value.Date == item.OrderDate.Value.Date).ToList();
                    newItem.QuotationsConfirmedTAT = Convert.ToInt32(filtereddispatches.Average(c => c.QuotationsConfirmedTAT));
                    newItem.ConfirmedClosedTAT = Convert.ToInt32(filtereddispatches.Average(c => c.ConfirmedClosedTAT));
                    newItem.ClosedDispatchTAT = Convert.ToInt32(filtereddispatches.Average(c => c.ClosedDispatchTAT));
                    newItem.DispatchDeliveryTAT = Convert.ToInt32(filtereddispatches.Average(c => c.DispatchDeliveryTAT));
                    newItem.DeliveryFollowUpTAT = Convert.ToInt32(filtereddispatches.Average(c => c.DeliveryFollowUpTAT));


                    newItem.Confirmed = filtereddispatchTATs.Where(c => c.Confirmed == 1).Count();
                    newItem.quotations = filtereddispatchTATs.Where(c => c.OrderDate.HasValue).Count();
                    newItem.Closed = filtereddispatchTATs.Where(c => c.Closed == 1).Count();
                    newItem.DrugDelivered = filtereddispatchTATs.Where(c => c.DrugDelivered == 1).Count();
                    newItem.Dispatched = filtereddispatchTATs.Where(c => c.Dispatched == 1).Count();
                    newItem.followup_done = filtereddispatchTATs.Where(c => c.followup_done == 1).Count();
                    newItem.drug_refill = filtereddispatchTATs.Where(c => c.drug_refill == 1).Count();
                    newItem.total_charge = filteredTotals.Sum(c => c.total_charge);

                    list.Add(newItem);

                }

                return list;

            }

        }


        // GET: ConciergeSummary
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public ActionResult ConciergeIndex(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            DateTime fromDate;
            DateTime toDate;

            List<ConciergeOrders_Result> list = new List<ConciergeOrders_Result>();
            if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
            {
                list = Concierge();
                fromDate = DateTime.Today.Date.AddDays(-7);
                toDate = DateTime.Now;
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
            }
            else
            {
                fromDate = DateTime.Parse(fc["fromDate"]).Date;
                toDate = DateTime.Parse(fc["toDate"]).Date;
                using (ProcedureModels _entities = new ProcedureModels())
                {
                    ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                    ViewBag.toDate = toDate.ToString("dd-MM-yyyy");

                    List<ConciergeOrders_Result> dispatchesTATs = _entities.ConciergeOrders().Where(c => DateTime.Parse(c.OrderDate.ToString()).Date >= DateTime.Parse(fromDate.ToString()).Date && DateTime.Parse(c.OrderDate.ToString()).Date <= DateTime.Parse(toDate.ToString()).Date).ToList();
                    List<ConciergeOrders_Result> dispatches = dispatchesTATs.GroupBy(c => c.deposit_id).Select(c => c.First()).OrderBy(c => c.OrderDate).ToList();
                    List<ConciergeOrders_Result> groupedDispatches = dispatches.GroupBy(c => DateTime.Parse(c.OrderDate.ToString()).Date).Select(c => c.First()).ToList();
                    foreach (ConciergeOrders_Result item in groupedDispatches)
                    {
                        //concierge list
                        ConciergeOrders_Result newItem = new ConciergeOrders_Result
                        {
                            OrderDate = item.OrderDate
                        };
                        List<ConciergeOrders_Result> filtereddispatches = dispatchesTATs.Where(c => c.deposit_id == item.deposit_id).ToList();
                        List<ConciergeOrders_Result> filteredTotals = dispatchesTATs.Where(c => c.OrderDate.Value.Date == item.OrderDate.Value.Date).ToList();
                        List<ConciergeOrders_Result> filtereddispatchTATs = dispatches.Where(c => c.OrderDate.Value.Date == item.OrderDate.Value.Date).ToList();
                        newItem.QuotationsConfirmedTAT = Convert.ToInt32(filtereddispatches.Average(c => c.QuotationsConfirmedTAT));
                        newItem.ConfirmedClosedTAT = Convert.ToInt32(filtereddispatches.Average(c => c.ConfirmedClosedTAT)); newItem.ConfirmedDispatchTAT = item.ConfirmedDispatchTAT;
                        newItem.DispatchDeliveryTAT = Convert.ToInt32(filtereddispatches.Average(c => c.DispatchDeliveryTAT));
                        newItem.DeliveryFollowUpTAT = Convert.ToInt32(filtereddispatches.Average(c => c.DeliveryFollowUpTAT));
                        newItem.Confirmed = filtereddispatchTATs.Where(c => c.Confirmed == 1).Count();
                        newItem.quotations = filtereddispatchTATs.Where(c => c.OrderDate.HasValue).Count();
                        newItem.Closed = filtereddispatchTATs.Where(c => c.Closed == 1).Count();
                        newItem.DrugDelivered = filtereddispatchTATs.Where(c => c.DrugDelivered == 1).Count();
                        newItem.Dispatched = filtereddispatchTATs.Where(c => c.Dispatched == 1).Count();
                        newItem.followup_done = filtereddispatchTATs.Where(c => c.followup_done == 1).Count();
                        newItem.drug_refill = filtereddispatchTATs.Where(c => c.drug_refill == 1).Count();
                        newItem.total_charge = filteredTotals.Sum(c => c.total_charge);
                        list.Add(newItem);


                    }
                }
            }

            ConciergeViewModel index = new ConciergeViewModel
            {
                ConciergeOrders = list
            };
            return View(index);
        }


     

    }
}