﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ViewModels;

namespace WOOPPlusCloud2.Controllers
{
    public class PatientsReportController : Controller
    {
        // GET: PatientsReport
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [Authorize(Roles = "Admin,Clinical,Manager")]
        //[OutputCache(Duration = 480, VaryByParam = "none")]
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public ActionResult PatientReportView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels())
            {

                DateTime fromDate;
                DateTime toDate;
                DateTime today = DateTime.Now;
                int invoiceFlag;
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    fromDate = new DateTime(today.Year, today.Month, 1);
                    toDate = DateTime.Today; ;
                }
                else
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");

                List<PatientsReport_Result> patients = _entities.PatientsReport(fromDate, toDate).ToList();
                if (!String.IsNullOrEmpty(fc["insuranceDropDown"]))
                {
                    patients = patients.Where(c => c.INSURANCECODE == fc["insuranceDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    patients = patients.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    patients = patients.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["packageDropDown"]))
                {
                    patients = patients.Where(c => c.Package == fc["packageDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["visitsDropDown"]))
                {
                    patients = patients.Where(c => c.VISITTYPE == fc["visitsDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    patients = patients.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["invoiceDropDown"]))
                {
                    if (fc["invoiceDropDown"] == "finalized")
                    {
                        invoiceFlag = 1;
                    }
                    else
                        invoiceFlag = 0;

                    patients = patients.Where(c => c.invoice_closed == invoiceFlag).ToList();
                }

                return View(patients);
            }

        }


        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [Authorize(Roles = "Admin,Clinical,Manager")]
       
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]

        public ActionResult _VisitSummaries(string cycle)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels())
            {
                List<PatientVitalsProc_Result> patientVitals = _entities.PatientVitalsProc(cycle).ToList();
                List<PatientSigns_Result> patientSigns = _entities.PatientSigns(cycle).ToList();
                List<PatientSymptoms_Result> patientSymptoms = _entities.PatientSymptoms(cycle).ToList();
                List<PatientNotes_Result> patientNotes = _entities.PatientNotes(cycle).ToList();
                List<PatientDiagnosis_Result> patientDiagnosis = _entities.PatientDiagnosis(cycle).ToList();
                List<PatientDrugs_Result> patientDrugs = _entities.PatientDrugs(cycle).ToList();
                List<PatientDrugs_Result> list = new List<PatientDrugs_Result>();
                foreach (PatientDrugs_Result item in patientDrugs)
                {
                    PatientDrugs_Result newItem = new PatientDrugs_Result
                    {
                        center_product_description = item.center_product_description,
                        cycle_id = item.cycle_id,
                        product_code = item.product_code,
                        unit_price = item.unit_price,
                        trans_quantity = item.trans_quantity,
                        savings = item.savings,
                        vatvalue = item.vatvalue,
                        trans_date = item.trans_date,
                        regular_retail_price = item.regular_retail_price

                };
                    newItem.total = Convert.ToDecimal(newItem.trans_quantity) * newItem.unit_price;
                    
                    list.Add(newItem);
                }
                VisitSummaryModel view = new VisitSummaryModel
                {

                    PatientVitals = patientVitals,
                    PatientNotes = patientNotes,
                    PatientDiagnosis = patientDiagnosis,
                    PatientSigns = patientSigns,
                    PatientSymptoms = patientSymptoms,
                    PatientTreatment = list
                };
                return PartialView("_VisitSummaries", view);
            }



        }
    }
}