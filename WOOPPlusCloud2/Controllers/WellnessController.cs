﻿using System.Web.Mvc;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    public class WellnessController : Controller
    {
        // GET: Wellness
        public ActionResult Index()
        {
            return View();
        }

        // GET: Wellness/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Wellness/Create
        public ActionResult CreateWellness()
        {
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                return View(); 
            }

              
        }

        // POST: Wellness/Create
        [HttpPost]
        public ActionResult CreateWellness(FormCollection collection)
        {
            try
            {
                using (ProcedureModels _entities = new ProcedureModels()) 
                {
                    //_entities.insertWellness();
                    return RedirectToAction("Index");
                }

                   
            }
            catch
            {
                return View();
            }
        }

        // GET: Wellness/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Wellness/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Wellness/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Wellness/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
