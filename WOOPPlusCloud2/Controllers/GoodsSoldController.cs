﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    public class GoodsSoldController : Controller
    {
        // GET: GoodsSold
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [OutputCache(Duration = 480, VaryByParam = "none")]
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public ActionResult GoodsSoldView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                DateTime fromDate;
                DateTime toDate;
                DateTime today = DateTime.Now;
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    toDate = DateTime.Now;
                    fromDate = new DateTime(today.Year, today.Month, 1);

                }
                else
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                    
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
                List<GoodsSold_Result> goodsSold = _entities.GoodsSold(fromDate, toDate).ToList();
                IEnumerable<GoodsSold_Result> data = goodsSold.AsEnumerable();

                /*Summary of Total Sales and Costs*/
                ViewBag.TotalSales = $"{data.Sum(x => x.TotalSold):#,###0}";
                ViewBag.TotalCosts = $"{data.Sum(x => x.TotalCost):#,###0}";
                ViewBag.Margin = $"{data.Sum(x => x.Profit):#,###0}";

                List<GoodsSold_Result> list = new List<GoodsSold_Result>();
                foreach (GoodsSold_Result item in goodsSold) 
                {
                    GoodsSold_Result goodsItem = new GoodsSold_Result
                    {
                        product_code = item.product_code,
                        DRUG = item.DRUG,
                        Quantity = item.Quantity,
                        cost_price = item.cost_price,
                        TotalSold = item.TotalSold
                    };
                    goodsItem.UnitSelling = goodsItem.TotalSold / Convert.ToDecimal(goodsItem.Quantity);
                    goodsItem.TotalCost = goodsItem.cost_price * Convert.ToDecimal(goodsItem.Quantity);
                    goodsItem.Profit = goodsItem.TotalSold - goodsItem.TotalCost;
                    list.Add(goodsItem);
                }
                
                return View(list);
            }
        }
    }
}