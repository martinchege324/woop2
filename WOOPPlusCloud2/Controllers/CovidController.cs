﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    public class CovidController : Controller
    {
        //function to retrieve covid data 
        ProcedureModels entities = new ProcedureModels();
        private List<CovidReportproc_Result> GetCovidData(DateTime startDate, DateTime endDate, FormCollection collection) 
        {

            List <CovidReportproc_Result> covidResults = entities.CovidReportproc(startDate, endDate).ToList();
            if (!String.IsNullOrEmpty(collection["insuranceDropDown"]))
            {
                covidResults = covidResults.Where(c => c.INSURANCECODE == collection["insuranceDropDown"]).ToList();
            }

            if (!String.IsNullOrEmpty(collection["schemeDropDown"]))
            {
                covidResults = covidResults.Where(c => c.SCHEMECODE == collection["schemeDropDown"]).ToList();
            }

            if (!String.IsNullOrEmpty(collection["visitsDropDown"]))
            {
                covidResults = covidResults.Where(c => c.VISITTYPE == collection["visitsDropDown"]).ToList();
            }
            if (!String.IsNullOrEmpty(collection["branchDropDown"]))
            {
                covidResults = covidResults.Where(c => c.CENTER == collection["branchDropDown"]).ToList();
            }
            return covidResults;
        }
        // GET: Covid
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [Authorize]
        [WhitespaceFilter]
        [CompressFilter]
       
        public ActionResult CovidView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            DateTime fromDate;
            DateTime toDate;


            if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
            {
                
                fromDate = new DateTime(DateTime.Now.Year,DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00);
                toDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23,59 , 59);
            }
            else
            {
                fromDate = DateTime.Parse(fc["fromDate"]).Date;
                toDate = DateTime.Parse(fc["toDate"]).Date;
            }
            ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
            ViewBag.toDate = toDate.ToString("dd-MM-yyyy");


            List<CovidReportproc_Result> covidList = GetCovidData(fromDate, toDate, fc);


            ViewBag.Patients = covidList.Count;
            ViewBag.Paid = covidList.Where(c => c.PaymentStatus == "Paid").Count();
            ViewBag.NotPaid = covidList.Where(c => c.PaymentStatus == "Not Paid").Count();
            ViewBag.Performed = covidList.Where(c => c.ResultsEnteredTime.HasValue == true).Count();
            ViewBag.NotPerformed = covidList.Where(c => c.ResultsEnteredTime.HasValue == false).Count();
            ViewBag.Positive = covidList.Where(c => c.RESULT == "POSITIVE").Count();
            ViewBag.Negative = covidList.Where(c => c.RESULT == "NEGATIVE").Count();


            return View(covidList);
         

        }
        // GET: Covid
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [Authorize]
        [WhitespaceFilter]
        [CompressFilter]
       
        public ActionResult CovidTATView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }


            //f.assignDate(fc);
            DateTime fromDate;
            DateTime toDate;


            if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
            {
                toDate = DateTime.Today;
                fromDate = toDate.AddDays(-1);
            }
            else
            {
                fromDate = DateTime.Parse(fc["fromDate"]).Date;
                toDate = DateTime.Parse(fc["toDate"]).Date;
            }
            ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
            ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
            List<CovidReportproc_Result> covidTATList = GetCovidData(fromDate, toDate, fc).OrderBy(c => c.VisitDate).ToList();
            List<CovidReportproc_Result> groupedTATList = covidTATList.GroupBy(c => DateTime.Parse(c.VisitDate).Date).Select(c => c.FirstOrDefault()).ToList();
            List<CovidReportproc_Result> filteredCovidList = new List<CovidReportproc_Result>();
            foreach (CovidReportproc_Result item in groupedTATList) 
            {
                CovidReportproc_Result resultItem = new CovidReportproc_Result();

                resultItem.VisitDate = item.VisitDate;
                List<CovidReportproc_Result> filter = covidTATList.Where(c => DateTime.Parse(c.VisitDate).Date == DateTime.Parse(item.VisitDate).Date).ToList();
                resultItem.RequestSampleCollection = filter.Average(c=>c.RequestSampleCollectionTAT);
                resultItem.SampleCollectionResults = filter.Average(c=>c.SampleCollectionResultsTAT);
                resultItem.ResultsToRelease = filter.Average(c => c.ResultsToReleaseTAT);
                resultItem.VisitCount = filter.Count;
                resultItem.PaidCount = filter.Where(c=>c.PaymentStatus=="Paid").Count();
                resultItem.NotPaidCount = filter.Where(c => c.PaymentStatus == "Not Paid").Count();
                resultItem.InsuranceCount = filter.Where(c => !String.IsNullOrEmpty(c.INSURANCECODE)).Count();
                resultItem.CashCount = filter.Where(c => String.IsNullOrEmpty(c.INSURANCECODE)).Count();
               
                filteredCovidList.Add(resultItem);
            }
            return View(filteredCovidList);
  

        }
    }
}
