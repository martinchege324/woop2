﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    public class UrinalysisController : Controller
    {
        // GET: Urinalysis
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [Authorize(Roles = "Admin,Clinical,Manager")]
        //[OutputCache(Duration = 480, VaryByParam = "none")]
        [WhitespaceFilter]
        [CompressFilter]
        //[ETag]
        public ActionResult UrinalysisView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels())
            {
                DateTime fromDate;
                DateTime toDate;
                
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    toDate = DateTime.Today;
                    fromDate = toDate.AddDays(-7);
                }
                else
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
                List<Urinalysis_Result> urinalysis = _entities.Urinalysis(fromDate, toDate).ToList();
                if (!String.IsNullOrEmpty(fc["insuranceDropDown"]))
                {
                    urinalysis = urinalysis.Where(c => c.INSURANCECODE == fc["insuranceDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    urinalysis = urinalysis.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    urinalysis = urinalysis.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();
                }
               
                if (!String.IsNullOrEmpty(fc["visitsDropDown"]))
                {
                    urinalysis = urinalysis.Where(c => c.VISITTYPE == fc["visitsDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    urinalysis = urinalysis.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }

                return View(urinalysis);
            }
        }
    }
}