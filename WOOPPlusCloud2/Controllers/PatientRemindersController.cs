﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    public class PatientRemindersController : Controller
    {
        // GET: PatientReminders
        //returns list of reminder types to the UI
        [OutputCache(Duration = 480, VaryByParam = "none")]
        public async Task<JsonResult> GetReminderTypes()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                return Json(await _entities.patient_reminder_types.Select(c => new { c.patient_reminder_type_id, c.reminder_type_description }).OrderBy(c => c.reminder_type_description).ToListAsync().ConfigureAwait(false), JsonRequestBehavior.AllowGet);
            }

        }
        //returns list of doctors 
        [OutputCache(Duration = 480, VaryByParam = "none")]
        public async Task<JsonResult> GetDoctors()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                return Json(await _entities.staffs.Where(c =>c.staff_category_id ==1).Select(c => new {c.staff_id, c.staff_other_names, c.staff_surname }).OrderBy(c=>c.staff_other_names).ToListAsync().ConfigureAwait(false), JsonRequestBehavior.AllowGet);
            }

        }
        //returns list of resolutions
        [OutputCache(Duration = 480, VaryByParam = "none")]
        public async Task<JsonResult> GetResolutions()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                List<patient_reminder_resolutions> resolutionsList = new List<patient_reminder_resolutions>();
                List<patient_reminder_resolutions> list = await _entities.patient_reminder_resolutions.ToListAsync().ConfigureAwait(false);
                patient_reminder_resolutions resolutionDue = new patient_reminder_resolutions();
                resolutionDue.patient_reminder_resolution_id = 10;
                resolutionDue.patient_reminder_resolution_desc = "Due";
                resolutionsList.Add(resolutionDue);
                patient_reminder_resolutions resolutionDefaulted = new patient_reminder_resolutions();
                resolutionDefaulted.patient_reminder_resolution_id = 11;
                resolutionDefaulted.patient_reminder_resolution_desc = "Defaulted";
                resolutionsList.Add(resolutionDefaulted);
                list.AddRange(resolutionsList);
                var updatedList = list.OrderBy(c => c.patient_reminder_resolution_desc).Select(c => new { c.patient_reminder_resolution_id, c.patient_reminder_resolution_desc });
               
                return Json(updatedList, JsonRequestBehavior.AllowGet);
            }

        }

        //function to retrieve covid data 
        ProcedureModels entities = new ProcedureModels();
        
        private List<PatientReminders_Result> GetReminders(DateTime startDate, DateTime endDate, FormCollection collection) 
        {
            List<staff> listOfStaff = entities.staffs.ToList();
            List<PatientReminders_Result> remindersList = entities.PatientReminders(startDate, endDate).ToList();
            if (!String.IsNullOrEmpty(collection["schemeDropDown"]))
            {
                remindersList = remindersList.Where(c => c.SCHEME == collection["schemeDropDown"]).ToList();
               
            }
            if (!String.IsNullOrEmpty(collection["doctorDropDown"]))
            {
                int assignedDoctor = int.Parse(collection["doctorDropDown"]);
                remindersList = remindersList.Where(c => c.assigned_doctor == assignedDoctor).ToList();

            }
            if (!String.IsNullOrEmpty(collection["reminderDropDown"]))
            {
                int reminderType = int.Parse(collection["reminderDropDown"]);
                remindersList = remindersList.Where(c => c.patient_reminder_type_id == reminderType).ToList();

            }
            if (!String.IsNullOrEmpty(collection["branchDropDown"]))
            {
                remindersList = remindersList.Where(c => c.facility_desc == collection["branchDropDown"]).ToList();
            }
            if (!String.IsNullOrEmpty(collection["resolutionsDropDown"]))
            {
                remindersList = remindersList.Where(c => c.Status == collection["resolutionsDropDown"]).ToList();
            }
            return remindersList;
        }

        

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [Authorize]
        [WhitespaceFilter]
        [CompressFilter]

        public ActionResult PatientRemindersView(FormCollection fc)
        {

            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }


            //f.assignDate(fc);
            DateTime fromDate;
            DateTime toDate;


            if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
            {
                fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00);
                toDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }
            else
            {
                fromDate = DateTime.Parse(fc["fromDate"]).Date;
                toDate = DateTime.Parse(fc["toDate"]).Date;
            }
            ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
            ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
            List<PatientReminders_Result> remindersList = GetReminders(fromDate, toDate, fc);

            return View(remindersList);


        }

       
       
    }
}
