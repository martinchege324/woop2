﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ActionFilters;
using System.Data.Entity;
using System.Threading.Tasks;

namespace WOOPPlusCloud2.Controllers
{
    [Authorize(Roles = "Admin,Manager")]
    [WhitespaceFilter]
   
    [CompressFilter]
    public class FinancialController : Controller
    {
               
        DateTime today = DateTime.Now;
        DateTime fromDate;
        DateTime toDate;

        //Function to assign dates to all views 
        public FormCollection assignDate(FormCollection formCollection)
        {

            if (String.IsNullOrEmpty(formCollection["fromDate"]) && String.IsNullOrEmpty(formCollection["toDate"]))
            {

                fromDate = new DateTime(today.Year, today.Month, 1);
                toDate = DateTime.Today;
            }
            else
            {
                fromDate = DateTime.Parse(formCollection["fromDate"]).Date;
                toDate = DateTime.Parse(formCollection["toDate"]).Date;
            }
            ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
            ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
            return formCollection;
        }

        //function to return revenue list from the database
        #region groupFinancesByString

        private List<FinancialProc_Result> groupFinancesByString(List<FinancialProc_Result> finance)
        {
            List<FinancialProc_Result> list = new List<FinancialProc_Result>();

            List<FinancialProc_Result> groupedFinancials = finance.GroupBy(c => c.INSURANCE).Select(c => c.First()).ToList();
            
            foreach (FinancialProc_Result item in groupedFinancials)
            {
                FinancialProc_Result newItem = new FinancialProc_Result();
                IEnumerable<FinancialProc_Result> x = finance.Where(c => c.INSURANCE == item.INSURANCE);

                newItem.TOTALCOST = x.Sum(c => c.TOTALCOST);
                newItem.Orders = x.Sum(c => c.Orders);
                newItem.INSURANCE = item.INSURANCE;
                newItem.SCHEME = item.SCHEME;
                newItem.item_description = item.item_description;
                newItem.VISITTYPE = item.VISITTYPE;
                newItem.MARGIN = x.Sum(c => c.TOTALSELLINGPRICE) - x.Sum(c => c.TOTALCOST);
                newItem.percentage_margins = newItem.MARGIN != 0 && newItem.TOTALCOST != 0 ? $"{newItem.MARGIN / newItem.TOTALCOST * 100:#,###0}" : $"{0:#,###0}";
                newItem.TOTALSELLINGPRICE = x.Sum(c => c.TOTALSELLINGPRICE);
                newItem.TOTALRETAILPRICE = x.Sum(c => c.TOTALRETAILPRICE);
                newItem.SAVINGS = x.Sum(c => c.SAVINGS);
                newItem.percentage_savings = newItem.SAVINGS != 0 && newItem.TOTALRETAILPRICE != 0
                    ? $"{x.Sum(c => c.SAVINGS) / x.Sum(c => c.TOTALRETAILPRICE) * 100:#,###0}"
                    : $"{0:#,###0}";
                newItem.CENTER = item.CENTER;
                list.Add(newItem);
            }
            return list;
        }
        #endregion
        #region Detailed Report
        //Detailed Revenue Report
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        
        public ActionResult FinancialDetailed(FormCollection fc)
        {

            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            assignDate(fc);
            using (ProcedureModels _entities = new ProcedureModels())
            {

                List<FinancialProc_Result> financials = _entities.FinancialProc(fromDate, toDate).ToList();

                if (!string.IsNullOrEmpty(fc["insuranceDropDown"]))
                {
                    financials = financials.Where(c => c.INSURANCE == fc["insuranceDropDown"]).ToList();

                }
                
                if (!string.IsNullOrEmpty(fc["visitsDropDown"]))
                {
                    financials = financials.Where(c => c.VISITTYPE == fc["visitsDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    financials = financials.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }

                List<FinancialProc_Result> list = new List<FinancialProc_Result>();
                foreach (FinancialProc_Result item in financials)
                {
                    FinancialProc_Result newItem = new FinancialProc_Result
                    {
                        charge_date = item.charge_date,
                        NAMES = item.NAMES,
                        OPNO = item.OPNO,
                        INVOICE = item.INVOICE,
                        TOTALCOST = item.TOTALCOST,
                        INSURANCE = item.INSURANCE,
                        SCHEME = item.SCHEME,
                        item_description = item.item_description,
                        VISITTYPE = item.VISITTYPE,
                        MARGIN = item.TOTALSELLINGPRICE - item.TOTALCOST,
                        CENTER = item.CENTER
                    };
                    newItem.percentage_margins = newItem.MARGIN != 0 && newItem.TOTALCOST != 0 ? $"{newItem.MARGIN / newItem.TOTALCOST * 100:#,###0}" : $"{0:#,###0}";
                    newItem.TOTALSELLINGPRICE = item.TOTALSELLINGPRICE;
                    newItem.TOTALRETAILPRICE = item.TOTALRETAILPRICE;
                    newItem.SAVINGS = item.SAVINGS;
                    newItem.CENTER = item.CENTER;
                    newItem.percentage_savings = newItem.SAVINGS != 0 && newItem.TOTALRETAILPRICE != 0
                        ? $"{item.SAVINGS / item.TOTALRETAILPRICE * 100:#,###0}"
                        : $"{0:#,###0}";
                    list.Add(newItem);
                }
                return View(list);
            }
        }

        #endregion
        #region insuranceRevenue
        // GET: Insurance Revenue 
        [WhitespaceFilter]
        [CompressFilter]
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult FinancialReportView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                assignDate(fc);
                List<FinancialProc_Result> financials = _entities.FinancialProc(fromDate, toDate).ToList();
                if (!string.IsNullOrEmpty(fc["insuranceDropDown"]))
                {
                    financials = financials.Where(c => c.INSURANCE == fc["insuranceDropDown"]).ToList();

                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    financials = financials.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }

                //pass the list to perform grouping tasks
                List<FinancialProc_Result> list = groupFinancesByString(financials);
                IEnumerable<FinancialProc_Result> summary = list.AsEnumerable();
                ViewBag.Cost = $"{summary.Sum(c => c.TOTALCOST):#,###0}";
                ViewBag.Selling = $"{summary.Sum(c => c.TOTALSELLINGPRICE):#,###0}";
                ViewBag.Margin = $"{summary.Sum(c => c.MARGIN):#,###0}";
                return View(list);
            }
               
        }
        #endregion

        //Partial Views to Render the Different Revenue Reports
  
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult DepartmentRevenue(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            assignDate(fc);
            using (ProcedureModels _entities = new ProcedureModels())
            {
                List<FinancialProc_Result> list = new List<FinancialProc_Result>();
                List<FinancialProc_Result> financials = _entities.FinancialProc(fromDate, toDate).ToList();
                if (!string.IsNullOrEmpty(fc["departmentDropDown"]))
                {
                    financials = financials.Where(c => c.item_description == fc["departmentDropDown"]).ToList();

                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    financials = financials.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }
                List<FinancialProc_Result> groupedFinancials = financials.GroupBy(c => c.item_description).Select(c => c.First()).ToList();
                foreach (FinancialProc_Result item in groupedFinancials)
                {
                    FinancialProc_Result newItem = new FinancialProc_Result();
                    IEnumerable<FinancialProc_Result> x = financials.Where(c => c.item_description == item.item_description);

                    newItem.TOTALCOST = x.Sum(c => c.TOTALCOST);
                    newItem.Orders = x.Sum(c => c.Orders);
                    newItem.INSURANCE = item.INSURANCE;
                    newItem.SCHEME = item.SCHEME;
                    newItem.item_description = item.item_description;
                    newItem.VISITTYPE = item.VISITTYPE;
                    newItem.MARGIN = x.Sum(c => c.TOTALSELLINGPRICE) - x.Sum(c => c.TOTALCOST);
                    newItem.percentage_margins = newItem.MARGIN != 0 && newItem.TOTALCOST != 0 ? $"{newItem.MARGIN / newItem.TOTALCOST * 100:#,###0}" : $"{0:#,###0}";
                    newItem.TOTALSELLINGPRICE = x.Sum(c => c.TOTALSELLINGPRICE);
                    newItem.TOTALRETAILPRICE = x.Sum(c => c.TOTALRETAILPRICE);
                    newItem.SAVINGS = x.Sum(c => c.SAVINGS);
                    newItem.percentage_savings = newItem.SAVINGS != 0 && newItem.TOTALRETAILPRICE != 0
                        ? $"{x.Sum(c => c.SAVINGS) / x.Sum(c => c.TOTALRETAILPRICE) * 100:#,###0}"
                        : $"{0:#,###0}";
                    newItem.CENTER = item.CENTER;
                    list.Add(newItem);
                }
                IEnumerable<FinancialProc_Result> summary = list.AsEnumerable();
                ViewBag.Cost = $"{summary.Sum(c => c.TOTALCOST):#,###0}";
                ViewBag.Selling = $"{summary.Sum(c => c.TOTALSELLINGPRICE):#,###0}";
                ViewBag.Margin = $"{summary.Sum(c => c.MARGIN):#,###0}";
                return View(list);
            }
           
        }

        [OutputCache(Duration = 480, VaryByParam = "none")]
        public JsonResult getDepartments() 
        {
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                toDate = DateTime.Today;
                fromDate = toDate.AddMonths(-1);
                List<FinancialProc_Result> financials = _entities.FinancialProc(fromDate, toDate).ToList();
                List<FinancialProc_Result> departments = financials.GroupBy(c => c.item_description).Select(c => c.First()).ToList();
                return Json(departments, JsonRequestBehavior.AllowGet);
            }
        }

        //Get Revenue by Visit Types 
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult VisitTypeRevenue(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            assignDate(fc);
            using (ProcedureModels _entities = new ProcedureModels())
            {
                List<FinancialProc_Result> list = new List<FinancialProc_Result>();
                assignDate(fc);
                List<FinancialProc_Result> financials = _entities.FinancialProc(fromDate, toDate).ToList();
                if (!string.IsNullOrEmpty(fc["visitsDropDown"]))
                {
                    financials = financials.Where(c => c.VISITTYPE == fc["visitsDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    financials = financials.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }
                List<FinancialProc_Result> groupedFinancials = financials.GroupBy(c => c.VISITTYPE).Select(c => c.First()).ToList();


                foreach (FinancialProc_Result item in groupedFinancials)
                {
                    FinancialProc_Result newItem = new FinancialProc_Result();
                    IEnumerable<FinancialProc_Result> x = financials.Where(c => c.VISITTYPE == item.VISITTYPE);

                    newItem.TOTALCOST = x.Sum(c => c.TOTALCOST);
                    newItem.Orders = x.Sum(c => c.Orders);
                    newItem.INSURANCE = item.INSURANCE;
                    newItem.SCHEME = item.SCHEME;
                    newItem.item_description = item.item_description;
                    newItem.VISITTYPE = item.VISITTYPE;
                    newItem.MARGIN = x.Sum(c => c.TOTALSELLINGPRICE) - x.Sum(c => c.TOTALCOST);
                    newItem.percentage_margins = newItem.MARGIN != 0 && newItem.TOTALCOST != 0 ? $"{newItem.MARGIN / newItem.TOTALCOST * 100:#,###0}" : $"{0:#,###0}";
                    newItem.TOTALSELLINGPRICE = x.Sum(c => c.TOTALSELLINGPRICE);
                    newItem.TOTALRETAILPRICE = x.Sum(c => c.TOTALRETAILPRICE);
                    newItem.SAVINGS = x.Sum(c => c.SAVINGS);
                    newItem.percentage_savings = newItem.SAVINGS != 0 && newItem.TOTALRETAILPRICE != 0
                        ? $"{x.Sum(c => c.SAVINGS) / x.Sum(c => c.TOTALRETAILPRICE) * 100:#,###0}"
                        : $"{0:#,###0}";
                    newItem.CENTER = item.CENTER;
                    list.Add(newItem);
                }
                IEnumerable<FinancialProc_Result> summary = list.AsEnumerable();
                ViewBag.Cost = $"{summary.Sum(c => c.TOTALCOST):#,###0}";
                ViewBag.Selling = $"{summary.Sum(c => c.TOTALSELLINGPRICE):#,###0}";
                ViewBag.Margin = $"{summary.Sum(c => c.MARGIN):#,###0}";

                return View(list);
            }
        }

        //Return Schemes, Insurance Co's and their respective Revenue
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult InsuranceSchemeRevenue(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels())
            {
                List<FinancialProc_Result> list = new List<FinancialProc_Result>();
                assignDate(fc);
                List<FinancialProc_Result> financials = _entities.FinancialProc(fromDate, toDate).ToList();
                if (!string.IsNullOrEmpty(fc["insuranceDropDown"]))
                {
                    financials = financials.Where(c => c.INSURANCE == fc["insuranceDropDown"]).ToList();

                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    financials = financials.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }
                List<FinancialProc_Result> groupedFinancials = financials.GroupBy(c => c.SCHEME).Select(c => c.First()).ToList();
                foreach (FinancialProc_Result item in groupedFinancials)
                {
                    FinancialProc_Result newItem = new FinancialProc_Result();
                    IEnumerable<FinancialProc_Result> x = financials.Where(c => c.SCHEME == item.SCHEME);

                    newItem.TOTALCOST = x.Sum(c => c.TOTALCOST);
                    newItem.Orders = x.Sum(c => c.Orders);
                    newItem.INSURANCE = item.INSURANCE;
                    newItem.SCHEME = item.SCHEME;
                    newItem.item_description = item.item_description;
                    newItem.VISITTYPE = item.VISITTYPE;
                    newItem.MARGIN = x.Sum(c => c.TOTALSELLINGPRICE) - x.Sum(c => c.TOTALCOST);
                    newItem.percentage_margins = newItem.MARGIN != 0 && newItem.TOTALCOST != 0 ? $"{newItem.MARGIN / newItem.TOTALCOST * 100:#,###0}" : $"{0:#,###0}";
                    newItem.TOTALSELLINGPRICE = x.Sum(c => c.TOTALSELLINGPRICE);
                    newItem.TOTALRETAILPRICE = x.Sum(c => c.TOTALRETAILPRICE);
                    newItem.SAVINGS = x.Sum(c => c.SAVINGS);
                    newItem.percentage_savings = newItem.SAVINGS != 0 && newItem.TOTALRETAILPRICE != 0
                        ? $"{x.Sum(c => c.SAVINGS) / x.Sum(c => c.TOTALRETAILPRICE) * 100:#,###0}"
                        : $"{0:#,###0}";
                    newItem.CENTER = item.CENTER;
                    list.Add(newItem);
                }
                IEnumerable<FinancialProc_Result> summary = list.AsEnumerable();
                ViewBag.Cost = $"{summary.Sum(c => c.TOTALCOST):#,###0}";
                ViewBag.Selling = $"{summary.Sum(c => c.TOTALSELLINGPRICE):#,###0}";
                ViewBag.Margin = $"{summary.Sum(c => c.MARGIN):#,###0}";
                return View(list);
            }
        }

        //all finance report 
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult AllRevenueView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            assignDate(fc);
            using (ProcedureModels entities = new ProcedureModels()) 
            {
                List<FinanceProcNew_Result> financials = entities.FinanceProcNew(fromDate, toDate).ToList();
                if (!string.IsNullOrEmpty(fc["insuranceDropDown"]))
                {
                    financials = financials.Where(c => c.INSURANCECODE == fc["insuranceDropDown"]).ToList();

                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    financials = financials.Where(c => c.CENTER == fc["branchDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["schemeDropDown"]))
                {
                    financials = financials.Where(c => c.SCHEMECODE == fc["schemeDropDown"]).ToList();

                }
                if (!String.IsNullOrEmpty(fc[" visitsDropDown"]))
                {
                    financials = financials.Where(c => c.VISITTYPE == fc[" visitsDropDown"]).ToList();

                }
               
                IEnumerable<FinanceProcNew_Result> summary = financials.AsEnumerable();
                ViewBag.Cost = $"{summary.Sum(c => c.TOTALCOST):#,###0}";
                ViewBag.Selling = $"{summary.Sum(c => c.TOTALSELLINGPRICE):#,###0}";
                ViewBag.Savings = $"{summary.Sum(c => c.SAVINGS):#,###0}";
                return View(financials);
            }
               
        }
            //get payment methods
            public async Task<JsonResult> getPaymentMethods() 
        
        {
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                List<payment_method> paymentModes = await _entities.payment_method.ToListAsync().ConfigureAwait(false);
                return Json(paymentModes, JsonRequestBehavior.AllowGet);
            }
        }

      

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult CashReceiptsView(FormCollection fc) 
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                assignDate(fc);
                List<CashReceipts_Result> receipts = _entities.CashReceipts(fromDate, toDate).ToList();
                if (!string.IsNullOrEmpty(fc["paymentMethodDropDown"]))
                {
                    receipts = receipts.Where(c => c.PaymentMethod1 == fc["paymentMethodDropDown"] || c.PaymentMethod2 == fc["paymentMethodDropDown"]).ToList();
                }
                //staff
                if (!string.IsNullOrEmpty(fc["staffDropDown"]))
                {
                    receipts = receipts.Where(c => c.Staff == fc["staffDropDown"]).ToList();
                }
                if (!String.IsNullOrEmpty(fc["branchDropDown"]))
                {
                    receipts = receipts.Where(c => c.Center == fc["branchDropDown"]).ToList();
                }

                //Summary
                decimal? cash = (from cashPayments in receipts where (cashPayments.PaymentMethod1 == "Cash" || cashPayments.PaymentMethod2 == "Cash") select cashPayments).Sum(c => c.ReceiptAmount);
                decimal? total = receipts.Sum(c => c.ReceiptAmount);
                decimal? mpesa = (from cashPayments in receipts where (cashPayments.PaymentMethod1 == "M-Pesa" || cashPayments.PaymentMethod2 == "M-Pesa") select cashPayments).Sum(c => c.ReceiptAmount);
                decimal? cheque = (from cashPayments in receipts where (cashPayments.PaymentMethod1 == "Cheque" || cashPayments.PaymentMethod2 == "Cheque") select cashPayments).Sum(c => c.ReceiptAmount);
                decimal? pdq = (from cashPayments in receipts where (cashPayments.PaymentMethod1 == "PDQ" || cashPayments.PaymentMethod2 == "PDQ") select cashPayments).Sum(c => c.ReceiptAmount);
                decimal? bank = (from cashPayments in receipts where (cashPayments.PaymentMethod1 == "Bank Transfer" || cashPayments.PaymentMethod2 == "Bank Transfer") select cashPayments).Sum(c => c.ReceiptAmount);
                decimal? cancelled = (from cashPayments in receipts where cashPayments.Cancelled == "1" select cashPayments).Sum(c => c.ReceiptAmount);
                ViewBag.Total = $"{total:#,###0}";
                ViewBag.Cash = $"{cash :#,###0}";
                ViewBag.MPesa = $"{mpesa:#,###0}";
                ViewBag.Cheque = $"{cheque:#,###0}";
                ViewBag.PDQ = $"{pdq:#,###0}";
                ViewBag.Bank = $"{bank:#,###0}";
                ViewBag.Cancelled = $"{cancelled:#,###0}";
                return View(receipts);
            }
           
        }
    }
}