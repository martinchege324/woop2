﻿using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ActionFilters;

namespace WOOPPlusCloud2.Controllers
{
    [OutputCache(Duration = 480, VaryByParam = "none")]

    public class TargetsController : Controller
    {
        private readonly ProcedureModels _entities = new ProcedureModels();

        // GET: Targets
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public ActionResult TargetView()
        {
            return View();
        }

        //Pulling results as an AJAX Call
        public JsonResult getTargets(string id)
        {
            System.Data.Entity.Core.Objects.ObjectResult<DisplayTargets_Result> targets = _entities.DisplayTargets();
            return Json(targets, JsonRequestBehavior.AllowGet);
        }


        // POST: Targets/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DisplayTargets_Result model, FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                _entities.InsertTarget(model.target_name, model.target_value);
                return RedirectToAction("TargetView");
            }
            catch
            {
                return View();
            }
        }

        // GET: Targets/Edit/5
        public ActionResult Edit(string name)
        {
            return View();
        }

        // POST: Targets/Edit/5
        [HttpPost]
        public ActionResult Edit(DisplayTargets_Result model, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                _entities.UpdateTarget(model.target_name, model.target_value);
                return RedirectToAction("TargetView");
            }
            catch
            {
                return View();
            }
        }
    }
}