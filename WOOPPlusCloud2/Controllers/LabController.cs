﻿using System;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ActionFilters;

namespace WOOPPlusCloud2.Controllers
{
    [OutputCache(Duration = 480, VaryByParam = "none")]

    public class LabController : Controller
    {
        private readonly ProcedureModels _entities = new ProcedureModels();

        // GET: Lab
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public ActionResult LabTests()
        {
            DateTime fromDate = DateTime.Today.AddMonths(-1);
            System.Collections.Generic.IEnumerable<labTATs_Result> labs = _entities.labTATs().Where(c => c.date > fromDate).AsEnumerable();
            double? summary = labs.Average(c => c.DateDiff);
            ViewBag.TAT = summary;
            return View();
        }
    }
}