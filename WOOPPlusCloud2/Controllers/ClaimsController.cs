﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ActionFilters;

namespace WOOPPlusCloud2.Controllers
{
    public class ClaimsController : Controller
    {
        // GET: Claims
        [Authorize(Roles = "Admin,Clinical,Manager")]
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        //[OutputCache(Duration = 480, VaryByParam = "none")]
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public ActionResult ClaimSummaryView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels())
            {
                DateTime fromDate;
                DateTime toDate;
                DateTime today = DateTime.Now;
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    fromDate = DateTime.Now.AddDays(-7);
                    toDate = DateTime.Today;
                }
                else
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");

                IOrderedEnumerable<deposit> claims = _entities.deposits.ToList().Where(c => DateTime.Parse(c.date_created.ToString()).Date >= fromDate && (DateTime.Parse(c.date_created.ToString()) <= toDate)).OrderBy(c => c.date_created);

                List<deposit> groupedClaims = claims.GroupBy(c => DateTime.Parse(c.date_created.ToString()).Date).Select(c => c.FirstOrDefault()).ToList();

                List<deposit> claimView = new List<deposit>();

                foreach (deposit item in groupedClaims)
                {
                    deposit resultItem = new deposit();
                    resultItem.date_created = item.date_created;
                    List<deposit> filteredClaims = claims.Where(c => DateTime.Parse(c.date_created.ToString()).Date == DateTime.Parse(item.date_created.ToString()).Date).ToList();
                    resultItem.invoice_closed = filteredClaims.Where(c => c.invoice_closed == 1).Count();
                    resultItem.offline_smart_post = filteredClaims.Where(c => c.offline_smart_post == 2).Count();
                    resultItem.smart_not_run = filteredClaims.Where(c => c.offline_smart_post != 2).Count();
                    resultItem.invoice_rejected = filteredClaims.Where(c => c.invoice_rejected == 1).Count();
                    resultItem.invoice_paid = filteredClaims.Where(c => c.invoice_paid == 1).Count();
                    resultItem.invoice_not_paid = filteredClaims.Where(c => c.invoice_paid != 1).Count();
                    resultItem.invoice_dispatched = filteredClaims.Where(c => c.invoice_dispatched == 1).Count();
                    resultItem.invoice_returned = filteredClaims.Where(c => c.invoice_returned == 1).Count();
                    resultItem.invoice_not_dispatched = filteredClaims.Where(c => c.invoice_dispatched != 1).Count();
                    resultItem.invoice_partially_paid = filteredClaims.Where(c => c.amount_paid > 0 && c.invoice_paid != 1).Count();
                    claimView.Add(resultItem);
                }
                return View(claimView);
            }

        }
    }
}