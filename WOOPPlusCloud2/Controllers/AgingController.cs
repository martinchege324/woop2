﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ActionFilters;
using System.Threading.Tasks;

namespace WOOPPlusCloud2.Controllers
{
    [Authorize]
    //[OutputCache(Duration = 480, VaryByParam = "none")]

    public class AgingController : Controller
    {
        
        private delegate List<AgingProc_Result> AgingSPDelegate();

        // GET: Aging

        private List<AgingProc_Result> Aging()
        {
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                List<AgingProc_Result> list = new List<AgingProc_Result>();
                //Exclude hub and whole sale 
                List<AgingProc_Result> aging = _entities.AgingProc().Where(c => c.payment_account_desc != "WHOLE SALE").Where(c => c.payment_account_desc != "HUB").ToList();

                List<AgingProc_Result> groupedAging = aging.GroupBy(c => c.payment_account_id).Select(c => c.First()).ToList();
                decimal? totalCharged = aging.Sum(c => c.TOTALCHARGE);
                decimal? totalPaid = aging.Sum(c => c.PAIDAMOUNT);
                decimal? totalBalance = totalCharged - totalPaid;

                ViewBag.RunningBalance = $"{totalBalance:#,###0}";
                ViewBag.TotalCharge = $"{totalCharged:#,###0}";
                ViewBag.TotalPaid = $"{totalPaid:#,###0}";

                foreach (AgingProc_Result item in groupedAging)
                {
                    AgingProc_Result newItem = new AgingProc_Result
                    {
                        payment_account_desc = item.payment_account_desc
                    };
                    IEnumerable<AgingProc_Result> group = aging.Where(c => c.payment_account_id == item.payment_account_id);
                    IEnumerable<AgingProc_Result> agingProcResults = group.ToList();
                    //total charged
                    newItem.TOTALCHARGE = agingProcResults.Sum(c => c.TOTALCHARGE);
                    //paid amount
                    newItem.PAIDAMOUNT = agingProcResults.Sum(c => c.PAIDAMOUNT);
                    //unallocated amount
                    newItem.unallocated_amount = item.unallocated_amount;
                    //running balance
                    newItem.running_balance = newItem.TOTALCHARGE - newItem.PAIDAMOUNT;

                    //aging stuff 
                    IEnumerable<AgingProc_Result> overOneFifty = agingProcResults.Where(c =>
                        DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-150));
                    newItem.summary_over_one_fifty = overOneFifty.Sum(c => c.TOTALCHARGE) - overOneFifty.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> over = agingProcResults.Where(c =>
                        DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-120));
                    newItem.summary = over.Sum(c => c.TOTALCHARGE) - over.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> overNinety = agingProcResults.Where(c => DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-90) && DateTime.Parse(c.invoice_closed_date.ToString()) > DateTime.Today.AddDays(-120));
                    newItem.summary_ninety = overNinety.Sum(c => c.TOTALCHARGE) - overNinety.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> overSixty = agingProcResults.Where(c => DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-60) && DateTime.Parse(c.invoice_closed_date.ToString()) > DateTime.Today.AddDays(-90));
                    newItem.summary_sixty = overSixty.Sum(c => c.TOTALCHARGE) - overSixty.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> overThirty = agingProcResults.Where(c => DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-30) && DateTime.Parse(c.invoice_closed_date.ToString()) > DateTime.Today.AddDays(-60));
                    newItem.summary_thirty = overThirty.Sum(c => c.TOTALCHARGE) - overThirty.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> current = agingProcResults.Where(c => DateTime.Parse(c.invoice_closed_date.ToString()) > DateTime.Today.AddDays(-30));
                    newItem.summary_current = current.Sum(c => c.TOTALCHARGE) - current.Sum(c => c.PAIDAMOUNT);


                    list.Add(newItem);
                }
                return list;
            }
                
        }

        public Task<List<AgingProc_Result>> AgingAsync()
        {
            AgingSPDelegate caller = new AgingSPDelegate(Aging);
            return Task.Factory.FromAsync(caller.BeginInvoke, caller.EndInvoke, null);
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [Authorize]
        [OutputCache(Duration = 480, VaryByParam = "none")]
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        public async Task<ActionResult> AgingReportView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            DateTime fromDate;
            DateTime toDate;
            List<AgingProc_Result> list = await AgingAsync().ConfigureAwait(false);
            if (!String.IsNullOrEmpty(fc["fromDate"]) && !String.IsNullOrEmpty(fc["toDate"]))
            {
                fromDate = DateTime.Parse(fc["fromDate"]).Date;
                toDate = DateTime.Parse(fc["toDate"]).Date;
                List<AgingProc_Result> groupedAging = list.GroupBy(c => c.payment_account_id).Select(c => c.First())
                .Where(c => c.invoice_closed_date >= fromDate && c.invoice_closed_date <= toDate).ToList();
                decimal? totalCharged = list.Sum(c => c.TOTALCHARGE);
                decimal? totalPaid = list.Sum(c => c.PAIDAMOUNT);
                decimal? totalBalance = totalCharged - totalPaid;

                ViewBag.RunningBalance = $"{totalBalance:#,###0}";
                ViewBag.TotalCharge = $"{totalCharged:#,###0}";
                ViewBag.TotalPaid = $"{totalPaid:#,###0}";

                foreach (AgingProc_Result item in groupedAging)
                {
                    AgingProc_Result newItem = new AgingProc_Result
                    {
                        payment_account_desc = item.payment_account_desc
                    };
                    IEnumerable<AgingProc_Result> group = list.Where(c => c.payment_account_id == item.payment_account_id);
                    IEnumerable<AgingProc_Result> agingProcResults = group.ToList();
                    newItem.TOTALCHARGE = agingProcResults.Sum(c => c.TOTALCHARGE);
                    newItem.PAIDAMOUNT = agingProcResults.Sum(c => c.PAIDAMOUNT);
                    newItem.running_balance = newItem.TOTALCHARGE - newItem.PAIDAMOUNT;
                    //unallocated amount
                    newItem.unallocated_amount = item.unallocated_amount;

                    //aging stuff 

                    IEnumerable<AgingProc_Result> overOneFifty = agingProcResults.Where(c =>
                        DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-150));
                    newItem.summary_over_one_fifty = overOneFifty.Sum(c => c.TOTALCHARGE) - overOneFifty.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> over = agingProcResults.Where(c =>
                        DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-120));
                    newItem.summary = over.Sum(c => c.TOTALCHARGE) - over.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> overNinety = agingProcResults.Where(c => DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-90) && DateTime.Parse(c.invoice_closed_date.ToString()) > DateTime.Today.AddDays(-120));
                    newItem.summary_ninety = overNinety.Sum(c => c.TOTALCHARGE) - overNinety.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> overSixty = agingProcResults.Where(c => DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-60) && DateTime.Parse(c.invoice_closed_date.ToString()) > DateTime.Today.AddDays(-90));
                    newItem.summary_sixty = overSixty.Sum(c => c.TOTALCHARGE) - overSixty.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> overThirty = agingProcResults.Where(c => DateTime.Parse(c.invoice_closed_date.ToString()) < DateTime.Today.AddDays(-30) && DateTime.Parse(c.invoice_closed_date.ToString()) > DateTime.Today.AddDays(-60));
                    newItem.summary_thirty = overThirty.Sum(c => c.TOTALCHARGE) - overThirty.Sum(c => c.PAIDAMOUNT);
                    IEnumerable<AgingProc_Result> current = agingProcResults.Where(c => DateTime.Parse(c.invoice_closed_date.ToString()) > DateTime.Today.AddDays(-30));
                    newItem.summary_current = current.Sum(c => c.TOTALCHARGE) - current.Sum(c => c.PAIDAMOUNT);
                    
                    list.Add(newItem);
                }

                return View(list);

            }
            else 
            {
                return View(list);
            } 
           
        }

    }
}