﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    public class RefillsController : Controller
    {
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [WhitespaceFilter]
        [CompressFilter]
        [ETag]
        // GET: Refills
        public ActionResult RefillsView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                //Reminders Summary
                DateTime fromDate;
                DateTime toDate;
                List<Reminders_Result> remindersList = new List<Reminders_Result>();
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    fromDate = DateTime.Now.AddDays(-7);
                    toDate = DateTime.Today;
                }
                else
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");

                List<Reminders_Result> reminders = _entities.Reminders().Where(c => DateTime.Parse(c.ReminderDate.ToString()).Date >= DateTime.Parse(fromDate.ToString()).Date && DateTime.Parse(c.ReminderDate.ToString()).Date <= DateTime.Parse(toDate.ToString()).Date).ToList();
                reminders = reminders.GroupBy(c => c.patient_reminder_id).Select(c => c.First()).ToList();
                IOrderedEnumerable<Reminders_Result> groupedReminders = reminders.GroupBy(c => DateTime.Parse(c.ReminderDate.ToString()).Date).Select(c => c.First()).OrderBy(c => c.ReminderDate);
                foreach (Reminders_Result item in groupedReminders)
                {
                    //reminders list
                    Reminders_Result reminderItem = new Reminders_Result
                    {
                        ReminderDate = item.ReminderDate
                    };
                    List<Reminders_Result> filteredReminders = reminders.Where(c => c.ReminderDate.Value.Date == item.ReminderDate.Value.Date).ToList();
                    reminderItem.Total = filteredReminders.Where(c => c.ReminderDate.HasValue).Count();
                    reminderItem.sms_sent = filteredReminders.Where(c => c.sms_sent == 1).Count();
                    reminderItem.patient_reminder_resolution_id = filteredReminders.Where(c => c.patient_reminder_resolution_id > 0).Count();
                    reminderItem.outstanding = reminderItem.Total - (reminderItem.sms_sent + reminderItem.patient_reminder_resolution_id);
                    //Add internal sms sent and (patient sms sent: external)
                    remindersList.Add(reminderItem);

                }
                return View(remindersList);
            }
                
        }
       
    }
}