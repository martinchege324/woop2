﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ActionFilters;
using System.Net;
using System.Web;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using System;

namespace WOOPPlusCloud2.Controllers
{
    /// <summary>
    ///     Manage users, assign roles, approve and toggle active property.
    ///     Accessible to admin and manager
    /// </summary>
    [Authorize(Roles = "Admin")]
    [WhitespaceFilter]
    [CompressFilter]
    [ETag]
    public class ManageUsersController : Controller
    {
        //private readonly ProcedureModels _entities = new ProcedureModels();
        // private ApplicationUserManager _userManager;
        ApplicationDbContext db = new ApplicationDbContext();

        // GET: ManageUsers
        public async Task<ActionResult> Index()
        {
            using (ApplicationDbContext _entities = new ApplicationDbContext()) 
            {
                List<ApplicationUser> users = await  _entities.Users.ToListAsync().ConfigureAwait(false);
                return View(users);
            }
                
        }

        /*// GET: ManageUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }
        */

        // GET: ManageUsers/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: ManageUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create(ApplicationUser model)
        //{
        //    using (ApplicationDbContext db = new ApplicationDbContext()) 
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            ApplicationUser user = new ApplicationUser { UserName = model.Email, Email = model.Email, PasswordHash = model.PasswordHash,/*Password = model.Password, */PhoneNumber = model.PhoneNumber /*user_roles = model.user_roles*/};

        //            IdentityResult result = await UserManager.CreateAsync(user, model.PasswordHash);

        //            if (result.Succeeded)
        //            {
        //                await this.UserManager.AddToRoleAsync(user.Id, model.user_roles);
        //                bool isActive = model.Is_Active;
        //                bool isApproved = model.Is_Approved;
        //                ProcedureModels _entities = new ProcedureModels();
        //                _entities.UpdateUsers(isActive, isApproved, user.Id);
        //                //Sign In the User by Default
        //                //await SignInManager.SignInAsync(user, false, false);


        //                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
        //                // Send an email with this link
        //                // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
        //                // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
        //                // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
        //                //return the user to the home page after sign up
        //                //return RedirectToAction("Index", "Home");
        //                return RedirectToAction("Index");

        //            }



        //        }

        //        return View();
        //    }

        //}

        // GET: ManageUsers/Edit/5
        
        public ActionResult Edit(string id)
        {
           
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                ApplicationUser applicationUser = db.Users.Find(id);
                if (applicationUser == null)
                {
                    return HttpNotFound();
                }
                ManageUsersViewModel manageUsersViewModel = new ManageUsersViewModel();
                manageUsersViewModel.Id = id;
                manageUsersViewModel.UserName = applicationUser.UserName;
                //manageUsersViewModel.s = applicationUser.staff_full_names;
                manageUsersViewModel.Is_Active = applicationUser.Is_Active;
                manageUsersViewModel.Is_Approved = applicationUser.Is_Approved;
                //string adminUserRole = ConfigurationManager.AppSettings["adminUserRole"];
                ViewBag.Roles = new SelectList(db.Roles.ToList(), "Name", "Name", applicationUser.user_roles);
                return View(manageUsersViewModel);
            
               
        }

        // POST: ManageUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ManageUsersViewModel applicationUser)
        {
            
            
                if (String.IsNullOrEmpty(applicationUser.user_roles))
                {
                    ViewBag.Roles = new SelectList(db.Roles.ToList(), "Name", "Name");
                    ModelState.AddModelError("user_roles", "Please select a user role.");
                    return View(applicationUser);
                }

               if (ModelState.IsValid)
                {
                    ApplicationUser applicationUserToEdit = db.Users.Find(applicationUser.Id);
                    applicationUserToEdit.Is_Approved = applicationUser.Is_Approved;
                    applicationUserToEdit.Is_Active = applicationUser.Is_Active;
                    db.Entry(applicationUserToEdit).State = EntityState.Modified;
                    db.SaveChanges();
                    ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                IList<string> roles = await userManager.GetRolesAsync(applicationUser.Id).ConfigureAwait(false);
                    await userManager.RemoveFromRolesAsync(applicationUser.Id, roles.ToArray()).ConfigureAwait(false);
                    await userManager.AddToRoleAsync(applicationUser.Id, applicationUser.user_roles).ConfigureAwait(false);
                    return RedirectToAction("Index");
                }
                ViewBag.Roles = new SelectList(db.Roles.ToList(), "Name", "Name");
                return View(applicationUser);
            
               
           
        }

        // GET: ManageUsers/Delete/5
        public ActionResult Delete(string id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
            
                
        }

        //POST: ManageUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
           
            ApplicationUser applicationUser = db.Users.Find(id);
            db.Users.Remove(applicationUser);
            db.SaveChanges();
            return RedirectToAction("Index");
       
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}