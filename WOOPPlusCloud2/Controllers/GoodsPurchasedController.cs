﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.ActionFilters;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    [Authorize]
    
    [OutputCache(Duration = 480, VaryByParam = "none")]
    [WhitespaceFilter]
    [CompressFilter]
    [ETag]
    public class GoodsPurchasedController : Controller
    {
        // GET: GoodsPurchased
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult GoodsPurchasedView(FormCollection fc)
        {
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                if (Request.HttpMethod.ToLower() == "post")
                {
                    System.Web.Helpers.AntiForgery.Validate();
                }
                DateTime fromDate;
                DateTime toDate;
                DateTime today = DateTime.Now;
                if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
                {
                    toDate = DateTime.Now;
                    fromDate = new DateTime(today.Year, today.Month, 1);
                }
                else
                {
                    fromDate = DateTime.Parse(fc["fromDate"]).Date;
                    
                    toDate = DateTime.Parse(fc["toDate"]).Date;
                    
                }
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
                List<GoodsPurchased_Result> goodsPurchased = _entities.GoodsPurchased(fromDate, toDate).ToList();
                if (!String.IsNullOrEmpty(fc["supplierDropDown"]))
                {
                    goodsPurchased = goodsPurchased.Where(c => c.supplier_name == fc["supplierDropDown"]).ToList();
                }
                IEnumerable<GoodsPurchased_Result> data = goodsPurchased.AsEnumerable();


                /*Summary of Totals and Discounts*/
                ViewBag.Total = $"{data.Sum(x => x.Total):#,###0}";
                ViewBag.Discount = $"{data.Average(x => x.discount):#,###0}";
                
                List<GoodsPurchased_Result> list = new List<GoodsPurchased_Result>();
                foreach (GoodsPurchased_Result item in goodsPurchased) 
                {
                    GoodsPurchased_Result purchasedItem = new GoodsPurchased_Result
                    {
                        trans_date = item.trans_date,
                        invoice_no = item.invoice_no,
                        product_code = item.product_code,
                        DRUG = item.DRUG,
                        Quantity = item.Quantity,
                        batch_no = item.batch_no,
                        expiry_date = item.expiry_date,
                        supplier_name = item.supplier_name,
                        discount = item.discount,
                        vatvalue = item.vatvalue,
                        Total = item.Total,
                        vat_ex_amount = item.vat_ex_amount,
                        UnitIncDisc = (item.Total - item.discount) / Convert.ToDecimal(item.Quantity),
                        UnitexDisc = item.Total / Convert.ToDecimal(item.Quantity)
                    };
                    list.Add(purchasedItem);

                }
                return View(list);
            }
                
        }
        public JsonResult getSupplier()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                DateTime toDate = DateTime.Now;
                DateTime fromDate = toDate.AddYears(-1);
                List<GoodsPurchased_Result> supplier = _entities.GoodsPurchased(fromDate, toDate).GroupBy(c => c.supplier_name).Select(c => c.First()).OrderBy(c => c.supplier_name).ToList();
                List<GoodsPurchased_Result> list = new List<GoodsPurchased_Result>();
                foreach (GoodsPurchased_Result item in supplier)
                {
                    GoodsPurchased_Result supplierItem = new GoodsPurchased_Result
                    {
                        supplier_name = item.supplier_name
                    };
                    list.Add(supplierItem);
                }

                return Json(list, JsonRequestBehavior.AllowGet);
            }

        }
    }
}