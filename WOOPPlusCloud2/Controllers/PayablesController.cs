﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;
using WOOPPlusCloud2.ActionFilters;
using System.Threading.Tasks;

namespace WOOPPlusCloud2.Controllers
{
    [Authorize(Roles = "Admin,Manager")]
    
    [WhitespaceFilter]
    [CompressFilter]
    [ETag]
    public class PayablesController : Controller
    {
        
        private delegate List<SupplierPaymentsProc_Result> SupplierPaymentsSPDelegate();
        private delegate List<SupplierTransProc_Result> SupplierTransSPDelegate();
        DateTime fromDate;
        DateTime toDate;
        DateTime today = DateTime.Now;

        private List<SupplierPaymentsProc_Result> SupplierPayments()
        {
            using (ProcedureModels _entities = new ProcedureModels()) 
            {
                List<SupplierPaymentsProc_Result> supplierPayments = _entities.SupplierPaymentsProc().OrderByDescending(c => c.supplier_payment_date).ToList();
                return supplierPayments;
            }
               
        }
        private List<SupplierTransProc_Result> SupplierTransactions()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                List<SupplierTransProc_Result> suppliers = _entities.SupplierTransProc().ToList();
                return suppliers;
            }
        }
        public Task<List<SupplierPaymentsProc_Result>> SupplierPaymentsAsync()
        {
            SupplierPaymentsSPDelegate caller = new SupplierPaymentsSPDelegate(SupplierPayments);
            return Task.Factory.FromAsync(caller.BeginInvoke, caller.EndInvoke, null);
        }
        public Task<List<SupplierTransProc_Result>> SupplierTransactionsAsync()
        {
            SupplierTransSPDelegate caller = new SupplierTransSPDelegate(SupplierTransactions);
            return Task.Factory.FromAsync(caller.BeginInvoke, caller.EndInvoke, null);
        }
        // GET: Payables

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public async Task<ActionResult> PayablesReportView(FormCollection fc)
        {
            if (Request.HttpMethod.ToLower() == "post")
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            //Method calls
            List<SupplierTransProc_Result> suppliers = new List<SupplierTransProc_Result>();
            List<SupplierTransProc_Result> groupedSuppliers = new List<SupplierTransProc_Result>();
            List<SupplierTransProc_Result> list = new List<SupplierTransProc_Result>();
            List<SupplierPaymentsProc_Result> paymentLists = new List<SupplierPaymentsProc_Result>();
            suppliers = await SupplierTransactionsAsync().ConfigureAwait(false);
            if (String.IsNullOrEmpty(fc["fromDate"]) && String.IsNullOrEmpty(fc["toDate"]))
            {
                ViewBag.fromDate = string.Empty;
                ViewBag.toDate = string.Empty;
                groupedSuppliers = suppliers.GroupBy(c => c.supplier_id).Select(c => c.First()).ToList();

            }
            else
            {
                fromDate = DateTime.Parse(fc["fromDate"]).Date;
                toDate = DateTime.Parse(fc["toDate"]).Date;
                ViewBag.fromDate = fromDate.ToString("dd-MM-yyyy");
                ViewBag.toDate = toDate.ToString("dd-MM-yyyy");
                suppliers = suppliers.Where(c => c.trans_date >= fromDate && c.trans_date <= toDate).ToList();
                groupedSuppliers = suppliers.GroupBy(c => c.supplier_id).Select(c => c.First()).Where(c => DateTime.Parse(c.trans_date.ToString()).Date >= fromDate &&  DateTime.Parse(c.trans_date.ToString()).Date <= toDate).ToList();

            }
           
   
            List<SupplierPaymentsProc_Result> supplierPayments = await SupplierPaymentsAsync().ConfigureAwait(false);
            IEnumerable<SupplierPaymentsProc_Result> paymentData = supplierPayments.AsEnumerable();
            
            IEnumerable<SupplierTransProc_Result> data = suppliers.AsEnumerable();


            /*Summary of Total Sales and Costs*/
            ViewBag.TotalPayable = $"{data.Sum(c => c.total):#,###0}";
            ViewBag.TotalPaid = $"{paymentData.Sum(c => c.supplier_payment_amount):#,###0}";
            ViewBag.TotalBalance = $"{data.Sum(c => c.total) - paymentData.Sum(c => c.supplier_payment_amount):#,###0}";
            ViewBag.TotalVAT = $"{data.Sum(x => x.vatvalue):#,###0}";
            ViewBag.TotalExc = $"{data.Sum(x => x.vat_ex_amount):#,###0}";

            foreach (SupplierTransProc_Result item in groupedSuppliers)
            {
                SupplierTransProc_Result newItem = new SupplierTransProc_Result();
                SupplierPaymentsProc_Result paymentItem = new SupplierPaymentsProc_Result();
                newItem.payments = new SupplierPaymentsProc_Result();
                newItem.supplier_account = item.supplier_account;
                newItem.supplier_name = item.supplier_name;
                IEnumerable<SupplierTransProc_Result> x = suppliers.Where(c => c.supplier_id == item.supplier_id);
                IEnumerable<SupplierPaymentsProc_Result> y = supplierPayments.Where(c => c.supplier_id == item.supplier_id);
                newItem.total = x.Sum(c => c.total);
                newItem.trans_quantity = x.Sum(c => c.trans_quantity);
                newItem.payments.supplier_payment_amount = y.Sum(c => c.supplier_payment_amount);
                newItem.payments.last_supplier_payment_amount = y.Select(c => c.supplier_payment_amount).FirstOrDefault();
                newItem.payments.balance = x.Sum(c => c.total) - y.Sum(c => c.supplier_payment_amount);
                newItem.payments.supplier_payment_date = y.Select(c => c.supplier_payment_date).FirstOrDefault();
                newItem.payments.paymentDate = $"{newItem.payments.supplier_payment_date:dd-MM-yyyy}";
                list.Add(newItem);


            }

       
            return View(list);
        }

     
    }
}