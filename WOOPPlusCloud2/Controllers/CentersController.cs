﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.Controllers
{
    public class CentersController : Controller
    {
        // GET: Centers
        [OutputCache(Duration = 480, VaryByParam = "none")]
        public JsonResult getFacilities()
        {
            using (ProcedureModels _entities = new ProcedureModels())
            {
                List<GetFacilities_Result> facilities = _entities.GetFacilities().ToList();
                return Json(facilities, JsonRequestBehavior.AllowGet);
            }
        }
    }
}