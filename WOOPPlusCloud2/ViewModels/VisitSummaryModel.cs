﻿using System.Collections.Generic;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.ViewModels
{
    public class VisitSummaryModel
    {
        public List<PatientNotes_Result> PatientNotes { get; set; }
        public List<PatientDiagnosis_Result> PatientDiagnosis { get; set; }
        public List<PatientSigns_Result> PatientSigns { get; set; }
        public List<PatientSymptoms_Result> PatientSymptoms { get; set; }
        public List<PatientVitalsProc_Result> PatientVitals { get; set; }
        public List<PatientDrugs_Result> PatientTreatment { get; set; }
    }
}