﻿using System.Collections.Generic;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.ViewModels
{
    public class ConciergeViewModel
    {
        public List<ConciergeOrders_Result> ConciergeOrders { get; set; }
        public List<Reminders_Result> Reminders { get; set; }
    }
}