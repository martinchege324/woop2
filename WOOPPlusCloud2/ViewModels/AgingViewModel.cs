﻿using PagedList;
using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.ViewModels
{
    public class AgingViewModel
    {
        public IPagedList<AgingProc_Result> AgingProcResults { get; set; }
    }
}