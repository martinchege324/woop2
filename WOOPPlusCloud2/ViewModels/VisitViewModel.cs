﻿using WOOPPlusCloud2.Models;

namespace WOOPPlusCloud2.ViewModels
{
    public class VisitViewModel
    {
        public visit_cycle VisitCycles { get; set; }
        public patient_registration PatientRegistrations { get; set; }
        public center_visit_types CenterVisitType { get; set; }
    }
}