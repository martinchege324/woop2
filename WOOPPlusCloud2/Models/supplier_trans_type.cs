using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class supplier_trans_type
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public supplier_trans_type()
        {
            supplier_trans = new HashSet<supplier_trans>();
            supplier_trans1 = new HashSet<supplier_trans>();
            supplier_trans2 = new HashSet<supplier_trans>();
        }

        [Key] public int supplier_trans_type_id { get; set; }

        [StringLength(250)] public string supplier_trans_type_description { get; set; }

        [StringLength(1)] public string supplier_trans_type_sign { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<supplier_trans> supplier_trans { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<supplier_trans> supplier_trans1 { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<supplier_trans> supplier_trans2 { get; set; }
    }
}