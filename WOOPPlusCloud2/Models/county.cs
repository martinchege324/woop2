using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("county")]
    public class county
    {
        [Key] public int county_id { get; set; }

        [StringLength(250)] public string county_name { get; set; }

        public int district_count { get; set; }

        public int? province_id { get; set; }
    }
}