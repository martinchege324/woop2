using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class inpatient_queue_count
    {
        [Key] public Guid inpatient_queue_count_id { get; set; }

        public int? q_level { get; set; }

        public int? que_details_id { get; set; }

        public int? center_id { get; set; }

        public int? Qcount { get; set; }

        public Guid? consultation_id { get; set; }

        public int? service_type_id { get; set; }
    }
}