using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("theatre")]
    public class theatre
    {
        [Key] public Guid theatre_id { get; set; }

        [StringLength(250)] public string theatre_description { get; set; }

        public int? center_id { get; set; }
    }
}