using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class bank
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bank()
        {
            bank_transactions = new HashSet<bank_transactions>();
        }

        [Key] public Guid bank_id { get; set; }

        public string bank_name { get; set; }

        public string bank_branch { get; set; }

        public string bank_address { get; set; }

        public string bank_tel { get; set; }

        [StringLength(50)] public string bank_code { get; set; }

        [StringLength(50)] public string acc_type { get; set; }

        public string acc_number { get; set; }

        public string contact_person { get; set; }

        public string contact_person_tel { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public int? center_id { get; set; }

        [StringLength(250)] public string acc_name { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bank_transactions> bank_transactions { get; set; }
    }
}