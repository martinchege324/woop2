using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class medical_history
    {
        [Key] public Guid medical_history_id { get; set; }

        [StringLength(1)] public string value { get; set; }

        public DateTime? startdate { get; set; }

        public DateTime? enddate { get; set; }

        [StringLength(250)] public string comment { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public int? sub_history_id { get; set; }

        public int? history_id { get; set; }

        public Guid cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public DateTime? date { get; set; }

        public int? history_type_id { get; set; }
    }
}