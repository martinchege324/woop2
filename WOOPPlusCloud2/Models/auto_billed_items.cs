using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class auto_billed_items
    {
        [Key] public Guid auto_billed_item_id { get; set; }

        public string auto_billed_item_desc { get; set; }

        public string item_id { get; set; }

        [StringLength(50)] public string item_type { get; set; }

        public int? active { get; set; }

        public int? center_id { get; set; }

        public int? quantity { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public Guid? block_ward_id { get; set; }
    }
}