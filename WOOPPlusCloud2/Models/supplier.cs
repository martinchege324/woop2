using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class supplier
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public supplier()
        {
            supplier_trans = new HashSet<supplier_trans>();
        }

        [Key] public Guid supplier_id { get; set; }

        [Required] [StringLength(250)] public string supplier_name { get; set; }

        [StringLength(250)] public string supplier_address { get; set; }

        [StringLength(50)] public string supplier_contact { get; set; }

        public int? center_id { get; set; }

        [StringLength(50)] public string supplier_account { get; set; }

        [StringLength(250)] public string account_balance { get; set; }

        [StringLength(250)] public string total_invoice { get; set; }

        [StringLength(250)] public string total_payment { get; set; }

        public int? supplier_category_id { get; set; }

        public decimal? revenue_share { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? active { get; set; }

        public int? linked_staff_id { get; set; }

        [StringLength(250)] public string first_bank_name { get; set; }

        [StringLength(250)] public string first_acc_name { get; set; }

        [StringLength(250)] public string first_acc_no { get; set; }

        [StringLength(250)] public string second_bank_name { get; set; }

        [StringLength(250)] public string second_acc_name { get; set; }

        [StringLength(250)] public string second_acc_no { get; set; }

        [StringLength(250)] public string KRAPIN { get; set; }

        [StringLength(250)] public string id_number { get; set; }

        [StringLength(150)] public string supplier_contact_person { get; set; }

        public Guid? trans_id { get; set; }

        public virtual center center { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<supplier_trans> supplier_trans { get; set; }
    }
}