using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("category")]
    public class category
    {
        [Key] public int category_id { get; set; }

        [Required] [StringLength(250)] public string category_desc { get; set; }

        [StringLength(1)] public string Workflow_group { get; set; }

        public int category_count { get; set; }
    }
}