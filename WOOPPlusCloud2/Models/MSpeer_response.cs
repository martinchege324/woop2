using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class MSpeer_response
    {
        public int? request_id { get; set; }

        [Key] [Column(Order = 0)] public string peer { get; set; }

        [Key] [Column(Order = 1)] public string peer_db { get; set; }

        public DateTime? received_date { get; set; }
    }
}