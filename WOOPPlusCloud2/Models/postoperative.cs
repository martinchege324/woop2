using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("postoperative")]
    public class postoperative
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int postoperative_id { get; set; }

        [StringLength(250)] public string postoperative_name { get; set; }

        public int? postoperative_category_id { get; set; }
    }
}