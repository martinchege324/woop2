using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("province")]
    public class province
    {
        [Key] public int province_id { get; set; }

        [Required] [StringLength(100)] public string province_name { get; set; }

        public int county_count { get; set; }

        public int? nation_id { get; set; }
    }
}