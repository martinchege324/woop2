using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class log
    {
        [Key] public Guid log_id { get; set; }

        public DateTime? log_date { get; set; }

        public int? center_id { get; set; }

        [StringLength(50)] public string username { get; set; }

        [StringLength(50)] public string userpassword { get; set; }

        [StringLength(1)] public string successful { get; set; }

        [StringLength(1)] public string logtype { get; set; }

        [StringLength(250)] public string machinename { get; set; }
    }
}