using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("district")]
    public class district
    {
        [Key] public int district_id { get; set; }

        public int? district_number { get; set; }

        [StringLength(250)] public string district_name { get; set; }

        public int constituency_count { get; set; }

        public int? county_id { get; set; }
    }
}