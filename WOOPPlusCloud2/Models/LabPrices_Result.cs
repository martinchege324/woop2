//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WOOPPlusCloud2.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class LabPrices_Result
    {
        public string SAMPLE { get; set; }
        public string TEST { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> CORPORATEPRICE { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> CASHPRICE { get; set; }
        public Nullable<int> Active { get; set; }
        public string INSURANCE { get; set; }
        public string CENTER { get; set; }
        public Nullable<System.DateTime> DATECREATED { get; set; }
    }
}
