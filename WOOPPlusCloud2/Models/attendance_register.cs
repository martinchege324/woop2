using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class attendance_register
    {
        [Key] public Guid register_id { get; set; }

        public DateTime register_date { get; set; }

        [StringLength(250)] public string presentduration { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        [StringLength(1)] public string updated { get; set; }

        [StringLength(1)] public string staff_active { get; set; }

        [StringLength(250)] public string reason_for_excuse { get; set; }

        [Column(TypeName = "date")] public DateTime? leave_start_date { get; set; }

        [Column(TypeName = "date")] public DateTime? leave_end_date { get; set; }

        [StringLength(250)] public string attendance_status { get; set; }

        public int? training_id { get; set; }

        public int? staff_id { get; set; }

        public int? admin_id { get; set; }

        [StringLength(1)] public string staff_NO { get; set; }

        public int? center_id { get; set; }

        [StringLength(250)] public string comment { get; set; }
    }
}