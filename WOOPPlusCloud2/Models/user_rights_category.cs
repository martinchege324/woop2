using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class user_rights_category
    {
        [Key] public int right_category_id { get; set; }

        public string right_category_desc { get; set; }
    }
}