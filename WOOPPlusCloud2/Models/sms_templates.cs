using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sms_templates
    {
        [Key] public int sms_template_id { get; set; }

        [StringLength(1024)] public string description { get; set; }

        [StringLength(50)] public string name { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_date { get; set; }

        public Guid? tag_id { get; set; }

        public int? duration { get; set; }

        public int? active { get; set; }
    }
}