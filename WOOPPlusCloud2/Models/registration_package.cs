using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class registration_package
    {
        [Key] public Guid package_id { get; set; }

        [StringLength(255)] public string package_description { get; set; }

        public int? visit_count { get; set; }

        public string package_code { get; set; }

        public int? center_id { get; set; }

        public int? family_count { get; set; }
    }
}