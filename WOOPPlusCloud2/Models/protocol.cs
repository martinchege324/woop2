using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class protocol
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public protocol()
        {
            protocol_presciptions = new HashSet<protocol_presciptions>();
        }

        [Key] public Guid protocol_id { get; set; }

        [StringLength(50)] public string protocol_name { get; set; }

        public int? center_id { get; set; }

        public int? active { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<protocol_presciptions> protocol_presciptions { get; set; }
    }
}