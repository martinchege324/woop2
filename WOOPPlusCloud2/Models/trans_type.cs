using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class trans_type
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public trans_type()
        {
            trans_file = new HashSet<trans_file>();
        }

        [Key] public int trans_type_id { get; set; }

        [Required] [StringLength(250)] public string trans_type_desc { get; set; }

        [Required] [StringLength(1)] public string trans_type_sign { get; set; }

        public int? trans_type_count { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trans_file> trans_file { get; set; }
    }
}