using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class stock_order
    {
        [Key] public Guid stock_order_id { get; set; }

        public DateTime period { get; set; }

        public Guid center_product_id { get; set; }

        [Column(TypeName = "numeric")] public decimal os_balance { get; set; }

        [Column(TypeName = "numeric")] public decimal total_receipts { get; set; }

        [Column(TypeName = "numeric")] public decimal total_dispensed { get; set; }

        [Column(TypeName = "numeric")] public decimal adjustments { get; set; }

        [Column(TypeName = "numeric")] public decimal losses { get; set; }

        [Column(TypeName = "numeric")] public decimal cs_balance { get; set; }

        public int days_out_stock { get; set; }

        [Column(TypeName = "numeric")] public decimal order_quantity { get; set; }

        [Column(TypeName = "numeric")] public decimal order_cost { get; set; }

        [Required] [StringLength(20)] public string order_Ref { get; set; }

        public int center_id { get; set; }

        [Required] [StringLength(1)] public string cancelled { get; set; }

        [Required] [StringLength(1)] public string approved { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }
    }
}