using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class finance
    {
        [Key] public Guid finance_id { get; set; }

        public int unit_cost { get; set; }

        public int no_of_units { get; set; }

        public int total_cost { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        public int finance_type_item_id { get; set; }

        public int center_id { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string finance_item_desc { get; set; }

        public Guid? supplier_id { get; set; }
    }
}