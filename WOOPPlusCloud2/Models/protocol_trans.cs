using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class protocol_trans
    {
        [Key] public Guid protocol_trans_id { get; set; }

        public Guid? protocol_presciption_id { get; set; }

        public int cycle { get; set; }

        public DateTime? startdate { get; set; }

        public DateTime? expected_date { get; set; }

        public DateTime? actual_visit_date { get; set; }

        public int? completed { get; set; }

        public Guid? patient_id { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public string comments { get; set; }

        public int? active { get; set; }

        public DateTime? completed_on { get; set; }

        public DateTime? completed_by { get; set; }
    }
}