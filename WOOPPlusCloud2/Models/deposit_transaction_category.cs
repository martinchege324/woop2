using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class deposit_transaction_category
    {
        [Key] public Guid deposit_transaction_category_id { get; set; }

        [StringLength(250)] public string deposit_transaction_category_description { get; set; }

        public int? center_id { get; set; }
    }
}