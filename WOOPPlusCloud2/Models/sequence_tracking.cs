using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sequence_tracking
    {
        [Key] public Guid sequence_tracking_id { get; set; }

        public int? sequence_no { get; set; }

        public Guid? batch_id { get; set; }

        public Guid? location_id { get; set; }

        public decimal? initial_quantity { get; set; }

        public decimal? quantity { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }
    }
}