using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class protocol_presciptions
    {
        [Key] public Guid protocol_presciption_id { get; set; }

        public Guid? protocol_id { get; set; }

        public int cycles_prescribed { get; set; }

        public int cycles_completed { get; set; }

        public int? interval { get; set; }

        public string interval_desc { get; set; }

        public DateTime? startdate { get; set; }

        public Guid? patient_id { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public string comments { get; set; }

        public int? active { get; set; }

        public int? prescription_type_id { get; set; }

        [StringLength(250)] public string chemo_line { get; set; }

        public virtual protocol protocol { get; set; }
    }
}