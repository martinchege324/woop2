using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class finance_type_items
    {
        [Key] public int finance_type_item_id { get; set; }

        [StringLength(250)] public string finance_item_desc { get; set; }

        public int? finance_type_id { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        [StringLength(250)] public string AIE_Level_Code { get; set; }

        [StringLength(1)] public string income { get; set; }

        public int? item_active { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public int? center_id { get; set; }
    }
}