using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class uom_conversion
    {
        [Key] public int uom_conversion_id { get; set; }

        [Required] [StringLength(3)] public string from_uom_code { get; set; }

        [Required] [StringLength(3)] public string to_uom_code { get; set; }

        public int product_id { get; set; }

        public double uom_conversion_rate { get; set; }
    }
}