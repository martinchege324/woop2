using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class maternity_labour_management
    {
        [Key] public int maternity_labour_management_id { get; set; }

        [StringLength(250)] public string maternity_labour_management_desc { get; set; }

        public int? maternity_labour_management_category_id { get; set; }
    }
}