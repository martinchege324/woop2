using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class inpatient_feeding
    {
        [Key] public Guid inpatient_feeding_id { get; set; }

        public int? day_no { get; set; }

        public DateTime? admindate { get; set; }

        public DateTime? time { get; set; }

        [StringLength(250)] public string feed_type { get; set; }

        [StringLength(250)] public string route { get; set; }

        [StringLength(250)] public string volume { get; set; }

        [StringLength(250)] public string feeding_content { get; set; }

        [StringLength(250)] public string notes { get; set; }

        public int? staff_id { get; set; }

        public Guid? cycle_id { get; set; }

        [StringLength(250)] public string order_type { get; set; }

        [StringLength(1)] public string prescribed { get; set; }

        [StringLength(1)] public string administered { get; set; }

        public DateTime? prescribeddate { get; set; }

        public int? center_id { get; set; }

        public int? prescribed_by { get; set; }

        public int? administered_by { get; set; }
    }
}