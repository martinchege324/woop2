using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class los_sub_complications
    {
        [Key] public int los_sub_complication_id { get; set; }

        [StringLength(250)] public string los_sub_complication_desc { get; set; }

        public int? los_complication_id { get; set; }
    }
}