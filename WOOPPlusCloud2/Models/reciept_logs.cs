using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class reciept_logs
    {
        [Key] public Guid reciept_log_id { get; set; }

        public decimal? reciept_amount { get; set; }

        public string reciept_no { get; set; }

        public Guid? deposit_transaction_id { get; set; }

        public DateTime? printed_on { get; set; }

        public int? printed_by { get; set; }

        public int? center_id { get; set; }

        public decimal? total_bill { get; set; }
    }
}