using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class sage_parameters
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int center_id { get; set; }

        [Required] [StringLength(50)] public string serial_number { get; set; }

        [Required] [StringLength(50)] public string auth_key { get; set; }

        [Required] [StringLength(50)] public string sql_server_name { get; set; }

        [Required] [StringLength(50)] public string db_name { get; set; }

        [Required] [StringLength(50)] public string user_name { get; set; }

        [Required] [StringLength(50)] public string password { get; set; }

        [Required] [StringLength(50)] public string evolution_common_db_name { get; set; }
    }
}