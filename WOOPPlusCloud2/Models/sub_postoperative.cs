using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class sub_postoperative
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sub_postoperative_id { get; set; }

        [StringLength(250)] public string sub_postoperative_description { get; set; }

        public int? postoperative_id { get; set; }
    }
}