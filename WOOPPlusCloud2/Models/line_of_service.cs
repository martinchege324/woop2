using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class line_of_service
    {
        [Key] public int line_of_service_id { get; set; }

        [Required] [StringLength(250)] public string line_of_service_description { get; set; }

        [Required] [StringLength(1)] public string licensed { get; set; }
    }
}