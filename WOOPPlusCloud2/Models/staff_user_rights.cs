using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class staff_user_rights
    {
        [Key] public Guid staff_user_rights_id { get; set; }

        public int staff_id { get; set; }

        public int rights_id { get; set; }

        public int? center_id { get; set; }

        public int? admin_id { get; set; }

        public int? updatedby_id { get; set; }

        [StringLength(1)] public string allowed { get; set; }

        [StringLength(1)] public string licenced { get; set; }
    }
}