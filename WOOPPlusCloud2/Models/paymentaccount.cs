using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("paymentaccount")]
    public class paymentaccount
    {
        [Key] public int payment_account_id { get; set; }

        [StringLength(250)] public string payment_account_desc { get; set; }

        [StringLength(50)] public string payment_account_ref { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }
    }
}