using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class patient_allergies
    {
        [Key] public Guid patient_allergy_id { get; set; }

        public Guid? patient_id { get; set; }

        public int? allergy_id { get; set; }

        public string comment { get; set; }

        public int? active { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? center_id { get; set; }

        public virtual patient_allergies patient_allergies1 { get; set; }

        public virtual patient_allergies patient_allergies2 { get; set; }
    }
}