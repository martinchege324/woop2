using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class lab_test_specimens
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public lab_test_specimens()
        {
            lab_test_type = new HashSet<lab_test_type>();
        }

        [Key] public int lab_test_specimen_id { get; set; }

        [StringLength(250)] public string lab_test_specimen_desc { get; set; }

        public int? center_id { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<lab_test_type> lab_test_type { get; set; }
    }
}