using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class center_stock_price
    {
        [Key] public Guid center_stock_price_id { get; set; }

        public Guid center_product_id { get; set; }

        public Guid? supplier_id { get; set; }

        [Column("center_stock_price", TypeName = "numeric")]
        public decimal? center_stock_price1 { get; set; }

        public int? quantity { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }
    }
}