using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class staff_department
    {
        [Key] public Guid staff_department_id { get; set; }

        public Guid? hr_department_id { get; set; }

        public int? staff_id { get; set; }

        public int? rank { get; set; }

        [StringLength(1)] public string active { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }
    }
}