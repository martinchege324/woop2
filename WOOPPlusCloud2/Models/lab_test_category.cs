using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class lab_test_category
    {
        [Key] public int lab_test_categ_id { get; set; }

        [Required] [StringLength(250)] public string lab_test_categ_desc { get; set; }

        public int? lab_test_type_id { get; set; }

        public int? center_id { get; set; }
    }
}