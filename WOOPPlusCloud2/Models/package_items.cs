using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class package_items
    {
        [Key] public Guid package_item_id { get; set; }

        public string package_item_category { get; set; }

        public int? package_billable { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        public Guid? benefit_package_id { get; set; }

        public int? center_id { get; set; }
    }
}