using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("maternityapgscore")]
    public class maternityapgscore
    {
        [Key] public Guid maternityapgscore_id { get; set; }

        public Guid? cycle_id { get; set; }

        public int? babys_weight { get; set; }

        [StringLength(50)] public string pulse { get; set; }

        [StringLength(50)] public string grimace { get; set; }

        [StringLength(50)] public string appearance { get; set; }

        [StringLength(50)] public string respiration { get; set; }

        [StringLength(50)] public string status_of_baby { get; set; }

        [StringLength(50)] public string gender_of_baby { get; set; }

        [StringLength(150)] public string resuscitation { get; set; }

        [StringLength(150)] public string mode_of_delivery { get; set; }

        public int? total_score { get; set; }

        public int? secondtime { get; set; }

        public int? thirdtime { get; set; }

        public int? fourthtime { get; set; }

        public int? gestation_age { get; set; }

        [StringLength(350)] public string comment { get; set; }

        [StringLength(150)] public string doctor { get; set; }

        [StringLength(150)] public string midwife { get; set; }

        [StringLength(150)] public string disability { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        public int? staff_id { get; set; }
    }
}