using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class nursing_care
    {
        [Key] public Guid nursing_care_id { get; set; }

        public int? day_no { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        [StringLength(1)] public string walked { get; set; }

        [StringLength(1)] public string cleaned { get; set; }

        [StringLength(1)] public string changed_beddings { get; set; }

        [StringLength(250)] public string bowel_movement { get; set; }

        public int? urine_volume { get; set; }

        [StringLength(250)] public string notes { get; set; }

        public int? staff_id { get; set; }

        public Guid? cycle_id { get; set; }

        [StringLength(250)] public string output_type { get; set; }

        [StringLength(250)] public string output_amount { get; set; }

        [StringLength(250)] public string output_color { get; set; }

        [StringLength(250)] public string output { get; set; }

        public int? center_id { get; set; }

        [StringLength(250)] public string intake_type { get; set; }

        [StringLength(250)] public string intake_amount { get; set; }
    }
}