using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_case_type
    {
        [Key] public Guid sub_case_type_id { get; set; }

        public Guid? case_type_id { get; set; }

        [StringLength(250)] public string sub_case_type_desc { get; set; }

        public int? center_id { get; set; }
    }
}