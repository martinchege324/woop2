using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("partograph")]
    public class partograph
    {
        [Key] public Guid partograph_id { get; set; }

        public int? cervical_dilation { get; set; }

        public int? no_of_contractions { get; set; }

        public int? featal_heart_rate { get; set; }

        public int? duration_of_contration { get; set; }

        public DateTime? time { get; set; }

        public DateTime? date { get; set; }

        public int? updated { get; set; }

        [StringLength(36)] public string cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public int? descent_of_head { get; set; }
    }
}