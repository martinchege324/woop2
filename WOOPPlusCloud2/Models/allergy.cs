using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class allergy
    {
        [Key] public int allergy_id { get; set; }

        public string allergy_description { get; set; }

        public int? active { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? center_id { get; set; }
    }
}