using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class insurance_approved_items
    {
        [Key] public Guid insurance_approved_items_id { get; set; }

        public Guid? payment_account_trans_id { get; set; }

        public Guid? charge_item_id { get; set; }

        public DateTime? date { get; set; }
    }
}