using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("vat")]
    public class vat
    {
        [Key] [StringLength(1)] public string vatcode { get; set; }

        [StringLength(250)] public string vat_description { get; set; }

        [Column(TypeName = "numeric")] public decimal? vat_percentage { get; set; }

        [StringLength(1)] public string vat_active { get; set; }
    }
}