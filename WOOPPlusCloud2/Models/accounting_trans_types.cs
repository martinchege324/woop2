using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class accounting_trans_types
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public accounting_trans_types()
        {
            bank_transactions = new HashSet<bank_transactions>();
        }

        [Key] public int accounting_trans_type_id { get; set; }

        [StringLength(50)] public string accounting_trans_desc { get; set; }

        [StringLength(10)] public string trans_type_sign { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bank_transactions> bank_transactions { get; set; }
    }
}