using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class services_center_stock
    {
        [Key] public int service_center_stock_id { get; set; }

        public int? service_id { get; set; }

        public Guid? center_product_id { get; set; }
    }
}