using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_department_services
    {
        [Key] public Guid center_department_service_id { get; set; }

        public Guid? department_id { get; set; }

        public int? service_type_id { get; set; }

        public int? center_id { get; set; }

        public DateTime? created_date { get; set; }

        public DateTime? updated_date { get; set; }

        [StringLength(1)] public string perfom_direct { get; set; }

        [StringLength(1)] public string active { get; set; }
    }
}