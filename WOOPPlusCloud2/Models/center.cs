using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    [Table("center")]
    public class center
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public center()
        {
            bank_transactions = new HashSet<bank_transactions>();
            supplier_trans = new HashSet<supplier_trans>();
            suppliers = new HashSet<supplier>();
            surgery_details_trans = new HashSet<surgery_details_trans>();
            surgery_details_trans1 = new HashSet<surgery_details_trans>();
            trans_file = new HashSet<trans_file>();
            treatments = new HashSet<treatment>();
            visit_cycle = new HashSet<visit_cycle>();
            drug_dispatches = new HashSet<drug_dispatches>();
        }

        [Key] public int center_id { get; set; }

        [StringLength(250)] public string center_desc { get; set; }

        [StringLength(50)] public string center_reg_no { get; set; }

        [StringLength(50)] public string center_address { get; set; }

        [StringLength(50)] public string center_tel { get; set; }

        [StringLength(50)] public string center_town { get; set; }

        [StringLength(50)] public string center_village { get; set; }

        public int? Service_Number { get; set; }

        [Column(TypeName = "numeric")] public decimal? Service_Type { get; set; }

        [StringLength(100)] public string Health_Facility_Type { get; set; }

        [StringLength(100)] public string Status_Check { get; set; }

        public int? center_patient_counter { get; set; }

        [Column(TypeName = "numeric")] public decimal withdrawing_right { get; set; }

        public int? division_id { get; set; }

        [StringLength(250)] public string coordinates { get; set; }

        public int? patientPoints { get; set; }

        public int? points { get; set; }

        public int? billing_trans_id { get; set; }

        public int? receipt_counter { get; set; }

        public DateTime? financial_date { get; set; }

        [StringLength(50)] public string center_county { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        [StringLength(250)] public string facility_no_ref { get; set; }

        [StringLength(50)] public string email { get; set; }

        [StringLength(50)] public string PIN { get; set; }

        public byte is_sage_integrated { get; set; }

        [Column(TypeName = "image")] public byte[] image { get; set; }

        [Column(TypeName = "image")] public byte[] image1 { get; set; }

        public byte sage_sync_time { get; set; }

        public int is_syncing_on { get; set; }

        public int daily_patient_counter { get; set; }

        [Required] [StringLength(50)] public string center_currency { get; set; }

        [StringLength(250)] public string smart_group_practice_number { get; set; }

        public int? sample_serial_count { get; set; }

        public int? sms_count { get; set; }

        public int? first_shift_reportsent { get; set; }

        public int? second_shift_reportsent { get; set; }

        [StringLength(250)] public string facility_insurance_code { get; set; }

        [StringLength(250)] public string report_folder { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bank_transactions> bank_transactions { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<supplier_trans> supplier_trans { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<supplier> suppliers { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<surgery_details_trans> surgery_details_trans { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<surgery_details_trans> surgery_details_trans1 { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trans_file> trans_file { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<treatment> treatments { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<visit_cycle> visit_cycle { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<drug_dispatches> drug_dispatches { get; set; }
    }
}