using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class payment_accounts_transaction
    {
        [Key] [StringLength(36)] public string payment_account_trans_id { get; set; }

        [StringLength(36)] public string payment_account_id { get; set; }

        [StringLength(50)] public string payment_ref_no { get; set; }

        public double? invoice_amount { get; set; }

        [StringLength(36)] public string cycle_id { get; set; }

        [StringLength(250)] public string reviewed_by { get; set; }

        public DateTime? reviewed_date { get; set; }

        [StringLength(250)] public string review_comment { get; set; }

        public double? approved_amount { get; set; }

        [StringLength(250)] public string approved_by { get; set; }

        public DateTime? approval_date { get; set; }

        [StringLength(250)] public string approval_comment { get; set; }

        public int? invoice_submitted_by { get; set; }

        public int? invoice_submited_date { get; set; }
    }
}