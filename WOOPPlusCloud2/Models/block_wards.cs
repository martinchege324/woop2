using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class block_wards
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public block_wards()
        {
            ward_beds = new HashSet<ward_beds>();
        }

        [Key] public Guid block_ward_id { get; set; }

        [Required] [StringLength(250)] public string block_ward_desc { get; set; }

        public int? center_id { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ward_beds> ward_beds { get; set; }
    }
}