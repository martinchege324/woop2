using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class ward_beds
    {
        [Key] public Guid ward_bed_id { get; set; }

        [Required] [StringLength(250)] public string ward_bed_desc { get; set; }

        public Guid? block_ward_id { get; set; }

        [StringLength(1)] public string ward_bed_occupancy { get; set; }

        [StringLength(50)] public string item_code { get; set; }

        [StringLength(50)] public string account_id { get; set; }

        public int? center_id { get; set; }

        public decimal? regular_retail_price { get; set; }

        public int? isvatable { get; set; }

        [StringLength(150)] public string sage_cost_account { get; set; }

        public virtual block_wards block_wards { get; set; }
    }
}