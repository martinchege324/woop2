using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("losfollowup")]
    public class losfollowup
    {
        [Key] public int losfollowup_id { get; set; }

        [StringLength(250)] public string losfollowup_desc { get; set; }

        public int? losfollowup_category_id { get; set; }

        public int? center_id { get; set; }
    }
}