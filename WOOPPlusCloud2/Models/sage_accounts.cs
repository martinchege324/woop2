using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sage_accounts
    {
        [Key] public int account_id { get; set; }

        public int? account_link { get; set; }

        [StringLength(100)] public string name { get; set; }

        [StringLength(150)] public string account_no { get; set; }

        [StringLength(150)] public string account { get; set; }

        public int? account_type_id { get; set; }

        public int? sub_account_of_id { get; set; }

        public short? is_active { get; set; }
    }
}