using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class insurance_exclusions
    {
        [Key] public Guid insurance_exclusion_id { get; set; }

        public string insurance_exclusion_desc { get; set; }

        public Guid? patient_payment_account_type_id { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? center_id { get; set; }
    }
}