using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class referral_reasons
    {
        [Key] public int reason_id { get; set; }

        [StringLength(100)] public string reason_description { get; set; }

        public int? center_id { get; set; }
    }
}