using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class maternity_apgars_trans
    {
        [Key] public Guid maternity_apgars_trans_id { get; set; }

        public int? maternity_apgars_id { get; set; }

        public int? sub_maternity_apgars_id { get; set; }

        public DateTime? date { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? maternity_apgars_Category_id { get; set; }

        public Guid? child_patient_id { get; set; }
    }
}