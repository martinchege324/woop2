using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class visit_charges
    {
        [Key] public Guid charge_id { get; set; }

        public Guid cycle_id { get; set; }

        [Required] [StringLength(50)] public string charge_ref { get; set; }

        [Column(TypeName = "numeric")] public decimal charge_total { get; set; }

        public DateTime? date { get; set; }

        public int? visit_charge_category_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public int? charged_at_end { get; set; }

        public decimal? amount_paid { get; set; }

        public decimal? balance_due { get; set; }
    }
}