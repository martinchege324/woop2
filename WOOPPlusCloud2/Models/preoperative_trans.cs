using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class preoperative_trans
    {
        [Key] public Guid preoperativeTrans_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public int? preoperative_id { get; set; }

        public Guid? cycle_id { get; set; }

        public Guid? surgeries_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public int? sub_preoperative_id { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public DateTime? start_time { get; set; }

        public DateTime? end_time { get; set; }

        public int? selected_staff_id { get; set; }
    }
}