using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class payment_log
    {
        [Key] public Guid payment_log_id { get; set; }

        public Guid charge_item_id { get; set; }

        public Guid float_summary_id { get; set; }

        public DateTime? date_created { get; set; }

        public int? center_id { get; set; }
    }
}