using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_los_analysis
    {
        [Key] public int sub_los_analysis_id { get; set; }

        [StringLength(250)] public string sub_los_analysis_desc { get; set; }

        public int? los_analysis_id { get; set; }
    }
}