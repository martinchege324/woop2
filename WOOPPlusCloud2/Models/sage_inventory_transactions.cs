using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sage_inventory_transactions
    {
        [Key] public Guid sage_inventory_trans_id { get; set; }

        public Guid? trans_id { get; set; }

        public string invoice_no { get; set; }

        public decimal? total { get; set; }

        [StringLength(50)] public string sage_sale_order { get; set; }

        public DateTime? sage_posted_date { get; set; }

        public int? sage_posted_by { get; set; }

        public int? center_id { get; set; }
    }
}