using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class postoperative_category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int postoperative_category_id { get; set; }

        [StringLength(250)] public string postoperative_category_description { get; set; }
    }
}