using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class disease
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public disease()
        {
            symptoms_diagnosis = new HashSet<symptoms_diagnosis>();
            treatment_diagnosis = new HashSet<treatment_diagnosis>();
        }

        [Key] public int disease_id { get; set; }

        [StringLength(250)] public string disease_name { get; set; }

        public int? disease_category_id { get; set; }

        public int? center_id { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<symptoms_diagnosis> symptoms_diagnosis { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<treatment_diagnosis> treatment_diagnosis { get; set; }
    }
}