using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class client_loggin
    {
        [Key] public int client_loggin_id { get; set; }

        public int? client_loggin_role { get; set; }

        public Guid? userid { get; set; }

        public Guid? supplier_id { get; set; }

        public int? insurance_id { get; set; }

        public int? staff_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }
    }
}