using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_items_refund
    {
        [Key] public Guid center_items_refund_id { get; set; }

        public Guid cycle_id { get; set; }

        public Guid charge_item_id { get; set; }

        public Guid items_trans_id { get; set; }

        [Required] [StringLength(250)] public string item_description { get; set; }

        public double? quantity_requested { get; set; }

        public double? total_price_requested { get; set; }

        public double? quantity_refunded { get; set; }

        public double? total_price_refunded { get; set; }

        public int? center_id { get; set; }

        public int? requested_by { get; set; }

        public DateTime? requested_date { get; set; }

        public int? approved_by { get; set; }

        public DateTime? approved_date { get; set; }

        public int? rejected_by { get; set; }

        public DateTime? rejected_date { get; set; }

        [StringLength(250)] public string rejected_reason { get; set; }

        public int? active { get; set; }

        public string request_comment { get; set; }
    }
}