using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sage_failed_deposits
    {
        [Key] public int sage_failed_deposit_id { get; set; }

        public Guid deposit_transaction_id { get; set; }

        public int is_cash_transaction { get; set; }

        [Required] [StringLength(150)] public string reason_not_posted { get; set; }

        public int reposted { get; set; }

        public int center_id { get; set; }
    }
}