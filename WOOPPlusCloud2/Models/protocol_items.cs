using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class protocol_items
    {
        [Key] public Guid protocol_item_id { get; set; }

        public Guid? protocol_presciption_id { get; set; }

        public Guid? center_product_id { get; set; }

        public string prescription { get; set; }

        public int? quantity { get; set; }

        public string route { get; set; }

        public int? active { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public string comments { get; set; }

        public int? cycle { get; set; }

        public decimal? unit_price { get; set; }

        public decimal? total_price { get; set; }

        public int? interval { get; set; }

        [StringLength(250)] public string internal_desc { get; set; }

        public int? administered { get; set; }
    }
}