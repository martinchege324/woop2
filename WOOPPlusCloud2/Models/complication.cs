using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("complication")]
    public class complication
    {
        [Key] public Guid complication_id { get; set; }

        public DateTime? date { get; set; }

        public int? updated { get; set; }

        public int? los_complication_id { get; set; }

        public Guid? cycle_id { get; set; }

        public int? los_sub_complication_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string comment { get; set; }
    }
}