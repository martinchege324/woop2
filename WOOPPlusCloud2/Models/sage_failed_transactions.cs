using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sage_failed_transactions
    {
        [Key] public int sage_failed_transaction_id { get; set; }

        public Guid visit_cycle_id { get; set; }

        public int is_cash_transaction { get; set; }

        [Required] [StringLength(150)] public string reason_not_posted { get; set; }

        public int reposting_successful { get; set; }

        public int center_id { get; set; }

        public int cannot_repost { get; set; }

        public DateTime? date_failed { get; set; }

        public DateTime? date_reposted { get; set; }

        [StringLength(50)] public string sage_invoice_no { get; set; }
    }
}