using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class billing_type
    {
        [Key] public int billing_type_id { get; set; }

        [StringLength(250)] public string billing_type_description { get; set; }

        [StringLength(1)] public string billing_type_sign { get; set; }
    }
}