using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class scheduled_sms_settings
    {
        [Key] public long scheduled_sms_setting_id { get; set; }

        public long message_type_id { get; set; }

        public DateTime SendDateTime { get; set; }

        public int is_active { get; set; }

        public int center_id { get; set; }
    }
}