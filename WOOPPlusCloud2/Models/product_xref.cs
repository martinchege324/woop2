using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class product_xref
    {
        [Key] public int xref_id { get; set; }

        [StringLength(50)] public string xref_code { get; set; }

        [StringLength(250)] public string xref_description { get; set; }

        public decimal? xref_price { get; set; }

        public Guid? xref_supplier { get; set; }

        public int? product_id { get; set; }

        public int? xref_created_by { get; set; }

        public DateTime? xref_created_on { get; set; }

        public int? xref_edited_by { get; set; }

        public DateTime? xref_edited_on { get; set; }
    }
}