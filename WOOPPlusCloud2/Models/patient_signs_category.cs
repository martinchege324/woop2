using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class patient_signs_category
    {
        [Key] public int patient_sign_category_id { get; set; }

        public string patient_sign_category_description { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? edited_by { get; set; }
    }
}