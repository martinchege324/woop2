using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class visit_charge_category
    {
        [Key] public int visit_charge_category_id { get; set; }

        [StringLength(250)] public string visit_charge_category_description { get; set; }
    }
}