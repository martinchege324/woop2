using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class sms_parameters
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int center_id { get; set; }

        [Required] [StringLength(50)] public string com_port { get; set; }

        public int baud_rate { get; set; }

        public int time_out { get; set; }
    }
}