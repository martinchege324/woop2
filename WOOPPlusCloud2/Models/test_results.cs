using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class test_results
    {
        [Key] public Guid test_result_id { get; set; }

        public Guid test_id { get; set; }

        public Guid cycle_id { get; set; }

        public int sub_lab_test_id { get; set; }

        [StringLength(250)] public string labcomment { get; set; }

        [StringLength(250)] public string value { get; set; }

        [StringLength(1)] public string done { get; set; }

        public int? done_by { get; set; }

        public int? Verified { get; set; }

        public int? verified_by { get; set; }

        public DateTime? verified_date { get; set; }

        [StringLength(250)] public string verified_comment { get; set; }

        public int? cancelled_by { get; set; }

        public DateTime? cancelled_date { get; set; }

        [StringLength(250)] public string cancellation_comment { get; set; }

        public int? center_id { get; set; }

        public int? lab_test_id { get; set; }

        public DateTime? date { get; set; }

        public int? cancelled { get; set; }

        public int? updated { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        [StringLength(250)] public string ref_from { get; set; }

        [StringLength(250)] public string ref_to { get; set; }

        [StringLength(250)] public string critical_from { get; set; }

        [StringLength(250)] public string critical_to { get; set; }

        public int? isaboveref { get; set; }

        public int? iscritical { get; set; }

        public int? newversion_results { get; set; }
    }
}