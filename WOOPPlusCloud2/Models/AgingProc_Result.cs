//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WOOPPlusCloud2.Models
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class AgingProc_Result
    {
        public System.Guid cycle_id { get; set; }
        public string reffno { get; set; }
        public string payment_made_by_id { get; set; }
        public System.Guid deposit_id { get; set; }
        public Nullable<double> amount { get; set; }
        public string approved { get; set; }
        public Nullable<int> approved_by { get; set; }
        public Nullable<System.DateTime> date_approved { get; set; }
        public string cancelled { get; set; }
        public Nullable<int> cancelled_by { get; set; }
        public Nullable<System.DateTime> date_cancelled { get; set; }
        public string generated_pin { get; set; }
        public Nullable<int> center_id { get; set; }
        public Nullable<int> staff_id { get; set; }
        public string comment { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public Nullable<decimal> total_charge { get; set; }
        public Nullable<decimal> amount_paid { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> running_balance { get; set; }
        public Nullable<int> charged_at_end { get; set; }
        public Nullable<decimal> refund { get; set; }
        public Nullable<int> waived { get; set; }
        public Nullable<decimal> insurance_rebate { get; set; }
        public string invoice_number { get; set; }
        public Nullable<int> insurance_days { get; set; }
        public Nullable<decimal> requested_waiver_amount { get; set; }
        public Nullable<decimal> approved_waiver_amount { get; set; }
        public Nullable<int> request_waiver { get; set; }
        public Nullable<int> waiver_denied { get; set; }
        public Nullable<int> waiver_used { get; set; }
        public Nullable<int> invoice_closed { get; set; }
        public Nullable<int> invoice_split { get; set; }
        public Nullable<decimal> split_amount2 { get; set; }
        public Nullable<decimal> split_amount1 { get; set; }
        public Nullable<System.Guid> insurance_scheme1 { get; set; }
        public Nullable<System.Guid> insurance_scheme2 { get; set; }
        public Nullable<int> invoice_split_requested { get; set; }
        public Nullable<System.DateTime> rebate_date { get; set; }
        public Nullable<System.DateTime> invoice_closed_date { get; set; }
        public Nullable<byte> is_posted_to_sage { get; set; }
        public Nullable<decimal> invoice_closed_amount { get; set; }
        public Nullable<int> invoice_closed_by { get; set; }
        public string sage_order_no { get; set; }
        public Nullable<int> invoice_dispatched { get; set; }
        public Nullable<System.DateTime> dispatch_date { get; set; }
        public Nullable<int> dispatched_by { get; set; }
        public Nullable<int> offline_smart_post { get; set; }
        public Nullable<int> online_smart_post { get; set; }
        public Nullable<System.DateTime> online_smart_post_date { get; set; }
        public Nullable<System.DateTime> offline_smart_post_date { get; set; }
        public Nullable<int> invoice_paid { get; set; }
        public Nullable<System.DateTime> invoice_payment_date { get; set; }
        public Nullable<decimal> invoice_payment_enteredby { get; set; }
        public Nullable<int> invoice_rejected { get; set; }
        public Nullable<System.DateTime> invoice_rejection_date { get; set; }
        public Nullable<decimal> invoice_rejection_enteredby { get; set; }
        public string invoice_rejection_reason { get; set; }
        public Nullable<int> invoice_returned { get; set; }
        public Nullable<System.DateTime> invoice_returned_date { get; set; }
        public Nullable<decimal> invoice_returned_enteredby { get; set; }
        public string invoice_returned_reason { get; set; }
        public Nullable<decimal> invoice_paid_amount { get; set; }
        public string invoice_payment_reff { get; set; }
        public Nullable<int> cash_invoice_validated { get; set; }
        public Nullable<int> invoice_validated_by { get; set; }
        public Nullable<System.DateTime> invoice_validated_date { get; set; }
        public Nullable<decimal> initial_copay { get; set; }
        public Nullable<decimal> total_bill_copay { get; set; }
        public Nullable<int> delivery_disptached { get; set; }
        public Nullable<System.DateTime> delivery_dispatch_time { get; set; }
        public Nullable<int> delivery_dispatched_by { get; set; }
        public Nullable<int> claim_disptach_ready { get; set; }
        public Nullable<System.DateTime> claim_disptach_ready_date { get; set; }
        public Nullable<int> is_quotation { get; set; }
        public Nullable<System.DateTime> invoice_cancelled_date { get; set; }
        public Nullable<int> invoice_cancelled { get; set; }
        public Nullable<int> invoice_confirmed { get; set; }
        public Nullable<System.DateTime> invoice_confirmed_date { get; set; }
        public Nullable<int> invoice_confirmed_by { get; set; }
        public Nullable<decimal> invoice_confirmed_amount { get; set; }
        public Nullable<int> drug_delivered_by { get; set; }
        public Nullable<System.DateTime> drug_delivered_date { get; set; }
        public Nullable<int> drug_delivered { get; set; }
        public Nullable<decimal> invoice_rejection_amount { get; set; }
        public Nullable<int> invoice_unlocked { get; set; }
        public Nullable<System.DateTime> invoice_unlocked_date { get; set; }
        public Nullable<int> invoice_unlocked_enteredby { get; set; }
        public string invoice_cancelled_reason { get; set; }
        public Nullable<int> invoice_cancelled_enteredby { get; set; }
        public Nullable<int> amount_allocated { get; set; }
        public Nullable<int> sage_payment_posted { get; set; }
        public Nullable<decimal> sage_amount_paid { get; set; }
        public Nullable<int> facility_id { get; set; }
        public Nullable<System.Guid> insurance_payment_id { get; set; }
        public Nullable<decimal> exchange_rate { get; set; }
        public Nullable<System.DateTime> date_exchange_entered { get; set; }
        public Nullable<int> date_exchange_entered_by { get; set; }
        public Nullable<int> exchange_entered_by { get; set; }
        public Nullable<System.Guid> payer_id { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> TotalAmount { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> PAIDAMOUNT { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unallocated_amount { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> TOTALCHARGE { get; set; }
        public System.Guid patient_id { get; set; }
        public string names { get; set; }
        public string patient_facility_number { get; set; }
        public System.DateTime cycle_created_time { get; set; }
        public string account_no { get; set; }
        [DisplayName("Scheme")]
        public string patient_payment_account_type_desc { get; set; }
        public System.Guid patient_payment_account_type_id { get; set; }
        public System.Guid payment_account_id { get; set; }
        public string payment_account_desc { get; set; }
        public string patient_payment_account_id { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> summary_over_one_fifty { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> summary { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> summary_ninety { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> summary_sixty { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> summary_thirty { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,###0}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> summary_current { get; set; }

        string CENTER { get; set; }
    }
}
