using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class center_stock_price_change_log
    {
        [Key]
        [Column("center_stock_price_change_log")]
        public int center_stock_price_change_log1 { get; set; }

        public string product_code { get; set; }

        public Guid? center_product_id { get; set; }

        public string center_product_description { get; set; }

        public decimal? cost_price { get; set; }

        public decimal? product_price { get; set; }

        public decimal? corporate_price { get; set; }

        public decimal? other_price { get; set; }

        public decimal? regular_retail_price { get; set; }

        public decimal? trade_price { get; set; }

        public Guid? supplier_id { get; set; }

        public int? updatedby_staff_id { get; set; }

        public DateTime? update_date { get; set; }

        public int? center_id { get; set; }
    }
}