using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class diagnosi
    {
        [Key] public Guid diagnosis_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public int? disease_id { get; set; }

        public Guid? cycle_id { get; set; }

        public int? sub_disease_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string comment { get; set; }

        [StringLength(250)] public string disease_code { get; set; }

        [StringLength(1)] public string impression { get; set; }
    }
}