using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class invoice
    {
        [Key] public Guid invoice_id { get; set; }

        public string invoice_number { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public int? generated_number { get; set; }

        [StringLength(100)] public string reffno { get; set; }

        [StringLength(100)] public string preauth { get; set; }

        public int? invoice_closed { get; set; }

        public DateTime? invoice_closed_date { get; set; }

        public decimal? invoice_closed_amount { get; set; }

        public int? invoice_closed_by { get; set; }

        public int? invoice_split { get; set; }

        public decimal? split_amount2 { get; set; }

        public decimal? split_amount1 { get; set; }

        public Guid? insurance_scheme1 { get; set; }

        public Guid? insurance_scheme2 { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? paid { get; set; }

        public decimal? paid_amount { get; set; }

        public DateTime? payment_date { get; set; }

        public decimal? payment_enteredby { get; set; }
    }
}