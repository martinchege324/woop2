using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class test_sample_collection
    {
        [Key] public Guid test_sample_collection_id { get; set; }

        public Guid test_id { get; set; }

        public Guid cycle_id { get; set; }

        public DateTime? samplecollectiontime { get; set; }

        [StringLength(250)] public string samplecondition { get; set; }

        [StringLength(250)] public string samplelocation { get; set; }

        public int? samplecollectedby { get; set; }

        [StringLength(250)] public string cancellation_comment { get; set; }

        public int? cancelled_by { get; set; }

        public DateTime? cancelled_date { get; set; }

        public int? center_id { get; set; }

        public int? lab_test_id { get; set; }

        [StringLength(250)] public string sample { get; set; }

        public int? cancelled { get; set; }

        [StringLength(50)] public string sample_quantity { get; set; }

        [StringLength(250)] public string samplecomment { get; set; }

        [StringLength(50)] public string test_serial_no { get; set; }
    }
}