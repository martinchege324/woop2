using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class incision_type
    {
        [Key] public int incision_type_id { get; set; }

        [StringLength(250)] public string incision_type_desc { get; set; }
    }
}