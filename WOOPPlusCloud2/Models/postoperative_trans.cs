using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class postoperative_trans
    {
        [Key] public Guid postoperativeTrans_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public int? postoperative_id { get; set; }

        [StringLength(36)] public string cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public int? sub_postoperative_id { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public DateTime? start_time { get; set; }

        public DateTime? end_time { get; set; }

        public int? selected_staff_id { get; set; }

        public Guid? surgeries_id { get; set; }
    }
}