using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class insurance_transaction
    {
        [Key] public Guid payment_account_trans_id { get; set; }

        public Guid? payment_account_id { get; set; }

        public Guid? cycle_id { get; set; }

        [StringLength(250)] public string reviewed_by { get; set; }

        public DateTime? reviewed_date { get; set; }

        [StringLength(250)] public string review_comment { get; set; }

        [StringLength(250)] public string approved_by { get; set; }

        public DateTime? approval_date { get; set; }

        [StringLength(250)] public string approval_comment { get; set; }

        public decimal? approved_amount { get; set; }
    }
}