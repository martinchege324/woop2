using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class patient_treatment_category
    {
        [Key] public int patient_treatment_category_id { get; set; }

        [StringLength(250)] public string patient_treatment_category_desc { get; set; }

        public int? line_of_service_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }
    }
}