using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class staff_incidents_category
    {
        [Key] public Guid staff_incident_category_id { get; set; }

        [StringLength(100)] public string staff_incident_category_desc { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }
    }
}