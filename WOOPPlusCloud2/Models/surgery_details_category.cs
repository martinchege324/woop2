using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class surgery_details_category
    {
        [Key] public int surgery_details_categ_id { get; set; }

        [StringLength(250)] public string surgery_details_categ_desc { get; set; }
    }
}