using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_locations
    {
        [Key] public Guid location_id { get; set; }

        [StringLength(250)] public string location_description { get; set; }

        public int? center_id { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? edited_by { get; set; }

        [StringLength(1)] public string location_active { get; set; }

        public Guid? department_id { get; set; }
    }
}