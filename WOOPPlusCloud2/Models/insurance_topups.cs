using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class insurance_topups
    {
        [Key] public Guid insurance_topup_id { get; set; }

        public decimal? insurance_topup_amount { get; set; }

        public Guid? cycle_id { get; set; }

        public Guid invoice_id { get; set; }

        [StringLength(1)] public string cancelled { get; set; }

        public int? cancelled_by { get; set; }

        public DateTime? date_cancelled { get; set; }

        public DateTime? date_created { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public int? paid { get; set; }

        public DateTime? date_paid { get; set; }

        [StringLength(50)] public string receipt_no { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }
    }
}