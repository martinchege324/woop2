using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class surgery_visit_type
    {
        [Key] public int surgery_visit_type_id { get; set; }

        [StringLength(250)] public string surgery_visit_type_description { get; set; }
    }
}