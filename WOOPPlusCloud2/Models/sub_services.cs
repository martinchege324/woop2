using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_services
    {
        [Key] public int sub_service_id { get; set; }

        [Required] [StringLength(250)] public string sub_service_description { get; set; }

        public int? service_id { get; set; }

        public int? center_id { get; set; }
    }
}