using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("batch")]
    public class batch
    {
        [Key] public Guid batch_id { get; set; }

        [StringLength(150)] public string batch_no { get; set; }

        public string batch_sequence { get; set; }

        public Guid? center_product_id { get; set; }

        public Guid? location_id { get; set; }

        public int? center_id { get; set; }

        public Guid? supplier_id { get; set; }

        public DateTime? expiry_date { get; set; }

        public decimal? cost_price { get; set; }

        public decimal? selling_price { get; set; }

        public decimal? initial_quantity { get; set; }

        public decimal? quantity { get; set; }

        public int? active { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        [StringLength(50)] public string previous_batch_sequence { get; set; }

        [StringLength(10)] public string current__batch_sequence { get; set; }

        [StringLength(50)] public string recieving_Uom_name { get; set; }

        public string manufacturer { get; set; }

        public decimal? corporate_price { get; set; }

        public int? is_disposed { get; set; }

        [StringLength(250)] public string invoice_no { get; set; }

        public decimal? vat_ex_amount { get; set; }

        public decimal? vatvalue { get; set; }
    }
}