using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("history")]
    public class history
    {
        [Key] public int history_id { get; set; }

        [StringLength(250)] public string history_desc { get; set; }

        public int? history_type_id { get; set; }

        public int? center_id { get; set; }

        public int? history_group { get; set; }
    }
}