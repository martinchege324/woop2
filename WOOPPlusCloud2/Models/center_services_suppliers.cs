using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_services_suppliers
    {
        [Key] public Guid service_supplier_id { get; set; }

        public int? service_id { get; set; }

        public Guid? supplier_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? center_id { get; set; }

        public int? Active { get; set; }
    }
}