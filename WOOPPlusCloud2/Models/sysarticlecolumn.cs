using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class sysarticlecolumn
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int artid { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int colid { get; set; }

        public bool? is_udt { get; set; }

        public bool? is_xml { get; set; }

        public bool? is_max { get; set; }
    }
}