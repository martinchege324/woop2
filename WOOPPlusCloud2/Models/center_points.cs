using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_points
    {
        [Key] public Guid center_point_id { get; set; }

        [StringLength(250)] public string token_inputed { get; set; }

        public DateTime? input_date { get; set; }

        public int? center_id { get; set; }
    }
}