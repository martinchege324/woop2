using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class sms_contact_groups
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sms_contact_groups()
        {
            sms_contacts = new HashSet<sms_contacts>();
        }

        [Key] public Guid contact_group_id { get; set; }

        [Required] [StringLength(50)] public string name { get; set; }

        [StringLength(50)] public string description { get; set; }

        public int? active { get; set; }

        public int? center_id { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sms_contacts> sms_contacts { get; set; }
    }
}