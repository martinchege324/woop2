using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class sub_patient_treatment
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sub_patient_treatment()
        {
            treatments = new HashSet<treatment>();
        }

        [Key] public int sub_patient_treatment_id { get; set; }

        [StringLength(250)] public string sub_patient_treatment_desc { get; set; }

        public int? patient_treatment_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<treatment> treatments { get; set; }
    }
}