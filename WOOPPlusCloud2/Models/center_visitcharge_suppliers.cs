using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_visitcharge_suppliers
    {
        [Key] public Guid visitcharge_supplier_id { get; set; }

        public Guid? visit_charge_price_id { get; set; }

        public Guid? supplier_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? center_id { get; set; }

        public int? active { get; set; }
    }
}