using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class aspnet_Roles
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public aspnet_Roles()
        {
            aspnet_Users = new HashSet<aspnet_Users>();
        }

        public Guid ApplicationId { get; set; }

        [Key] public Guid RoleId { get; set; }

        [Required] [StringLength(256)] public string RoleName { get; set; }

        [Required] [StringLength(256)] public string LoweredRoleName { get; set; }

        [StringLength(256)] public string Description { get; set; }

        public virtual aspnet_Applications aspnet_Applications { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<aspnet_Users> aspnet_Users { get; set; }
    }
}