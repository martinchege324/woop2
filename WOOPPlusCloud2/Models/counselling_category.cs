using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class counselling_category
    {
        [Key] public int counselling_category_id { get; set; }

        [StringLength(250)] public string counselling_category_description { get; set; }
    }
}