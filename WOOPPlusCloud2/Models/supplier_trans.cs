using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class supplier_trans
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public supplier_trans()
        {
            supplier_payment_logs = new HashSet<supplier_payment_logs>();
        }

        [Key] public Guid supplier_trans_id { get; set; }

        public DateTime? supplier_trans_date { get; set; }

        public int? supplier_trans_type_id { get; set; }

        [Column(TypeName = "numeric")] public decimal? supplier_trans_amount { get; set; }

        [StringLength(250)] public string supplier_trans_comment { get; set; }

        [StringLength(50)] public string supplier_trans_ref { get; set; }

        public Guid? supplier_id { get; set; }

        public int? center_id { get; set; }

        public Guid? payment_method_id { get; set; }

        [StringLength(250)] public string items_trans_id { get; set; }

        [StringLength(250)] public string item_type { get; set; }

        [StringLength(250)] public string full_item_description { get; set; }

        public decimal? quantity { get; set; }

        public int? active { get; set; }

        public DateTime? updated_date { get; set; }

        public int? updated_by { get; set; }

        public decimal? vat_ex_amount { get; set; }

        public decimal? vatvalue { get; set; }

        public decimal? amount_paid { get; set; }

        [StringLength(150)] public string supplier_payment_ref { get; set; }

        public DateTime? supplier_payment_date { get; set; }

        public int? paid { get; set; }

        public virtual center center { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<supplier_payment_logs> supplier_payment_logs { get; set; }

        public virtual supplier_trans_type supplier_trans_type { get; set; }

        public virtual supplier_trans_type supplier_trans_type1 { get; set; }

        public virtual supplier supplier { get; set; }

        public virtual supplier_trans_type supplier_trans_type2 { get; set; }
    }
}