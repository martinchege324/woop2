using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class trans_file
    {
        [Key] public Guid trans_id { get; set; }

        public DateTime? trans_date { get; set; }

        [Required] [StringLength(100)] public string trans_reference { get; set; }

        public int center_id { get; set; }

        public int trans_type_id { get; set; }

        [Required] [StringLength(3)] public string uom_code { get; set; }

        public double? trans_quantity { get; set; }

        public double? trans_base_quantity { get; set; }

        [StringLength(150)] public string batch_no { get; set; }

        public DateTime? expiry_date { get; set; }

        [StringLength(250)] public string trans_comment { get; set; }

        public int product_id { get; set; }

        public Guid? center_product_id { get; set; }

        [Required] [StringLength(1)] public string complete { get; set; }

        public Guid? cycle_id { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string prescription { get; set; }

        public Guid? supplier_id { get; set; }

        public decimal? unit_price { get; set; }

        public decimal? total { get; set; }

        public Guid? location_stock_id { get; set; }

        [StringLength(1)] public string cancelled { get; set; }

        [StringLength(1)] public string approved { get; set; }

        [StringLength(1)] public string vatcode { get; set; }

        public decimal? vatvalue { get; set; }

        public Guid? batch_id { get; set; }

        [StringLength(50)] public string batch_sequence { get; set; }

        public int? paid { get; set; }

        public Guid? second_location_id { get; set; }

        public int? substituded { get; set; }

        public Guid? substite_trans_id { get; set; }

        public int? substituded_by { get; set; }

        public DateTime? substitution_date { get; set; }

        public decimal? current_stock { get; set; }

        public int? cancelled_by { get; set; }

        public DateTime? cancelled_date { get; set; }

        public int? discharge_drug { get; set; }

        [StringLength(250)] public string invoice_no { get; set; }

        public int? proforma { get; set; }

        public decimal? savings { get; set; }

        public int? refund_requested { get; set; }

        public decimal? discount { get; set; }

        public decimal? balance_quantity { get; set; }

        public DateTime? item_created_date { get; set; }

        public int? item_created_by { get; set; }

        public int? stock_take { get; set; }

        public int? isinpatient { get; set; }

        public int? isinsurance { get; set; }

        public int? days { get; set; }

        public decimal? cost_price { get; set; }

        public int? concierge { get; set; }

        public int? refill { get; set; }

        public int? refill_reminder_sent { get; set; }

        public DateTime? refill_reminder_sent_date { get; set; }

        public decimal? vat_ex_amount { get; set; }

        public decimal? regular_retail_price { get; set; }

        public decimal? available_quantity { get; set; }

        [StringLength(250)] public string sage_sale_order { get; set; }

        public DateTime? sage_posted_date { get; set; }

        public int? sage_sale_posted { get; set; }

        public decimal? packs { get; set; }

        public virtual center center { get; set; }

        public virtual center_stock center_stock { get; set; }

        public virtual stock_f stock_f { get; set; }

        public virtual visit_cycle visit_cycle { get; set; }

        public virtual trans_type trans_type { get; set; }

        public virtual uom uom { get; set; }
    }
}