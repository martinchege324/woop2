using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class patient_signs
    {
        [Key] public int patient_sign_id { get; set; }

        [StringLength(250)] public string patient_sign_description { get; set; }

        public int? patient_sign_category_id { get; set; }

        public int? counter { get; set; }

        public int? center_id { get; set; }
    }
}