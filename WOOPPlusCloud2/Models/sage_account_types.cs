using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class sage_account_types
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int account_type_id { get; set; }

        [Required] [StringLength(100)] public string name { get; set; }

        public short is_balance_sheet { get; set; }

        public short is_debit { get; set; }
    }
}