using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class staff_leave
    {
        [Key] public Guid staff_leave_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string reason { get; set; }

        [StringLength(50)] public string staff_leave_type { get; set; }

        public DateTime? start_date { get; set; }

        public DateTime? end_date { get; set; }

        [StringLength(250)] public string authorised_by { get; set; }

        [StringLength(1)] public string cancelled { get; set; }

        [StringLength(250)] public string cancelled_by { get; set; }

        public DateTime? cancelled_on { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_on { get; set; }

        public DateTime? updated_by { get; set; }
    }
}