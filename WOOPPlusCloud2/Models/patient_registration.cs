using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class patient_registration
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public patient_registration()
        {
            visit_cycle = new HashSet<visit_cycle>();
            patient_tags = new HashSet<patient_tags>();
        }

        [Key] public Guid patient_id { get; set; }

        [StringLength(50)] public string patient_first_name { get; set; }

        [StringLength(50)] public string patient_middle_name { get; set; }

        [StringLength(50)] public string patient_last_name { get; set; }

        [StringLength(10)] public string gender { get; set; }

        public DateTime? dob { get; set; }

        [StringLength(50)] public string patient_tel { get; set; }

        [StringLength(50)] public string patient_address { get; set; }

        [StringLength(50)] public string patient_postal_code { get; set; }

        [Required] [StringLength(50)] public string patient_number { get; set; }

        [StringLength(50)] public string patient_fathers_name { get; set; }

        [StringLength(50)] public string patient_mothers_name { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        [StringLength(250)] public string id_no { get; set; }

        [StringLength(20)] public string nhif_no { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(250)] public string patient_guardians_name { get; set; }

        [StringLength(250)] public string patient_next_of_kin { get; set; }

        public DateTime? death_date { get; set; }

        public DateTime? time_of_death { get; set; }

        [StringLength(250)] public string next_of_kin_relationship { get; set; }

        [StringLength(250)] public string religion { get; set; }

        [StringLength(250)] public string nationality { get; set; }

        [StringLength(250)] public string email { get; set; }

        [StringLength(250)] public string kin_telephone { get; set; }

        [StringLength(250)] public string kin_residence { get; set; }

        public Guid? mother_patient_id { get; set; }

        [StringLength(250)] public string birth_cert { get; set; }

        [StringLength(250)] public string other_id { get; set; }

        [Column(TypeName = "image")] public byte[] image { get; set; }

        [StringLength(100)] public string patient_facility_number { get; set; }

        [StringLength(250)] public string alt_kin_telephone { get; set; }

        [StringLength(250)] public string emg_contact_name { get; set; }

        [StringLength(250)] public string emg_contact_telephone { get; set; }

        [StringLength(250)] public string emg_contact_alt_telephone { get; set; }

        [StringLength(250)] public string emg_contact_residence { get; set; }

        [StringLength(250)] public string emg_contact_relationship { get; set; }

        [StringLength(250)] public string unique_gov_no { get; set; }

        [StringLength(250)] public string mothers_patient_facility_id { get; set; }

        [StringLength(250)] public string alt_telephone { get; set; }

        public int? OPK { get; set; }

        [StringLength(250)] public string kin_id_no { get; set; }

        [StringLength(250)] public string emg_contact_id_no { get; set; }

        public int? updated_by { get; set; }

        public DateTime? update_date { get; set; }

        [StringLength(100)] public string citytown { get; set; }

        [StringLength(200)] public string occupation { get; set; }

        public Guid? family_id { get; set; }

        public byte? is_family_head { get; set; }

        [StringLength(45)] public string dependant_relationship { get; set; }

        [StringLength(250)] public string inpatient_number { get; set; }

        public int? outreach { get; set; }

        [StringLength(250)] public string outreach_location { get; set; }

        public int? patient_status { get; set; }

        public string delivery_location { get; set; }

        public int? active { get; set; }

        public int? deactivated_by { get; set; }

        public DateTime? deactivated_on { get; set; }

        [StringLength(250)] public string distance_travelled { get; set; }

        [StringLength(250)] public string has_smartphone { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<visit_cycle> visit_cycle { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<patient_tags> patient_tags { get; set; }
    }
}