using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class vital
    {
        [Key] public Guid vitals_id { get; set; }

        public int? respiratory_rate { get; set; }

        [StringLength(50)] public string bp { get; set; }

        public double? height { get; set; }

        public double? weight { get; set; }

        public double? age { get; set; }

        public double? temperature { get; set; }

        public int? pulse { get; set; }

        public Guid cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public DateTime? date { get; set; }

        public decimal? oxygen_saturation { get; set; }

        public decimal? bmi { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public DateTime? lmp { get; set; }

        [StringLength(250)] public string edd { get; set; }

        public int? parity { get; set; }

        public int? gravidae { get; set; }

        public int? miscarriages { get; set; }

        public int? abortion { get; set; }

        public int? head_circumference { get; set; }

        [StringLength(250)] public string edd_uncertain { get; set; }

        public int? uncertain { get; set; }

        [StringLength(250)] public string visual_acuity { get; set; }

        [StringLength(250)] public string right_eye { get; set; }

        [StringLength(250)] public string left_eye { get; set; }

        public decimal? BSA { get; set; }

        public int? in_triage { get; set; }

        public decimal? HBA1C { get; set; }

        public decimal? FBS { get; set; }

        public decimal? RBS { get; set; }

        public decimal? waist_circumference { get; set; }

        public decimal? hip_circumference { get; set; }

        public decimal? body_fat { get; set; }

        public decimal? body_water { get; set; }

        public decimal? subcutaneous_fat { get; set; }

        public decimal? metabolic_age { get; set; }

        public decimal? bone_density { get; set; }

        public decimal? visceral_fat { get; set; }

        public decimal? skeletal_muscle { get; set; }

        public decimal? fat_free { get; set; }

        public decimal? bmr { get; set; }

        public virtual visit_cycle visit_cycle { get; set; }
    }
}