using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class referral
    {
        [Key] public Guid referral_id { get; set; }

        [StringLength(1)] public string referred { get; set; }

        public DateTime? next_visit_date { get; set; }

        public int? reason_id { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id_transferred_to { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string ambulance_comment { get; set; }

        [StringLength(50)] public string ambulance_no { get; set; }

        [StringLength(250)] public string referral_reason { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public DateTime? date { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public Guid? consultation_id { get; set; }

        public int? service_type_id { get; set; }

        public int? approved { get; set; }

        public int? approved_by { get; set; }

        public DateTime? approved_on { get; set; }

        public int? appointment_doctor { get; set; }

        public Guid? patient_id { get; set; }

        [StringLength(250)] public string center_transferred_to_desc { get; set; }

        public int? reminder_sent { get; set; }

        public DateTime? reminder_sent_date { get; set; }
    }
}