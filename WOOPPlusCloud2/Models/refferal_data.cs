using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class refferal_data
    {
        [Key] public Guid referral_data_id { get; set; }

        public Guid? referral_category_id { get; set; }

        public string referral_data_desc { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? center_id { get; set; }
    }
}