using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class administer_drug
    {
        [Key] public Guid administer_id { get; set; }

        public DateTime? adm_date { get; set; }

        public int? center_id { get; set; }

        public Guid? center_product_id { get; set; }

        public int? quanity { get; set; }

        public int? staff_id { get; set; }

        [StringLength(1)] public string prescribed { get; set; }

        public Guid? cycle_id { get; set; }

        [StringLength(250)] public string comment { get; set; }
    }
}