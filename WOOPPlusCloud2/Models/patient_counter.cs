using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class patient_counter
    {
        [Key] public Guid patient_counter_id { get; set; }

        public int? billing_trans_id { get; set; }

        [StringLength(255)] public string counter { get; set; }

        public Guid? cycle_id { get; set; }

        [StringLength(255)] public string bill_check { get; set; }

        public int? center_id { get; set; }
    }
}