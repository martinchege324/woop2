using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class treatment_diagnosis
    {
        [Key] public int treatment_diagnosis_id { get; set; }

        public int? product_id { get; set; }

        public int? disease_id { get; set; }

        public int? center_id { get; set; }

        public virtual disease disease { get; set; }

        public virtual stock_f stock_f { get; set; }
    }
}