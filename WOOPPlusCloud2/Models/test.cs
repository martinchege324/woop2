using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("test")]
    public class test
    {
        [Key] public Guid test_id { get; set; }

        [StringLength(1)] public string test_internal { get; set; }

        public int? lab_test_id { get; set; }

        [StringLength(1)] public string result_positive { get; set; }

        public int? staff_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        [StringLength(250)] public string value { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public Guid? cycle_id { get; set; }

        public int? sub_lab_test_id { get; set; }

        [StringLength(1)] public string done { get; set; }

        public int? center_id { get; set; }

        public int? lab_test_categ_id { get; set; }

        [StringLength(250)] public string sample { get; set; }

        public DateTime? requestdate { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public int? done_by { get; set; }

        public DateTime? samplecollectiontime { get; set; }

        public int? samplecollectedby { get; set; }

        [StringLength(250)] public string samplecondition { get; set; }

        [StringLength(250)] public string samplelocation { get; set; }

        public int? request_center { get; set; }

        public int? paid { get; set; }

        [StringLength(250)] public string samplecomment { get; set; }

        [StringLength(250)] public string labcomment { get; set; }

        [StringLength(50)] public string sample_quantity { get; set; }

        [StringLength(1)] public string test_cancelled { get; set; }

        public Guid? consultation_id { get; set; }

        public int? external_test { get; set; }

        [StringLength(250)] public string external_facility { get; set; }

        public int? entered_by { get; set; }

        public int? fromexternal_test { get; set; }

        [StringLength(250)] public string fromexternal_facility { get; set; }

        public Guid? supplier_id { get; set; }

        [StringLength(250)] public string cancellation_comment { get; set; }

        public int? Verified { get; set; }

        public int? verified_by { get; set; }

        public DateTime? verified_date { get; set; }

        [StringLength(250)] public string verified_comment { get; set; }

        public int? cancelled_by { get; set; }

        public DateTime? cancelled_date { get; set; }

        public int? proforma { get; set; }

        public DateTime? item_created_date { get; set; }

        public int? item_created_by { get; set; }

        public decimal? unit_price { get; set; }

        public decimal? total { get; set; }

        public int? isinpatient { get; set; }

        public int? isinsurance { get; set; }

        public int? service_type_id { get; set; }

        public decimal? regular_retail_price { get; set; }

        public decimal? savings { get; set; }

        public decimal? vat_amount { get; set; }
    }
}