using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class scheme_insurance_types
    {
        [Key] public Guid scheme_insurance_type_id { get; set; }

        [StringLength(250)] public string scheme_insurance_type_desc { get; set; }

        public Guid? patient_payment_account_type_id { get; set; }

        public decimal? consultation_copay { get; set; }

        public decimal? totalbill_copay { get; set; }

        public int? insurance_type_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? active { get; set; }
    }
}