using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class que_details
    {
        [Key] public int que_details_id { get; set; }

        [StringLength(100)] public string que_details_description { get; set; }
    }
}