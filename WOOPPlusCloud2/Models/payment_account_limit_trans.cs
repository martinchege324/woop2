using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class payment_account_limit_trans
    {
        [Key] public Guid payment_account_limit_trans_id { get; set; }

        public Guid? payment_account_limit_type_id { get; set; }

        public Guid? patient_payment_account_id { get; set; }

        public decimal? limit_amount { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? edited_by { get; set; }

        public int? insurance_type_id { get; set; }
    }
}