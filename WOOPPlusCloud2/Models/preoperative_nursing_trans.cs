using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class preoperative_nursing_trans
    {
        [Key] public Guid preoperative_nursing_trans_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public int? preoperative_nursing_id { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public int? sub_preoperative_nursing_id { get; set; }
    }
}