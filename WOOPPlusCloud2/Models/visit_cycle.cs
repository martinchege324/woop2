using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class visit_cycle
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public visit_cycle()
        {
            surgery_details_trans = new HashSet<surgery_details_trans>();
            surgery_details_trans1 = new HashSet<surgery_details_trans>();
            surgical_team = new HashSet<surgical_team>();
            trans_file = new HashSet<trans_file>();
            treatments = new HashSet<treatment>();
            vitals = new HashSet<vital>();
        }

        [Key] public Guid cycle_id { get; set; }

        public Guid patient_id { get; set; }

        public int center_id { get; set; }

        [StringLength(1)] public string cycle_completed { get; set; }

        public DateTime cycle_created_time { get; set; }

        [StringLength(1)] public string pending_page_title { get; set; }

        [Required] [StringLength(1)] public string re_visit { get; set; }

        public int visit_number { get; set; }

        public int visit_type { get; set; }

        public int? staff_id { get; set; }

        public DateTime? cycle_closed_time { get; set; }

        [StringLength(100)] public string chw_name { get; set; }

        [StringLength(1)] public string over_five { get; set; }

        public int? line_of_service_id { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        [StringLength(20)] public string chw_cell_no { get; set; }

        [StringLength(50)] public string patient_facility_number { get; set; }

        [StringLength(50)] public string patient_support_number { get; set; }

        [StringLength(50)] public string marital_status { get; set; }

        [StringLength(50)] public string occupation { get; set; }

        [StringLength(50)] public string education_level { get; set; }

        [StringLength(50)] public string proffession { get; set; }

        [StringLength(50)] public string telephone { get; set; }

        [StringLength(50)] public string location { get; set; }

        [StringLength(50)] public string sub_location { get; set; }

        [StringLength(50)] public string village { get; set; }

        [StringLength(50)] public string distance_travelled { get; set; }

        public int? center_id_transferred_from { get; set; }

        public int? sub_que_details_id { get; set; }

        [StringLength(250)] public string ambulance_comment { get; set; }

        [StringLength(50)] public string ambulance_no { get; set; }

        [StringLength(1)] public string case_under_review { get; set; }

        public int? case_sent_to_staff_id { get; set; }

        public Guid? case_type { get; set; }

        public DateTime? admission_date { get; set; }

        public DateTime? discharge_date { get; set; }

        public decimal? stay_duration { get; set; }

        [StringLength(1)] public string new_to_los { get; set; }

        [StringLength(1)] public string inpatient { get; set; }

        [StringLength(1)] public string discharged { get; set; }

        [StringLength(250)] public string discharge_mode { get; set; }

        public DateTime? consultation_start_time { get; set; }

        public DateTime? consultation_end_time { get; set; }

        [StringLength(1)] public string skip_triage { get; set; }

        public int? points { get; set; }

        public int? billing_trans_id { get; set; }

        public decimal? payment_method_id { get; set; }

        public Guid? payer_id { get; set; }

        public Guid? benefit_package_id { get; set; }

        public int? patient_daily_number { get; set; }

        public Guid? payer_id2 { get; set; }

        public int? insurance1 { get; set; }

        public int? insurance2 { get; set; }

        [StringLength(250)] public string admission_type { get; set; }

        public int? admited_by { get; set; }

        public int? discharged_by { get; set; }

        public int? cycle_completed_by { get; set; }

        public DateTime? cycle_completed_date { get; set; }

        public int? copay { get; set; }

        public DateTime? doctor_discharge_date { get; set; }

        [StringLength(250)] public string doctor_discharge_mode { get; set; }

        public int? doctor_discharged_by { get; set; }

        public DateTime? doctor_admission_date { get; set; }

        public int? admitting_doctor { get; set; }

        [StringLength(250)] public string referral_type { get; set; }

        [StringLength(250)] public string referring_entity { get; set; }

        public int? benefit_package_active { get; set; }

        public int? updated_by { get; set; }

        public int? new_package { get; set; }

        public int? reminder_sent { get; set; }

        public DateTime? reminder_sent_date { get; set; }

        public int? concierge { get; set; }

        public int? active { get; set; }

        public int? deactivated_by { get; set; }

        public DateTime? deactivated_on { get; set; }

        [StringLength(250)] public string referral_location { get; set; }

        public virtual center center { get; set; }

        public virtual patient_registration patient_registration { get; set; }

        public virtual staff staff { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<surgery_details_trans> surgery_details_trans { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<surgery_details_trans> surgery_details_trans1 { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<surgical_team> surgical_team { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trans_file> trans_file { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<treatment> treatments { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vital> vitals { get; set; }
    }
}