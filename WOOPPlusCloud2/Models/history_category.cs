using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class history_category
    {
        [Key] public int history_category_id { get; set; }

        [Required] [StringLength(250)] public string history_category_description { get; set; }

        public int? line_of_service_id { get; set; }

        public int? center_id { get; set; }

        [StringLength(50)] public string bgcolor { get; set; }
    }
}