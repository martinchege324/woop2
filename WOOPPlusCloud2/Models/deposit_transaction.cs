using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class deposit_transaction
    {
        [Key] public Guid deposit_transaction_id { get; set; }

        public Guid? deposit_id { get; set; }

        public int? staff_id { get; set; }

        public Guid? deposit_transaction_category_id { get; set; }

        public Guid? cycle_id { get; set; }

        [StringLength(1)] public string cancelled { get; set; }

        public int? cancelled_by { get; set; }

        public DateTime? date_cancelled { get; set; }

        public DateTime? date_created { get; set; }

        [StringLength(250)] public string comment { get; set; }

        [StringLength(1)] public string approved { get; set; }

        public int? approved_by { get; set; }

        public DateTime? date_approved { get; set; }

        public Guid? payment_method_id { get; set; }

        public decimal? deposit_transaction_amount { get; set; }

        [StringLength(50)] public string payment_made_by_id { get; set; }

        [StringLength(50)] public string receipt_no { get; set; }

        public decimal? conversion_factor { get; set; }

        public decimal? initial_transaction_amount { get; set; }

        public Guid? payment_method2_id { get; set; }

        public decimal? payment_amount1 { get; set; }

        public decimal? payment_amount2 { get; set; }

        public Guid? benefit_package_id { get; set; }

        [StringLength(250)] public string deposit_comment { get; set; }

        public int? center_id { get; set; }

        public int? proforma { get; set; }

        public DateTime? item_created_date { get; set; }
    }
}