using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class supplier_payment_logs
    {
        [Key] public Guid supplier_payment_log_id { get; set; }

        public Guid? supplier_payment_trans_id { get; set; }

        public Guid? supplier_trans_id { get; set; }

        public decimal? amount_paid { get; set; }

        public DateTime? item_created_date { get; set; }

        public int item_created_by { get; set; }

        public virtual supplier_payments supplier_payments { get; set; }

        public virtual supplier_trans supplier_trans { get; set; }
    }
}