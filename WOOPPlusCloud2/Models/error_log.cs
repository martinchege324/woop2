using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class error_log
    {
        [Key] public Guid error_log_id { get; set; }

        public string error_desc { get; set; }

        public string module { get; set; }

        public int? center_id { get; set; }

        public DateTime? recorded_date { get; set; }

        public int? recorded_by { get; set; }
    }
}