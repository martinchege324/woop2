using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    [Table("uom")]
    public class uom
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public uom()
        {
            trans_file = new HashSet<trans_file>();
        }

        [Key] [StringLength(3)] public string Uom_code { get; set; }

        [StringLength(50)] public string Uom_name { get; set; }

        [StringLength(1)] public string uom_favorite { get; set; }

        public int? generated_no { get; set; }

        public int? center_id { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trans_file> trans_file { get; set; }
    }
}