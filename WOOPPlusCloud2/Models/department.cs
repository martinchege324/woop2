using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("department")]
    public class department
    {
        [Key] public Guid department_id { get; set; }

        [StringLength(250)] public string department_desc { get; set; }

        public int? center_id { get; set; }

        public DateTime? date { get; set; }

        [StringLength(250)] public string department_code { get; set; }

        public int? Active { get; set; }

        public int? visible { get; set; }
    }
}