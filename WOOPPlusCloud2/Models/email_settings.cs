using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class email_settings
    {
        [Key] public int email_setting_id { get; set; }

        [StringLength(50)] public string deafult_email { get; set; }

        [StringLength(250)] public string password { get; set; }

        public DateTime? created_date { get; set; }

        [StringLength(50)] public string created_by { get; set; }

        public DateTime? edited_date { get; set; }

        [StringLength(50)] public string edited_by { get; set; }
    }
}