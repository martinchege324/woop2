using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class staff_shift
    {
        [Key] public Guid staff_shift_id { get; set; }

        public Guid? department_shift_id { get; set; }

        public Guid? staff_department_id { get; set; }

        [StringLength(1)] public string shift_completed { get; set; }

        public DateTime? shift_date { get; set; }

        [StringLength(250)] public string prepared_by { get; set; }

        [StringLength(1)] public string shift_cancelled { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }
    }
}