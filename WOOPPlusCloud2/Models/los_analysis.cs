using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class los_analysis
    {
        [Key] public int los_analysis_id { get; set; }

        [StringLength(250)] public string los_analysis_desc { get; set; }

        public int? los_analysis_type_id { get; set; }
    }
}