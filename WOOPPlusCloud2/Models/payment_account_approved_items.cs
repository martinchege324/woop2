using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class payment_account_approved_items
    {
        [Key] [StringLength(36)] public string payement_account_approved_item_id { get; set; }

        [StringLength(36)] public string patient_payment_account_transaction_id { get; set; }

        [StringLength(36)] public string charge_item_id { get; set; }

        public DateTime? date { get; set; }
    }
}