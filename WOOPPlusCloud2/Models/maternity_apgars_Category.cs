using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class maternity_apgars_Category
    {
        [Key] public int maternity_apgars_Category_id { get; set; }

        [StringLength(250)] public string maternity_apgars_Category_desc { get; set; }
    }
}