using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class consultation_room
    {
        [Key] public Guid consultation_id { get; set; }

        [StringLength(250)] public string consultation_description { get; set; }

        public int? staff_assigned { get; set; }

        public DateTime? created_date { get; set; }

        public DateTime? updated_date { get; set; }

        public int? created_by { get; set; }

        public int? edited_by { get; set; }

        public int? center_id { get; set; }

        public Guid? case_type_id { get; set; }
    }
}