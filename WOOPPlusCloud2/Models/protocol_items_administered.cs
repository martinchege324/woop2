using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class protocol_items_administered
    {
        [Key] public Guid protocol_item_administered_id { get; set; }

        public Guid protocol_trans_id { get; set; }

        public Guid? protocol_item_id { get; set; }

        public Guid? cycle_id { get; set; }

        public decimal quantity { get; set; }

        public int? administered_by { get; set; }

        public DateTime administerd_on { get; set; }
    }
}