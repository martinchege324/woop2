using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class scope_config
    {
        [Key] public Guid config_id { get; set; }

        [Column(TypeName = "xml")] [Required] public string config_data { get; set; }

        [StringLength(1)] public string scope_status { get; set; }
    }
}