using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class surgical_team
    {
        [Key] public Guid surgical_team_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        public int? staff_no { get; set; }

        public Guid? cycle_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(100)] public string staff_role { get; set; }

        public virtual staff staff { get; set; }

        public virtual staff staff1 { get; set; }

        public virtual visit_cycle visit_cycle { get; set; }
    }
}