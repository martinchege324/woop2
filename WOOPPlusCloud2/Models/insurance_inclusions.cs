using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class insurance_inclusions
    {
        [Key] public Guid insurance_inclusion_id { get; set; }

        public string insurance_inclusion_item_desc { get; set; }

        public string insurance_inclusion_item_id { get; set; }

        [StringLength(50)] public string insurance_inclusion_item_type { get; set; }

        public Guid? patient_payment_account_type_id { get; set; }

        public int? active { get; set; }

        public int? center_id { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? isinclusion { get; set; }
    }
}