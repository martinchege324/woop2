//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WOOPPlusCloud2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class benefits_package
    {
        [Key]
        public System.Guid benefit_package_id { get; set; }
        public string benefit_package_desc { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<int> edited_by { get; set; }
        public Nullable<System.DateTime> edited_date { get; set; }
        public Nullable<int> center_id { get; set; }
        public Nullable<decimal> benefit_package_charge_amount { get; set; }
        public Nullable<int> active { get; set; }
        public Nullable<int> facility_id { get; set; }
    }
}
