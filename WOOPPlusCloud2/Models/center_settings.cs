using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_settings
    {
        [Key] public Guid center_setting_id { get; set; }

        public Guid? setting_id { get; set; }

        public int? active { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }
    }
}