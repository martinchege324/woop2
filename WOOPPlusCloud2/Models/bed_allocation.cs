using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class bed_allocation
    {
        [Key] public Guid bed_allocation_id { get; set; }

        public Guid? ward_bed_id { get; set; }

        public Guid? cycle_id { get; set; }

        public Guid? patient_id { get; set; }

        public int? center_id { get; set; }

        public DateTime? date { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public int? active { get; set; }

        public int? allocatedby { get; set; }

        public int? deallocatedby { get; set; }

        public DateTime? deallocatedon { get; set; }
    }
}