using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class sub_preoperative
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sub_preoperative_id { get; set; }

        [Required] [StringLength(250)] public string sub_preoperative_description { get; set; }

        public int? preoperative_id { get; set; }
    }
}