using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class data_sync_logs
    {
        [Key] public Guid log_id { get; set; }

        public int? center_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        [StringLength(250)] public string action { get; set; }

        [StringLength(250)] public string method { get; set; }

        [StringLength(250)] public string code_file { get; set; }

        public int? line_number { get; set; }

        public bool? success { get; set; }

        [Column(TypeName = "text")] public string message { get; set; }
    }
}