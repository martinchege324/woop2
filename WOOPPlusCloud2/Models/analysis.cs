using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("analysis")]
    public class analysis
    {
        [Key] public Guid analysis_id { get; set; }

        public int? los_analysis_id { get; set; }

        public int? sub_los_analysis_id { get; set; }

        public Guid? cycle_id { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public DateTime? date { get; set; }

        [StringLength(250)] public string reason { get; set; }
    }
}