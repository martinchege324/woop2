using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class staff_loyalty_points
    {
        [Key] public Guid loyalty_point_id { get; set; }

        [StringLength(250)] public string loyalty_point_token { get; set; }

        public DateTime? create_date { get; set; }

        public int? created_by { get; set; }

        public int? center_id { get; set; }

        public int? loyalty_points { get; set; }

        public string tel_no { get; set; }
    }
}