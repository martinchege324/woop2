using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class signs_diagnosis
    {
        [Key] public int signs_diagnosis_id { get; set; }

        public int? sub_patient_sign_id { get; set; }

        public int? patient_sign_id { get; set; }

        public int? disease_id { get; set; }
    }
}