using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class center_location_stock
    {
        [Key] public Guid location_stock_id { get; set; }

        public Guid? center_stock_id { get; set; }

        public Guid? location_id { get; set; }

        [Column(TypeName = "numeric")] public decimal? quantity { get; set; }

        [StringLength(3)] public string uom_code { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? edited_by { get; set; }

        [Column(TypeName = "numeric")] public decimal? physical_count { get; set; }

        public int? physical_count_by { get; set; }

        public DateTime? physical_count_date { get; set; }

        public decimal? reorder_level { get; set; }

        public int? center_id { get; set; }
    }
}