using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class patient_tags
    {
        [Key] public Guid patient_tag_id { get; set; }

        public Guid? patient_id { get; set; }

        public Guid? tag_id { get; set; }

        public int? active { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? center_id { get; set; }

        public virtual patient_registration patient_registration { get; set; }

        public virtual tag tag { get; set; }
    }
}