using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class visit_service_register
    {
        [Key] public Guid visit_service_register_id { get; set; }

        public int? service_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        public int? sub_service_id { get; set; }

        public Guid? cycle_id { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public int? sub_service_item_id { get; set; }

        public DateTime? tb_ipt_startdate { get; set; }

        public DateTime? tb_ipt_enddate { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(1)] public string done { get; set; }

        [StringLength(250)] public string value { get; set; }

        public DateTime? requestdate { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public int? requested_by { get; set; }

        [StringLength(1)] public string paid { get; set; }

        public int? done_by { get; set; }

        public int? service_type_id { get; set; }

        [StringLength(250)] public string servicecomment { get; set; }

        public Guid? consultation_id { get; set; }

        [Column(TypeName = "image")] public byte[] image { get; set; }

        public int? quantity { get; set; }

        public decimal? total_charge { get; set; }

        public Guid? supplier_id { get; set; }

        [StringLength(250)] public string cancellation_comment { get; set; }

        public int? cancelled_by { get; set; }

        public DateTime? cancelled_date { get; set; }

        public int? cancelled { get; set; }

        public int? proforma { get; set; }

        public DateTime? item_created_date { get; set; }

        public int? item_created_by { get; set; }

        public decimal? unit_price { get; set; }

        public int? isinpatient { get; set; }

        public int? isinsurance { get; set; }

        public decimal? regular_retail_price { get; set; }

        public decimal? savings { get; set; }

        public decimal? vat_amount { get; set; }
    }
}