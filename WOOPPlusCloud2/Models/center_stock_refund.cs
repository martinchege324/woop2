using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_stock_refund
    {
        [Key] public Guid center_stock_refund_id { get; set; }

        public Guid? trans_id { get; set; }

        public Guid? center_product_id { get; set; }

        public double? quantity_requested { get; set; }

        public double? total_price_requested { get; set; }

        public double? quantity_refunded { get; set; }

        public double? total_price_refunded { get; set; }

        public int? center_id { get; set; }

        public int? requested_by { get; set; }

        public DateTime? requested_date { get; set; }

        public int? approved_by { get; set; }

        public int? active { get; set; }

        public DateTime? approved_date { get; set; }
    }
}