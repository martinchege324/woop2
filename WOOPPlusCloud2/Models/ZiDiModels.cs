using System.Data.Entity;

namespace WOOPPlusCloud2.Models
{
    public class ZiDiModels : DbContext
    {
        public ZiDiModels()
            : base("name=ZiDiModels")
        {
        }

        public virtual DbSet<accounting_trans_types> accounting_trans_types { get; set; }
        public virtual DbSet<administer_drug> administer_drug { get; set; }
        public virtual DbSet<allergy> allergies { get; set; }
        public virtual DbSet<analysis> analyses { get; set; }
        public virtual DbSet<aspnet_Applications> aspnet_Applications { get; set; }
        public virtual DbSet<aspnet_Membership> aspnet_Membership { get; set; }
        public virtual DbSet<aspnet_Paths> aspnet_Paths { get; set; }
        public virtual DbSet<aspnet_PersonalizationAllUsers> aspnet_PersonalizationAllUsers { get; set; }
        public virtual DbSet<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser { get; set; }
        public virtual DbSet<aspnet_Profile> aspnet_Profile { get; set; }
        public virtual DbSet<aspnet_Roles> aspnet_Roles { get; set; }
        public virtual DbSet<aspnet_SchemaVersions> aspnet_SchemaVersions { get; set; }
        public virtual DbSet<aspnet_Users> aspnet_Users { get; set; }
        public virtual DbSet<aspnet_WebEvent_Events> aspnet_WebEvent_Events { get; set; }
        public virtual DbSet<asset_type> asset_type { get; set; }
        public virtual DbSet<asset> assets { get; set; }
        public virtual DbSet<attendance_register> attendance_register { get; set; }
        public virtual DbSet<auto_billed_items> auto_billed_items { get; set; }
        public virtual DbSet<bank_transactions> bank_transactions { get; set; }
        public virtual DbSet<bank> banks { get; set; }
        public virtual DbSet<batch> batches { get; set; }
        public virtual DbSet<bed_allocation> bed_allocation { get; set; }
        public virtual DbSet<benefits_package> benefits_package { get; set; }
        public virtual DbSet<billing_trans> billing_trans { get; set; }
        public virtual DbSet<billing_type> billing_type { get; set; }
        public virtual DbSet<block_wards> block_wards { get; set; }
        public virtual DbSet<case_type_category> case_type_category { get; set; }
        public virtual DbSet<category> categories { get; set; }
        public virtual DbSet<center> centers { get; set; }
        public virtual DbSet<center_assets> center_assets { get; set; }
        public virtual DbSet<center_consultation_price> center_consultation_price { get; set; }
        public virtual DbSet<center_department_services> center_department_services { get; set; }
        public virtual DbSet<center_items_refund> center_items_refund { get; set; }
        public virtual DbSet<center_labtest_price> center_labtest_price { get; set; }
        public virtual DbSet<center_labtest_stocks> center_labtest_stocks { get; set; }
        public virtual DbSet<center_labtest_suppliers> center_labtest_suppliers { get; set; }
        public virtual DbSet<center_location_stock> center_location_stock { get; set; }
        public virtual DbSet<center_locations> center_locations { get; set; }
        public virtual DbSet<center_points> center_points { get; set; }
        public virtual DbSet<center_service_price> center_service_price { get; set; }
        public virtual DbSet<center_services_stocks> center_services_stocks { get; set; }
        public virtual DbSet<center_services_suppliers> center_services_suppliers { get; set; }
        public virtual DbSet<center_settings> center_settings { get; set; }
        public virtual DbSet<center_smart_parameters> center_smart_parameters { get; set; }
        public virtual DbSet<center_stock> center_stock { get; set; }
        public virtual DbSet<center_stock_margins> center_stock_margins { get; set; }
        public virtual DbSet<center_stock_price> center_stock_price { get; set; }
        public virtual DbSet<center_stock_price_change_log> center_stock_price_change_log { get; set; }
        public virtual DbSet<center_stock_refund> center_stock_refund { get; set; }
        public virtual DbSet<center_users> center_users { get; set; }
        public virtual DbSet<center_visit_types> center_visit_types { get; set; }
        public virtual DbSet<center_visitcharge_suppliers> center_visitcharge_suppliers { get; set; }
        public virtual DbSet<center_ward_services> center_ward_services { get; set; }
        public virtual DbSet<client_loggin> client_loggin { get; set; }
        public virtual DbSet<complication> complications { get; set; }
        public virtual DbSet<constituency> constituencies { get; set; }
        public virtual DbSet<consultation_room> consultation_room { get; set; }
        public virtual DbSet<counselling> counsellings { get; set; }
        public virtual DbSet<counselling_category> counselling_category { get; set; }
        public virtual DbSet<counselling_type> counselling_type { get; set; }
        public virtual DbSet<county> counties { get; set; }
        public virtual DbSet<data_sync_logs> data_sync_logs { get; set; }
        public virtual DbSet<dba_indexDefragExclusion> dba_indexDefragExclusion { get; set; }
        public virtual DbSet<dba_indexDefragLog> dba_indexDefragLog { get; set; }
        public virtual DbSet<dba_indexDefragStatus> dba_indexDefragStatus { get; set; }
        public virtual DbSet<decision_support> decision_support { get; set; }
        public virtual DbSet<decision_support_categ> decision_support_categ { get; set; }
        public virtual DbSet<decision_support_type> decision_support_type { get; set; }
        public virtual DbSet<department> departments { get; set; }
        public virtual DbSet<department_shift> department_shift { get; set; }
        public virtual DbSet<deposit> deposits { get; set; }
        public virtual DbSet<deposit_transaction> deposit_transaction { get; set; }
        public virtual DbSet<deposit_transaction_category> deposit_transaction_category { get; set; }
        public virtual DbSet<diagnosi> diagnosis { get; set; }
        public virtual DbSet<disease> diseases { get; set; }
        public virtual DbSet<district> districts { get; set; }
        public virtual DbSet<division> divisions { get; set; }
        public virtual DbSet<doctor_order> doctor_order { get; set; }
        public virtual DbSet<drug_admin> drug_admin { get; set; }
        public virtual DbSet<drug_dispatches> drug_dispatches { get; set; }
        public virtual DbSet<email_outbox> email_outbox { get; set; }
        public virtual DbSet<email_settings> email_settings { get; set; }
        public virtual DbSet<email_templates> email_templates { get; set; }
        public virtual DbSet<error_log> error_log { get; set; }
        public virtual DbSet<family> families { get; set; }
        public virtual DbSet<finance_categ> finance_categ { get; set; }
        public virtual DbSet<finance_type> finance_type { get; set; }
        public virtual DbSet<finance_type_items> finance_type_items { get; set; }
        public virtual DbSet<finance> finances { get; set; }
        public virtual DbSet<history> histories { get; set; }
        public virtual DbSet<history_category> history_category { get; set; }
        public virtual DbSet<history_type> history_type { get; set; }
        public virtual DbSet<hr_departments> hr_departments { get; set; }
        public virtual DbSet<incident> incidents { get; set; }
        public virtual DbSet<incidents_reviews> incidents_reviews { get; set; }
        public virtual DbSet<incision_position> incision_position { get; set; }
        public virtual DbSet<incision_site> incision_site { get; set; }
        public virtual DbSet<incision_type> incision_type { get; set; }
        public virtual DbSet<inpatient_feeding> inpatient_feeding { get; set; }
        public virtual DbSet<inpatient_queue> inpatient_queue { get; set; }
        public virtual DbSet<inpatient_queue_count> inpatient_queue_count { get; set; }
        public virtual DbSet<insurance_approved_items> insurance_approved_items { get; set; }
        public virtual DbSet<insurance_exclusions> insurance_exclusions { get; set; }
        public virtual DbSet<insurance_inclusions> insurance_inclusions { get; set; }
        public virtual DbSet<insurance_payments> insurance_payments { get; set; }
        public virtual DbSet<insurance_topups> insurance_topups { get; set; }
        public virtual DbSet<insurance_transaction> insurance_transaction { get; set; }
        public virtual DbSet<insurance_types> insurance_types { get; set; }
        public virtual DbSet<invoice> invoices { get; set; }
        public virtual DbSet<lab_test> lab_test { get; set; }
        public virtual DbSet<lab_test_category> lab_test_category { get; set; }
        public virtual DbSet<lab_test_specimens> lab_test_specimens { get; set; }
        public virtual DbSet<lab_test_type> lab_test_type { get; set; }
        public virtual DbSet<line_of_service> line_of_service { get; set; }
        public virtual DbSet<line_of_service_trans> line_of_service_trans { get; set; }
        public virtual DbSet<location_stock_department> location_stock_department { get; set; }
        public virtual DbSet<log> logs { get; set; }
        public virtual DbSet<los_analysis> los_analysis { get; set; }
        public virtual DbSet<los_analysis_type> los_analysis_type { get; set; }
        public virtual DbSet<los_complication> los_complication { get; set; }
        public virtual DbSet<los_complication_type> los_complication_type { get; set; }
        public virtual DbSet<los_sub_complications> los_sub_complications { get; set; }
        public virtual DbSet<losfollowup> losfollowups { get; set; }
        public virtual DbSet<losfollowup_category> losfollowup_category { get; set; }
        public virtual DbSet<losfollowup_trans> losfollowup_trans { get; set; }
        public virtual DbSet<losfollowup_type> losfollowup_type { get; set; }
        public virtual DbSet<maternity_apgars> maternity_apgars { get; set; }
        public virtual DbSet<maternity_apgars_Category> maternity_apgars_Category { get; set; }
        public virtual DbSet<maternity_apgars_trans> maternity_apgars_trans { get; set; }
        public virtual DbSet<maternity_labour> maternity_labour { get; set; }
        public virtual DbSet<maternity_labour_management> maternity_labour_management { get; set; }
        public virtual DbSet<maternity_labour_management_category> maternity_labour_management_category { get; set; }
        public virtual DbSet<maternityapgscore> maternityapgscores { get; set; }
        public virtual DbSet<medical_history> medical_history { get; set; }
        public virtual DbSet<MSpeer_conflictdetectionconfigrequest> MSpeer_conflictdetectionconfigrequest { get; set; }
        public virtual DbSet<MSpeer_lsns> MSpeer_lsns { get; set; }
        public virtual DbSet<nation> nations { get; set; }
        public virtual DbSet<notes_trans> notes_trans { get; set; }
        public virtual DbSet<nursing_care> nursing_care { get; set; }
        public virtual DbSet<operation_technique> operation_technique { get; set; }
        public virtual DbSet<package_exclusion> package_exclusion { get; set; }
        public virtual DbSet<package_items> package_items { get; set; }
        public virtual DbSet<partograph> partographs { get; set; }
        public virtual DbSet<patient_allergies> patient_allergies { get; set; }
        public virtual DbSet<patient_counter> patient_counter { get; set; }
        public virtual DbSet<patient_payment_account_types> patient_payment_account_types { get; set; }
        public virtual DbSet<patient_payment_accounts> patient_payment_accounts { get; set; }
        public virtual DbSet<patient_registration> patient_registration { get; set; }
        public virtual DbSet<patient_reminder_resolutions> patient_reminder_resolutions { get; set; }
        public virtual DbSet<patient_reminder_types> patient_reminder_types { get; set; }
        public virtual DbSet<patient_reminders> patient_reminders { get; set; }
        public virtual DbSet<patient_signs> patient_signs { get; set; }
        public virtual DbSet<patient_signs_category> patient_signs_category { get; set; }
        public virtual DbSet<patient_symptoms> patient_symptoms { get; set; }
        public virtual DbSet<patient_tags> patient_tags { get; set; }
        public virtual DbSet<patient_treatment> patient_treatment { get; set; }
        public virtual DbSet<patient_treatment_category> patient_treatment_category { get; set; }
        public virtual DbSet<payment> payments { get; set; }
        public virtual DbSet<payment_account_approved_items> payment_account_approved_items { get; set; }
        public virtual DbSet<payment_account_limit_trans> payment_account_limit_trans { get; set; }
        public virtual DbSet<payment_account_limit_types> payment_account_limit_types { get; set; }
        public virtual DbSet<payment_account_types> payment_account_types { get; set; }
        public virtual DbSet<payment_accounts> payment_accounts { get; set; }
        public virtual DbSet<payment_accounts_transaction> payment_accounts_transaction { get; set; }
        public virtual DbSet<payment_log> payment_log { get; set; }
        public virtual DbSet<payment_method> payment_method { get; set; }
        public virtual DbSet<paymentaccount> paymentaccounts { get; set; }
        public virtual DbSet<postoperative> postoperatives { get; set; }
        public virtual DbSet<postoperative_category> postoperative_category { get; set; }
        public virtual DbSet<postoperative_trans> postoperative_trans { get; set; }
        public virtual DbSet<preoperative> preoperatives { get; set; }
        public virtual DbSet<preoperative_category> preoperative_category { get; set; }
        public virtual DbSet<preoperative_nursing> preoperative_nursing { get; set; }
        public virtual DbSet<preoperative_nursing_category> preoperative_nursing_category { get; set; }
        public virtual DbSet<preoperative_nursing_trans> preoperative_nursing_trans { get; set; }
        public virtual DbSet<preoperative_trans> preoperative_trans { get; set; }
        public virtual DbSet<probable_diagnosis> probable_diagnosis { get; set; }
        public virtual DbSet<product_xref> product_xref { get; set; }
        public virtual DbSet<protocol_items> protocol_items { get; set; }
        public virtual DbSet<protocol_items_administered> protocol_items_administered { get; set; }
        public virtual DbSet<protocol_presciptions> protocol_presciptions { get; set; }
        public virtual DbSet<protocol_trans> protocol_trans { get; set; }
        public virtual DbSet<protocol> protocols { get; set; }
        public virtual DbSet<protocols_stock_mapping> protocols_stock_mapping { get; set; }
        public virtual DbSet<province> provinces { get; set; }
        public virtual DbSet<que_details> que_details { get; set; }
        public virtual DbSet<reciept_logs> reciept_logs { get; set; }
        public virtual DbSet<referral_categories> referral_categories { get; set; }
        public virtual DbSet<referral_reasons> referral_reasons { get; set; }
        public virtual DbSet<referral_trans> referral_trans { get; set; }
        public virtual DbSet<referral> referrals { get; set; }
        public virtual DbSet<refferal_data> refferal_data { get; set; }
        public virtual DbSet<registration_package> registration_package { get; set; }
        public virtual DbSet<sage_account_types> sage_account_types { get; set; }
        public virtual DbSet<sage_accounts> sage_accounts { get; set; }
        public virtual DbSet<sage_failed_deposits> sage_failed_deposits { get; set; }
        public virtual DbSet<sage_failed_transactions> sage_failed_transactions { get; set; }
        public virtual DbSet<sage_inventory_transactions> sage_inventory_transactions { get; set; }
        public virtual DbSet<sage_parameters> sage_parameters { get; set; }
        public virtual DbSet<sage_variables> sage_variables { get; set; }
        public virtual DbSet<scheduled_sms_settings> scheduled_sms_settings { get; set; }
        public virtual DbSet<schema_info> schema_info { get; set; }
        public virtual DbSet<scheme_insurance_types> scheme_insurance_types { get; set; }
        public virtual DbSet<scope_config> scope_config { get; set; }
        public virtual DbSet<scope_info> scope_info { get; set; }
        public virtual DbSet<sequence_tracking> sequence_tracking { get; set; }
        public virtual DbSet<service_type> service_type { get; set; }
        public virtual DbSet<service> services { get; set; }
        public virtual DbSet<services_center_stock> services_center_stock { get; set; }
        public virtual DbSet<setting> settings { get; set; }
        public virtual DbSet<sign> signs { get; set; }
        public virtual DbSet<signs_diagnosis> signs_diagnosis { get; set; }
        public virtual DbSet<sms_contact_groups> sms_contact_groups { get; set; }
        public virtual DbSet<sms_contacts> sms_contacts { get; set; }
        public virtual DbSet<sms_inbox> sms_inbox { get; set; }
        public virtual DbSet<sms_outbox> sms_outbox { get; set; }
        public virtual DbSet<sms_parameters> sms_parameters { get; set; }
        public virtual DbSet<sms_purchases> sms_purchases { get; set; }
        public virtual DbSet<sms_settings> sms_settings { get; set; }
        public virtual DbSet<sms_templates> sms_templates { get; set; }
        public virtual DbSet<staff> staffs { get; set; }
        public virtual DbSet<staff_category> staff_category { get; set; }
        public virtual DbSet<staff_department> staff_department { get; set; }
        public virtual DbSet<staff_incidents> staff_incidents { get; set; }
        public virtual DbSet<staff_incidents_category> staff_incidents_category { get; set; }
        public virtual DbSet<staff_leave> staff_leave { get; set; }
        public virtual DbSet<staff_loyalty_points> staff_loyalty_points { get; set; }
        public virtual DbSet<staff_shift> staff_shift { get; set; }
        public virtual DbSet<staff_user_rights> staff_user_rights { get; set; }
        public virtual DbSet<stock_f> stock_f { get; set; }
        public virtual DbSet<stock_order> stock_order { get; set; }
        public virtual DbSet<sub_case_type> sub_case_type { get; set; }
        public virtual DbSet<sub_diseases> sub_diseases { get; set; }
        public virtual DbSet<sub_history> sub_history { get; set; }
        public virtual DbSet<sub_lab_test> sub_lab_test { get; set; }
        public virtual DbSet<sub_los_analysis> sub_los_analysis { get; set; }
        public virtual DbSet<sub_maternity_apgars> sub_maternity_apgars { get; set; }
        public virtual DbSet<sub_maternity_labour_management> sub_maternity_labour_management { get; set; }
        public virtual DbSet<sub_patient_symptoms> sub_patient_symptoms { get; set; }
        public virtual DbSet<sub_patient_treatment> sub_patient_treatment { get; set; }
        public virtual DbSet<sub_postoperative> sub_postoperative { get; set; }
        public virtual DbSet<sub_preoperative> sub_preoperative { get; set; }
        public virtual DbSet<sub_preoperative_nursing> sub_preoperative_nursing { get; set; }
        public virtual DbSet<sub_que_details> sub_que_details { get; set; }
        public virtual DbSet<sub_service_items> sub_service_items { get; set; }
        public virtual DbSet<sub_services> sub_services { get; set; }
        public virtual DbSet<sub_staff_incidents> sub_staff_incidents { get; set; }
        public virtual DbSet<sub_surgery_details> sub_surgery_details { get; set; }
        public virtual DbSet<supplier_payment_logs> supplier_payment_logs { get; set; }
        public virtual DbSet<supplier_payments> supplier_payments { get; set; }
        public virtual DbSet<supplier_trans> supplier_trans { get; set; }
        public virtual DbSet<supplier_trans_type> supplier_trans_type { get; set; }
        public virtual DbSet<supplier> suppliers { get; set; }
        public virtual DbSet<suppliers_category> suppliers_category { get; set; }
        public virtual DbSet<surgery> surgeries { get; set; }
        public virtual DbSet<surgery_details> surgery_details { get; set; }
        public virtual DbSet<surgery_details_category> surgery_details_category { get; set; }
        public virtual DbSet<surgery_details_trans> surgery_details_trans { get; set; }
        public virtual DbSet<surgery_visit_type> surgery_visit_type { get; set; }
        public virtual DbSet<surgical_team> surgical_team { get; set; }
        public virtual DbSet<survey_answers> survey_answers { get; set; }
        public virtual DbSet<survey_questions> survey_questions { get; set; }
        public virtual DbSet<survey_trans> survey_trans { get; set; }
        public virtual DbSet<symptom> symptoms { get; set; }
        public virtual DbSet<symptoms_diagnosis> symptoms_diagnosis { get; set; }
        public virtual DbSet<sysreplserver> sysreplservers { get; set; }
        public virtual DbSet<tag> tags { get; set; }
        public virtual DbSet<test> tests { get; set; }
        public virtual DbSet<test_diagnosis> test_diagnosis { get; set; }
        public virtual DbSet<test_results> test_results { get; set; }
        public virtual DbSet<test_sample_collection> test_sample_collection { get; set; }
        public virtual DbSet<test_serial_numbers> test_serial_numbers { get; set; }
        public virtual DbSet<theatre> theatres { get; set; }
        public virtual DbSet<training> trainings { get; set; }
        public virtual DbSet<trans_file> trans_file { get; set; }
        public virtual DbSet<trans_type> trans_type { get; set; }
        public virtual DbSet<treatment> treatments { get; set; }
        public virtual DbSet<treatment_diagnosis> treatment_diagnosis { get; set; }
        public virtual DbSet<uom> uoms { get; set; }
        public virtual DbSet<uom_conversion> uom_conversion { get; set; }
        public virtual DbSet<user_rights> user_rights { get; set; }
        public virtual DbSet<vat> vats { get; set; }
        public virtual DbSet<visit_charge_category> visit_charge_category { get; set; }
        public virtual DbSet<visit_charge_items> visit_charge_items { get; set; }
        public virtual DbSet<visit_charge_price> visit_charge_price { get; set; }
        public virtual DbSet<visit_charges> visit_charges { get; set; }
        public virtual DbSet<visit_cycle> visit_cycle { get; set; }
        public virtual DbSet<visit_service_register> visit_service_register { get; set; }
        public virtual DbSet<vital> vitals { get; set; }
        public virtual DbSet<ward_bed_price> ward_bed_price { get; set; }
        public virtual DbSet<ward_beds> ward_beds { get; set; }

        public virtual DbSet<MSpeer_conflictdetectionconfigresponse> MSpeer_conflictdetectionconfigresponse
        {
            get;
            set;
        }

        public virtual DbSet<MSpeer_originatorid_history> MSpeer_originatorid_history { get; set; }
        public virtual DbSet<MSpeer_request> MSpeer_request { get; set; }
        public virtual DbSet<MSpeer_response> MSpeer_response { get; set; }
        public virtual DbSet<MSpeer_topologyrequest> MSpeer_topologyrequest { get; set; }
        public virtual DbSet<MSpeer_topologyresponse> MSpeer_topologyresponse { get; set; }
        public virtual DbSet<MSpub_identity_range> MSpub_identity_range { get; set; }
        public virtual DbSet<reciept_compare> reciept_compare { get; set; }
        public virtual DbSet<scheduled_sms_types> scheduled_sms_types { get; set; }
        public virtual DbSet<sysarticlecolumn> sysarticlecolumns { get; set; }
        public virtual DbSet<sysarticle> sysarticles { get; set; }
        public virtual DbSet<sysarticleupdate> sysarticleupdates { get; set; }
        public virtual DbSet<syspublication> syspublications { get; set; }
        public virtual DbSet<sysschemaarticle> sysschemaarticles { get; set; }
        public virtual DbSet<syssubscription> syssubscriptions { get; set; }
        public virtual DbSet<systranschema> systranschemas { get; set; }
        public virtual DbSet<user_rights_category> user_rights_category { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<accounting_trans_types>()
                .Property(e => e.accounting_trans_desc)
                .IsUnicode(false);

            modelBuilder.Entity<accounting_trans_types>()
                .Property(e => e.trans_type_sign)
                .IsFixedLength();

            modelBuilder.Entity<administer_drug>()
                .Property(e => e.prescribed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<administer_drug>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<allergy>()
                .Property(e => e.allergy_description)
                .IsUnicode(false);

            modelBuilder.Entity<analysis>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<analysis>()
                .Property(e => e.reason)
                .IsUnicode(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Membership)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Paths)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Roles)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Users)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Paths>()
                .HasOptional(e => e.aspnet_PersonalizationAllUsers)
                .WithRequired(e => e.aspnet_Paths);

            modelBuilder.Entity<aspnet_Roles>()
                .HasMany(e => e.aspnet_Users)
                .WithMany(e => e.aspnet_Roles)
                .Map(m => m.ToTable("aspnet_UsersInRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<aspnet_Users>()
                .HasOptional(e => e.aspnet_Membership)
                .WithRequired(e => e.aspnet_Users);

            modelBuilder.Entity<aspnet_Users>()
                .HasOptional(e => e.aspnet_Profile)
                .WithRequired(e => e.aspnet_Users);

            modelBuilder.Entity<aspnet_WebEvent_Events>()
                .Property(e => e.EventId)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<aspnet_WebEvent_Events>()
                .Property(e => e.EventSequence)
                .HasPrecision(19, 0);

            modelBuilder.Entity<aspnet_WebEvent_Events>()
                .Property(e => e.EventOccurrence)
                .HasPrecision(19, 0);

            modelBuilder.Entity<asset_type>()
                .Property(e => e.asset_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<asset>()
                .Property(e => e.asset_description)
                .IsUnicode(false);

            modelBuilder.Entity<attendance_register>()
                .Property(e => e.presentduration)
                .IsUnicode(false);

            modelBuilder.Entity<attendance_register>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<attendance_register>()
                .Property(e => e.staff_active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<attendance_register>()
                .Property(e => e.reason_for_excuse)
                .IsUnicode(false);

            modelBuilder.Entity<attendance_register>()
                .Property(e => e.attendance_status)
                .IsUnicode(false);

            modelBuilder.Entity<attendance_register>()
                .Property(e => e.staff_NO)
                .IsUnicode(false);

            modelBuilder.Entity<attendance_register>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<auto_billed_items>()
                .Property(e => e.auto_billed_item_desc)
                .IsUnicode(false);

            modelBuilder.Entity<auto_billed_items>()
                .Property(e => e.item_id)
                .IsUnicode(false);

            modelBuilder.Entity<auto_billed_items>()
                .Property(e => e.item_type)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.bank_name)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.bank_branch)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.bank_address)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.bank_tel)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.bank_code)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.acc_type)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.acc_number)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.contact_person)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.contact_person_tel)
                .IsUnicode(false);

            modelBuilder.Entity<bank>()
                .Property(e => e.acc_name)
                .IsUnicode(false);

            modelBuilder.Entity<batch>()
                .Property(e => e.batch_no)
                .IsUnicode(false);

            modelBuilder.Entity<batch>()
                .Property(e => e.batch_sequence)
                .IsUnicode(false);

            modelBuilder.Entity<batch>()
                .Property(e => e.cost_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<batch>()
                .Property(e => e.selling_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<batch>()
                .Property(e => e.initial_quantity)
                .HasPrecision(18, 0);

            modelBuilder.Entity<batch>()
                .Property(e => e.quantity)
                .HasPrecision(18, 0);

            modelBuilder.Entity<batch>()
                .Property(e => e.previous_batch_sequence)
                .IsUnicode(false);

            modelBuilder.Entity<batch>()
                .Property(e => e.current__batch_sequence)
                .IsFixedLength();

            modelBuilder.Entity<batch>()
                .Property(e => e.recieving_Uom_name)
                .IsUnicode(false);

            modelBuilder.Entity<batch>()
                .Property(e => e.manufacturer)
                .IsUnicode(false);

            modelBuilder.Entity<batch>()
                .Property(e => e.invoice_no)
                .IsUnicode(false);

            modelBuilder.Entity<bed_allocation>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<benefits_package>()
                .Property(e => e.benefit_package_desc)
                .IsUnicode(false);

            modelBuilder.Entity<billing_trans>()
                .Property(e => e.billing_trans_comment)
                .IsUnicode(false);

            modelBuilder.Entity<billing_trans>()
                .Property(e => e.client_notified)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<billing_trans>()
                .Property(e => e.center_updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<billing_trans>()
                .Property(e => e.trans_reference)
                .IsUnicode(false);

            modelBuilder.Entity<billing_type>()
                .Property(e => e.billing_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<billing_type>()
                .Property(e => e.billing_type_sign)
                .IsUnicode(false);

            modelBuilder.Entity<block_wards>()
                .Property(e => e.block_ward_desc)
                .IsUnicode(false);

            modelBuilder.Entity<case_type_category>()
                .Property(e => e.case_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<category>()
                .Property(e => e.category_desc)
                .IsUnicode(false);

            modelBuilder.Entity<category>()
                .Property(e => e.Workflow_group)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.center_desc)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.center_reg_no)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.center_address)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.center_tel)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.center_town)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.center_village)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.Service_Type)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center>()
                .Property(e => e.Health_Facility_Type)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.Status_Check)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.withdrawing_right)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center>()
                .Property(e => e.coordinates)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.center_county)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.facility_no_ref)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.PIN)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.center_currency)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.smart_group_practice_number)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.facility_insurance_code)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .Property(e => e.report_folder)
                .IsUnicode(false);

            modelBuilder.Entity<center>()
                .HasMany(e => e.surgery_details_trans)
                .WithOptional(e => e.center)
                .HasForeignKey(e => e.center_id);

            modelBuilder.Entity<center>()
                .HasMany(e => e.surgery_details_trans1)
                .WithOptional(e => e.center1)
                .HasForeignKey(e => e.center_id);

            modelBuilder.Entity<center>()
                .HasMany(e => e.trans_file)
                .WithRequired(e => e.center)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<center>()
                .HasMany(e => e.visit_cycle)
                .WithRequired(e => e.center)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<center_assets>()
                .Property(e => e.center_asset_sn)
                .IsUnicode(false);

            modelBuilder.Entity<center_assets>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<center_assets>()
                .Property(e => e.center_asset_make)
                .IsUnicode(false);

            modelBuilder.Entity<center_assets>()
                .Property(e => e.center_asset_type)
                .IsUnicode(false);

            modelBuilder.Entity<center_assets>()
                .Property(e => e.active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<center_department_services>()
                .Property(e => e.perfom_direct)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<center_department_services>()
                .Property(e => e.active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<center_items_refund>()
                .Property(e => e.item_description)
                .IsUnicode(false);

            modelBuilder.Entity<center_items_refund>()
                .Property(e => e.rejected_reason)
                .IsUnicode(false);

            modelBuilder.Entity<center_items_refund>()
                .Property(e => e.request_comment)
                .IsUnicode(false);

            modelBuilder.Entity<center_labtest_stocks>()
                .Property(e => e.quantity)
                .HasPrecision(18, 4);

            modelBuilder.Entity<center_location_stock>()
                .Property(e => e.quantity)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_location_stock>()
                .Property(e => e.uom_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<center_location_stock>()
                .Property(e => e.physical_count)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_locations>()
                .Property(e => e.location_description)
                .IsUnicode(false);

            modelBuilder.Entity<center_locations>()
                .Property(e => e.location_active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<center_points>()
                .Property(e => e.token_inputed)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.postman_token)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.token)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.smart_key)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.globalIdentifier1)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.globalIdentifier2)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.globalIdentifier3)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.initialize_url)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.member_details_url)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.member_benefits_url)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.userId)
                .IsUnicode(false);

            modelBuilder.Entity<center_smart_parameters>()
                .Property(e => e.smartdbconnection)
                .IsUnicode(false);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.centre_Uom_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.product_code)
                .IsUnicode(false);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.center_product_description)
                .IsUnicode(false);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.cost_price)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.margin)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.vatcode)
                .IsUnicode(false);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.physical_count)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.regular_retail_price)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.other_price)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.dose)
                .IsUnicode(false);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.margincorporate)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.marginhospital)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.marginretail)
                .HasPrecision(38, 2);

            modelBuilder.Entity<center_stock>()
                .Property(e => e.trade_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_margins>()
                .Property(e => e.margin)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_margins>()
                .Property(e => e.margincorporate)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_margins>()
                .Property(e => e.marginhospital)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_margins>()
                .Property(e => e.marginretail)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_price_change_log>()
                .Property(e => e.product_code)
                .IsUnicode(false);

            modelBuilder.Entity<center_stock_price_change_log>()
                .Property(e => e.center_product_description)
                .IsUnicode(false);

            modelBuilder.Entity<center_stock_price_change_log>()
                .Property(e => e.cost_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_price_change_log>()
                .Property(e => e.product_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_price_change_log>()
                .Property(e => e.corporate_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_price_change_log>()
                .Property(e => e.other_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_price_change_log>()
                .Property(e => e.regular_retail_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_stock_price_change_log>()
                .Property(e => e.trade_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<center_users>()
                .Property(e => e.allowed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<center_visit_types>()
                .Property(e => e.visit_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<complication>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<constituency>()
                .Property(e => e.constituency_name)
                .IsUnicode(false);

            modelBuilder.Entity<consultation_room>()
                .Property(e => e.consultation_description)
                .IsUnicode(false);

            modelBuilder.Entity<counselling>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<counselling_category>()
                .Property(e => e.counselling_category_description)
                .IsUnicode(false);

            modelBuilder.Entity<counselling_type>()
                .Property(e => e.counselling_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<county>()
                .Property(e => e.county_name)
                .IsUnicode(false);

            modelBuilder.Entity<data_sync_logs>()
                .Property(e => e.action)
                .IsUnicode(false);

            modelBuilder.Entity<data_sync_logs>()
                .Property(e => e.method)
                .IsUnicode(false);

            modelBuilder.Entity<data_sync_logs>()
                .Property(e => e.code_file)
                .IsUnicode(false);

            modelBuilder.Entity<data_sync_logs>()
                .Property(e => e.message)
                .IsUnicode(false);

            modelBuilder.Entity<dba_indexDefragLog>()
                .Property(e => e.sqlStatement)
                .IsUnicode(false);

            modelBuilder.Entity<dba_indexDefragLog>()
                .Property(e => e.errorMessage)
                .IsUnicode(false);

            modelBuilder.Entity<decision_support>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<decision_support_categ>()
                .Property(e => e.decision_support_categ_desc)
                .IsUnicode(false);

            modelBuilder.Entity<decision_support_type>()
                .Property(e => e.decision_support_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.department_desc)
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.department_code)
                .IsUnicode(false);

            modelBuilder.Entity<department_shift>()
                .Property(e => e.department_shift_desc)
                .IsUnicode(false);

            modelBuilder.Entity<department_shift>()
                .Property(e => e.active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<deposit>()
                .Property(e => e.approved)
                .IsUnicode(false);

            modelBuilder.Entity<deposit>()
                .Property(e => e.cancelled)
                .IsUnicode(false);

            modelBuilder.Entity<deposit>()
                .Property(e => e.generated_pin)
                .IsUnicode(false);

            modelBuilder.Entity<deposit>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<deposit>()
                .Property(e => e.total_charge)
                .HasPrecision(18, 0);

            modelBuilder.Entity<deposit>()
                .Property(e => e.amount_paid)
                .HasPrecision(18, 0);

            modelBuilder.Entity<deposit>()
                .Property(e => e.running_balance)
                .HasPrecision(18, 0);

            modelBuilder.Entity<deposit>()
                .Property(e => e.refund)
                .HasPrecision(18, 0);

            modelBuilder.Entity<deposit>()
                .Property(e => e.invoice_number)
                .IsUnicode(false);

            modelBuilder.Entity<deposit>()
                .Property(e => e.sage_order_no)
                .IsUnicode(false);

            modelBuilder.Entity<deposit>()
                .Property(e => e.invoice_rejection_reason)
                .IsUnicode(false);

            modelBuilder.Entity<deposit>()
                .Property(e => e.invoice_returned_reason)
                .IsUnicode(false);

            modelBuilder.Entity<deposit>()
                .Property(e => e.invoice_payment_reff)
                .IsUnicode(false);

            modelBuilder.Entity<deposit_transaction>()
                .Property(e => e.cancelled)
                .IsUnicode(false);

            modelBuilder.Entity<deposit_transaction>()
                .Property(e => e.approved)
                .IsUnicode(false);

            modelBuilder.Entity<deposit_transaction>()
                .Property(e => e.deposit_transaction_amount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<deposit_transaction>()
                .Property(e => e.payment_made_by_id)
                .IsUnicode(false);

            modelBuilder.Entity<deposit_transaction>()
                .Property(e => e.receipt_no)
                .IsUnicode(false);

            modelBuilder.Entity<deposit_transaction>()
                .Property(e => e.conversion_factor)
                .HasPrecision(18, 0);

            modelBuilder.Entity<deposit_transaction>()
                .Property(e => e.payment_amount2)
                .HasPrecision(18, 0);

            modelBuilder.Entity<deposit_transaction>()
                .Property(e => e.deposit_comment)
                .IsUnicode(false);

            modelBuilder.Entity<deposit_transaction_category>()
                .Property(e => e.deposit_transaction_category_description)
                .IsUnicode(false);

            modelBuilder.Entity<diagnosi>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<diagnosi>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<diagnosi>()
                .Property(e => e.disease_code)
                .IsUnicode(false);

            modelBuilder.Entity<diagnosi>()
                .Property(e => e.impression)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<disease>()
                .Property(e => e.disease_name)
                .IsUnicode(false);

            modelBuilder.Entity<district>()
                .Property(e => e.district_name)
                .IsUnicode(false);

            modelBuilder.Entity<division>()
                .Property(e => e.division_name)
                .IsUnicode(false);

            modelBuilder.Entity<doctor_order>()
                .Property(e => e.doctor_order_desc)
                .IsUnicode(false);

            modelBuilder.Entity<drug_admin>()
                .Property(e => e.quantity)
                .HasPrecision(18, 0);

            modelBuilder.Entity<drug_admin>()
                .Property(e => e.balance)
                .HasPrecision(18, 0);

            modelBuilder.Entity<drug_admin>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<drug_dispatches>()
                .Property(e => e.order_delivery_location)
                .IsUnicode(false);

            modelBuilder.Entity<drug_dispatches>()
                .Property(e => e.trans_type)
                .IsUnicode(false);

            modelBuilder.Entity<drug_dispatches>()
                .Property(e => e.total_charge)
                .HasPrecision(18, 0);

            modelBuilder.Entity<drug_dispatches>()
                .Property(e => e.drug_dispatch_comment)
                .IsUnicode(false);

            modelBuilder.Entity<drug_dispatches>()
                .Property(e => e.prescription_facility)
                .IsUnicode(false);

            modelBuilder.Entity<drug_dispatches>()
                .Property(e => e.after_disptach_comment)
                .IsUnicode(false);

            modelBuilder.Entity<drug_dispatches>()
                .Property(e => e.diagnosis)
                .IsUnicode(false);

            modelBuilder.Entity<email_outbox>()
                .Property(e => e.email_text_body)
                .IsUnicode(false);

            modelBuilder.Entity<email_outbox>()
                .Property(e => e.telephone)
                .IsUnicode(false);

            modelBuilder.Entity<email_outbox>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<email_outbox>()
                .Property(e => e.message)
                .IsUnicode(false);

            modelBuilder.Entity<email_settings>()
                .Property(e => e.deafult_email)
                .IsUnicode(false);

            modelBuilder.Entity<email_settings>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<email_settings>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<email_settings>()
                .Property(e => e.edited_by)
                .IsUnicode(false);

            modelBuilder.Entity<email_templates>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<email_templates>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<email_templates>()
                .Property(e => e.attachment_filename)
                .IsUnicode(false);

            modelBuilder.Entity<error_log>()
                .Property(e => e.error_desc)
                .IsUnicode(false);

            modelBuilder.Entity<error_log>()
                .Property(e => e.module)
                .IsUnicode(false);

            modelBuilder.Entity<family>()
                .Property(e => e.family_name)
                .IsUnicode(false);

            modelBuilder.Entity<family>()
                .Property(e => e.phone_no)
                .IsUnicode(false);

            modelBuilder.Entity<family>()
                .Property(e => e.family_facility_number)
                .IsUnicode(false);

            modelBuilder.Entity<finance_categ>()
                .Property(e => e.finance_Categ_desc)
                .IsUnicode(false);

            modelBuilder.Entity<finance_type>()
                .Property(e => e.finance_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<finance_type>()
                .Property(e => e.AIE_Code)
                .IsUnicode(false);

            modelBuilder.Entity<finance_type_items>()
                .Property(e => e.finance_item_desc)
                .IsUnicode(false);

            modelBuilder.Entity<finance_type_items>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<finance_type_items>()
                .Property(e => e.AIE_Level_Code)
                .IsUnicode(false);

            modelBuilder.Entity<finance_type_items>()
                .Property(e => e.income)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.finance_item_desc)
                .IsUnicode(false);

            modelBuilder.Entity<history>()
                .Property(e => e.history_desc)
                .IsUnicode(false);

            modelBuilder.Entity<history_category>()
                .Property(e => e.history_category_description)
                .IsUnicode(false);

            modelBuilder.Entity<history_category>()
                .Property(e => e.bgcolor)
                .IsUnicode(false);

            modelBuilder.Entity<history_type>()
                .Property(e => e.history_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<hr_departments>()
                .Property(e => e.hr_department_desc)
                .IsUnicode(false);

            modelBuilder.Entity<incident>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<incident>()
                .Property(e => e.admin_id)
                .IsUnicode(false);

            modelBuilder.Entity<incidents_reviews>()
                .Property(e => e.incident_ref)
                .IsUnicode(false);

            modelBuilder.Entity<incidents_reviews>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<incision_position>()
                .Property(e => e.incision_position_desc)
                .IsUnicode(false);

            modelBuilder.Entity<incision_site>()
                .Property(e => e.incision_site_desc)
                .IsUnicode(false);

            modelBuilder.Entity<incision_type>()
                .Property(e => e.incision_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_feeding>()
                .Property(e => e.feed_type)
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_feeding>()
                .Property(e => e.route)
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_feeding>()
                .Property(e => e.volume)
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_feeding>()
                .Property(e => e.feeding_content)
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_feeding>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_feeding>()
                .Property(e => e.order_type)
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_feeding>()
                .Property(e => e.prescribed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_feeding>()
                .Property(e => e.administered)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_queue>()
                .Property(e => e.done)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<inpatient_queue>()
                .Property(e => e.discharge)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<insurance_exclusions>()
                .Property(e => e.insurance_exclusion_desc)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_inclusions>()
                .Property(e => e.insurance_inclusion_item_desc)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_inclusions>()
                .Property(e => e.insurance_inclusion_item_id)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_inclusions>()
                .Property(e => e.insurance_inclusion_item_type)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_payments>()
                .Property(e => e.payment_reff)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_payments>()
                .Property(e => e.payment_comment)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_topups>()
                .Property(e => e.insurance_topup_amount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<insurance_topups>()
                .Property(e => e.cancelled)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_topups>()
                .Property(e => e.receipt_no)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_transaction>()
                .Property(e => e.reviewed_by)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_transaction>()
                .Property(e => e.review_comment)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_transaction>()
                .Property(e => e.approved_by)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_transaction>()
                .Property(e => e.approval_comment)
                .IsUnicode(false);

            modelBuilder.Entity<insurance_types>()
                .Property(e => e.insurance_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<invoice>()
                .Property(e => e.invoice_number)
                .IsUnicode(false);

            modelBuilder.Entity<invoice>()
                .Property(e => e.reffno)
                .IsUnicode(false);

            modelBuilder.Entity<invoice>()
                .Property(e => e.preauth)
                .IsUnicode(false);

            modelBuilder.Entity<lab_test>()
                .Property(e => e.lab_test_desc)
                .IsUnicode(false);

            modelBuilder.Entity<lab_test>()
                .Property(e => e.lab_test_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<lab_test>()
                .Property(e => e.referred)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<lab_test>()
                .Property(e => e.lab_test_code)
                .IsUnicode(false);

            modelBuilder.Entity<lab_test>()
                .Property(e => e.special)
                .IsUnicode(false);

            modelBuilder.Entity<lab_test_category>()
                .Property(e => e.lab_test_categ_desc)
                .IsUnicode(false);

            modelBuilder.Entity<lab_test_specimens>()
                .Property(e => e.lab_test_specimen_desc)
                .IsUnicode(false);

            modelBuilder.Entity<lab_test_type>()
                .Property(e => e.lab_test_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<lab_test_type>()
                .Property(e => e.sage_cost_account)
                .IsUnicode(false);

            modelBuilder.Entity<line_of_service>()
                .Property(e => e.line_of_service_description)
                .IsUnicode(false);

            modelBuilder.Entity<line_of_service>()
                .Property(e => e.licensed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<line_of_service_trans>()
                .Property(e => e.completed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<location_stock_department>()
                .Property(e => e.location_stock_department_description)
                .IsUnicode(false);

            modelBuilder.Entity<location_stock_department>()
                .Property(e => e.active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<log>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<log>()
                .Property(e => e.userpassword)
                .IsUnicode(false);

            modelBuilder.Entity<log>()
                .Property(e => e.successful)
                .IsUnicode(false);

            modelBuilder.Entity<log>()
                .Property(e => e.logtype)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<log>()
                .Property(e => e.machinename)
                .IsUnicode(false);

            modelBuilder.Entity<los_analysis>()
                .Property(e => e.los_analysis_desc)
                .IsUnicode(false);

            modelBuilder.Entity<los_analysis_type>()
                .Property(e => e.los_analysis_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<los_complication>()
                .Property(e => e.los_complication_desc)
                .IsUnicode(false);

            modelBuilder.Entity<los_complication_type>()
                .Property(e => e.los_complication_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<los_sub_complications>()
                .Property(e => e.los_sub_complication_desc)
                .IsUnicode(false);

            modelBuilder.Entity<losfollowup>()
                .Property(e => e.losfollowup_desc)
                .IsUnicode(false);

            modelBuilder.Entity<losfollowup_category>()
                .Property(e => e.losfollowup_category_desc)
                .IsUnicode(false);

            modelBuilder.Entity<losfollowup_trans>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<losfollowup_trans>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<losfollowup_trans>()
                .Property(e => e.visit)
                .IsUnicode(false);

            modelBuilder.Entity<losfollowup_type>()
                .Property(e => e.losfollowup_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<losfollowup_type>()
                .Property(e => e.bgcolor)
                .IsUnicode(false);

            modelBuilder.Entity<maternity_apgars>()
                .Property(e => e.maternity_apgars_desc)
                .IsUnicode(false);

            modelBuilder.Entity<maternity_apgars_Category>()
                .Property(e => e.maternity_apgars_Category_desc)
                .IsUnicode(false);

            modelBuilder.Entity<maternity_apgars_trans>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<maternity_labour>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<maternity_labour>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<maternity_labour>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<maternity_labour_management>()
                .Property(e => e.maternity_labour_management_desc)
                .IsUnicode(false);

            modelBuilder.Entity<maternity_labour_management_category>()
                .Property(e => e.maternity_labour_management_category_desc)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.pulse)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.grimace)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.appearance)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.respiration)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.status_of_baby)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.gender_of_baby)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.resuscitation)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.mode_of_delivery)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.doctor)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.midwife)
                .IsUnicode(false);

            modelBuilder.Entity<maternityapgscore>()
                .Property(e => e.disability)
                .IsUnicode(false);

            modelBuilder.Entity<medical_history>()
                .Property(e => e.value)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<medical_history>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<medical_history>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<nation>()
                .Property(e => e.nation_name)
                .IsUnicode(false);

            modelBuilder.Entity<notes_trans>()
                .Property(e => e.notes_desc)
                .IsUnicode(false);

            modelBuilder.Entity<notes_trans>()
                .Property(e => e.kardex_notes)
                .IsUnicode(false);

            modelBuilder.Entity<notes_trans>()
                .Property(e => e.pnote)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.walked)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.cleaned)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.changed_beddings)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.bowel_movement)
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.output_type)
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.output_amount)
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.output_color)
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.output)
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.intake_type)
                .IsUnicode(false);

            modelBuilder.Entity<nursing_care>()
                .Property(e => e.intake_amount)
                .IsUnicode(false);

            modelBuilder.Entity<operation_technique>()
                .Property(e => e.incision_type_other)
                .IsUnicode(false);

            modelBuilder.Entity<operation_technique>()
                .Property(e => e.incision_site_other)
                .IsUnicode(false);

            modelBuilder.Entity<operation_technique>()
                .Property(e => e.incision_position_other)
                .IsUnicode(false);

            modelBuilder.Entity<operation_technique>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<package_exclusion>()
                .Property(e => e.package_exclusion_desc)
                .IsUnicode(false);

            modelBuilder.Entity<package_exclusion>()
                .Property(e => e.package_exclusion_item_id)
                .IsUnicode(false);

            modelBuilder.Entity<package_items>()
                .Property(e => e.package_item_category)
                .IsUnicode(false);

            modelBuilder.Entity<partograph>()
                .Property(e => e.cycle_id)
                .IsUnicode(false);

            modelBuilder.Entity<patient_allergies>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<patient_allergies>()
                .HasOptional(e => e.patient_allergies1)
                .WithRequired(e => e.patient_allergies2);

            modelBuilder.Entity<patient_counter>()
                .Property(e => e.counter)
                .IsUnicode(false);

            modelBuilder.Entity<patient_counter>()
                .Property(e => e.bill_check)
                .IsUnicode(false);

            modelBuilder.Entity<patient_payment_account_types>()
                .Property(e => e.patient_payment_account_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<patient_payment_account_types>()
                .Property(e => e.scheme_code)
                .IsUnicode(false);

            modelBuilder.Entity<patient_payment_account_types>()
                .Property(e => e.account_no)
                .IsUnicode(false);

            modelBuilder.Entity<patient_payment_accounts>()
                .Property(e => e.patient_payment_account_id)
                .IsUnicode(false);

            modelBuilder.Entity<patient_payment_accounts>()
                .Property(e => e.patient_id)
                .IsUnicode(false);

            modelBuilder.Entity<patient_payment_accounts>()
                .Property(e => e.account_no)
                .IsUnicode(false);

            modelBuilder.Entity<patient_payment_accounts>()
                .Property(e => e.account_active)
                .IsUnicode(false);

            modelBuilder.Entity<patient_payment_accounts>()
                .Property(e => e.dependantrelationship)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_first_name)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_middle_name)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_last_name)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.gender)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_tel)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_address)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_postal_code)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_number)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_fathers_name)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_mothers_name)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.id_no)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.nhif_no)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_guardians_name)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_next_of_kin)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.next_of_kin_relationship)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.religion)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.nationality)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.kin_telephone)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.kin_residence)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.birth_cert)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.other_id)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.patient_facility_number)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.alt_kin_telephone)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.emg_contact_name)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.emg_contact_telephone)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.emg_contact_alt_telephone)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.emg_contact_residence)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.emg_contact_relationship)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.unique_gov_no)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.mothers_patient_facility_id)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.alt_telephone)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.kin_id_no)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.emg_contact_id_no)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.citytown)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.occupation)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.dependant_relationship)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.inpatient_number)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.outreach_location)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.delivery_location)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.distance_travelled)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .Property(e => e.has_smartphone)
                .IsUnicode(false);

            modelBuilder.Entity<patient_registration>()
                .HasMany(e => e.visit_cycle)
                .WithRequired(e => e.patient_registration)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<patient_reminder_resolutions>()
                .Property(e => e.patient_reminder_resolution_desc)
                .IsUnicode(false);

            modelBuilder.Entity<patient_reminder_types>()
                .Property(e => e.reminder_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<patient_reminders>()
                .Property(e => e.patient_reminder_comment)
                .IsUnicode(false);

            modelBuilder.Entity<patient_reminders>()
                .Property(e => e.reminder_resolution_comment)
                .IsUnicode(false);

            modelBuilder.Entity<patient_signs>()
                .Property(e => e.patient_sign_description)
                .IsUnicode(false);

            modelBuilder.Entity<patient_signs_category>()
                .Property(e => e.patient_sign_category_description)
                .IsUnicode(false);

            modelBuilder.Entity<patient_symptoms>()
                .Property(e => e.patient_symptom_description)
                .IsUnicode(false);

            modelBuilder.Entity<patient_treatment>()
                .Property(e => e.patient_treatment_desc)
                .IsUnicode(false);

            modelBuilder.Entity<patient_treatment>()
                .HasMany(e => e.treatments)
                .WithRequired(e => e.patient_treatment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<patient_treatment_category>()
                .Property(e => e.patient_treatment_category_desc)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.patient_able_to_pay)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.commment)
                .IsUnicode(false);

            modelBuilder.Entity<payment_account_approved_items>()
                .Property(e => e.payement_account_approved_item_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_account_approved_items>()
                .Property(e => e.patient_payment_account_transaction_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_account_approved_items>()
                .Property(e => e.charge_item_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_account_limit_trans>()
                .Property(e => e.limit_amount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<payment_account_limit_types>()
                .Property(e => e.payment_account_limit_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<payment_account_types>()
                .Property(e => e.payment_account_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts>()
                .Property(e => e.payment_account_desc)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts>()
                .Property(e => e.payment_account_ref)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts>()
                .Property(e => e.contact_person)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts_transaction>()
                .Property(e => e.payment_account_trans_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts_transaction>()
                .Property(e => e.payment_account_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts_transaction>()
                .Property(e => e.payment_ref_no)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts_transaction>()
                .Property(e => e.cycle_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts_transaction>()
                .Property(e => e.reviewed_by)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts_transaction>()
                .Property(e => e.review_comment)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts_transaction>()
                .Property(e => e.approved_by)
                .IsUnicode(false);

            modelBuilder.Entity<payment_accounts_transaction>()
                .Property(e => e.approval_comment)
                .IsUnicode(false);

            modelBuilder.Entity<payment_method>()
                .Property(e => e.payment_method_description)
                .IsUnicode(false);

            modelBuilder.Entity<payment_method>()
                .Property(e => e.active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<payment_method>()
                .Property(e => e.account_no)
                .IsUnicode(false);

            modelBuilder.Entity<paymentaccount>()
                .Property(e => e.payment_account_desc)
                .IsUnicode(false);

            modelBuilder.Entity<paymentaccount>()
                .Property(e => e.payment_account_ref)
                .IsUnicode(false);

            modelBuilder.Entity<postoperative>()
                .Property(e => e.postoperative_name)
                .IsUnicode(false);

            modelBuilder.Entity<postoperative_category>()
                .Property(e => e.postoperative_category_description)
                .IsUnicode(false);

            modelBuilder.Entity<postoperative_trans>()
                .Property(e => e.updated)
                .IsUnicode(false);

            modelBuilder.Entity<postoperative_trans>()
                .Property(e => e.cycle_id)
                .IsUnicode(false);

            modelBuilder.Entity<postoperative_trans>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<preoperative>()
                .Property(e => e.preoperative_name)
                .IsUnicode(false);

            modelBuilder.Entity<preoperative_category>()
                .Property(e => e.preoperative_category_description)
                .IsUnicode(false);

            modelBuilder.Entity<preoperative_nursing>()
                .Property(e => e.preoperative_nursing_name)
                .IsUnicode(false);

            modelBuilder.Entity<preoperative_nursing_category>()
                .Property(e => e.preoperative_nursing_categ_desc)
                .IsUnicode(false);

            modelBuilder.Entity<preoperative_nursing_trans>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<preoperative_trans>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<preoperative_trans>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<probable_diagnosis>()
                .Property(e => e.disease_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_xref>()
                .Property(e => e.xref_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_xref>()
                .Property(e => e.xref_description)
                .IsUnicode(false);

            modelBuilder.Entity<protocol_items>()
                .Property(e => e.prescription)
                .IsUnicode(false);

            modelBuilder.Entity<protocol_items>()
                .Property(e => e.route)
                .IsUnicode(false);

            modelBuilder.Entity<protocol_items>()
                .Property(e => e.comments)
                .IsUnicode(false);

            modelBuilder.Entity<protocol_items>()
                .Property(e => e.unit_price)
                .HasPrecision(38, 2);

            modelBuilder.Entity<protocol_items>()
                .Property(e => e.total_price)
                .HasPrecision(38, 2);

            modelBuilder.Entity<protocol_items>()
                .Property(e => e.internal_desc)
                .IsUnicode(false);

            modelBuilder.Entity<protocol_items_administered>()
                .Property(e => e.quantity)
                .HasPrecision(18, 0);

            modelBuilder.Entity<protocol_presciptions>()
                .Property(e => e.interval_desc)
                .IsUnicode(false);

            modelBuilder.Entity<protocol_presciptions>()
                .Property(e => e.comments)
                .IsUnicode(false);

            modelBuilder.Entity<protocol_presciptions>()
                .Property(e => e.chemo_line)
                .IsUnicode(false);

            modelBuilder.Entity<protocol_trans>()
                .Property(e => e.comments)
                .IsUnicode(false);

            modelBuilder.Entity<protocol>()
                .Property(e => e.protocol_name)
                .IsUnicode(false);

            modelBuilder.Entity<protocols_stock_mapping>()
                .Property(e => e.comments)
                .IsUnicode(false);

            modelBuilder.Entity<province>()
                .Property(e => e.province_name)
                .IsUnicode(false);

            modelBuilder.Entity<que_details>()
                .Property(e => e.que_details_description)
                .IsUnicode(false);

            modelBuilder.Entity<reciept_logs>()
                .Property(e => e.reciept_amount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<reciept_logs>()
                .Property(e => e.reciept_no)
                .IsUnicode(false);

            modelBuilder.Entity<referral_categories>()
                .Property(e => e.referral_category_desc)
                .IsUnicode(false);

            modelBuilder.Entity<referral_reasons>()
                .Property(e => e.reason_description)
                .IsUnicode(false);

            modelBuilder.Entity<referral_trans>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<referral>()
                .Property(e => e.referred)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<referral>()
                .Property(e => e.ambulance_comment)
                .IsUnicode(false);

            modelBuilder.Entity<referral>()
                .Property(e => e.ambulance_no)
                .IsUnicode(false);

            modelBuilder.Entity<referral>()
                .Property(e => e.referral_reason)
                .IsUnicode(false);

            modelBuilder.Entity<referral>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<referral>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<referral>()
                .Property(e => e.center_transferred_to_desc)
                .IsUnicode(false);

            modelBuilder.Entity<refferal_data>()
                .Property(e => e.referral_data_desc)
                .IsUnicode(false);

            modelBuilder.Entity<registration_package>()
                .Property(e => e.package_description)
                .IsUnicode(false);

            modelBuilder.Entity<registration_package>()
                .Property(e => e.package_code)
                .IsUnicode(false);

            modelBuilder.Entity<sage_account_types>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<sage_accounts>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<sage_accounts>()
                .Property(e => e.account_no)
                .IsUnicode(false);

            modelBuilder.Entity<sage_accounts>()
                .Property(e => e.account)
                .IsUnicode(false);

            modelBuilder.Entity<sage_failed_deposits>()
                .Property(e => e.reason_not_posted)
                .IsUnicode(false);

            modelBuilder.Entity<sage_failed_transactions>()
                .Property(e => e.reason_not_posted)
                .IsUnicode(false);

            modelBuilder.Entity<sage_failed_transactions>()
                .Property(e => e.sage_invoice_no)
                .IsUnicode(false);

            modelBuilder.Entity<sage_inventory_transactions>()
                .Property(e => e.invoice_no)
                .IsUnicode(false);

            modelBuilder.Entity<sage_inventory_transactions>()
                .Property(e => e.total)
                .HasPrecision(18, 0);

            modelBuilder.Entity<sage_inventory_transactions>()
                .Property(e => e.sage_sale_order)
                .IsUnicode(false);

            modelBuilder.Entity<sage_parameters>()
                .Property(e => e.serial_number)
                .IsUnicode(false);

            modelBuilder.Entity<sage_parameters>()
                .Property(e => e.auth_key)
                .IsUnicode(false);

            modelBuilder.Entity<sage_parameters>()
                .Property(e => e.sql_server_name)
                .IsUnicode(false);

            modelBuilder.Entity<sage_parameters>()
                .Property(e => e.db_name)
                .IsUnicode(false);

            modelBuilder.Entity<sage_parameters>()
                .Property(e => e.user_name)
                .IsUnicode(false);

            modelBuilder.Entity<sage_parameters>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<sage_parameters>()
                .Property(e => e.evolution_common_db_name)
                .IsUnicode(false);

            modelBuilder.Entity<sage_variables>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<sage_variables>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<scheme_insurance_types>()
                .Property(e => e.scheme_insurance_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<scheme_insurance_types>()
                .Property(e => e.consultation_copay)
                .HasPrecision(18, 0);

            modelBuilder.Entity<scheme_insurance_types>()
                .Property(e => e.totalbill_copay)
                .HasPrecision(18, 0);

            modelBuilder.Entity<scope_config>()
                .Property(e => e.scope_status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<scope_info>()
                .Property(e => e.scope_timestamp)
                .IsFixedLength();

            modelBuilder.Entity<sequence_tracking>()
                .Property(e => e.initial_quantity)
                .HasPrecision(18, 0);

            modelBuilder.Entity<sequence_tracking>()
                .Property(e => e.quantity)
                .HasPrecision(18, 0);

            modelBuilder.Entity<service_type>()
                .Property(e => e.service_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<service_type>()
                .Property(e => e.sage_cost_account)
                .IsUnicode(false);

            modelBuilder.Entity<service>()
                .Property(e => e.service_desc)
                .IsUnicode(false);

            modelBuilder.Entity<service>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<service>()
                .Property(e => e.checklist_item)
                .IsUnicode(false);

            modelBuilder.Entity<service>()
                .Property(e => e.services_code)
                .IsUnicode(false);

            modelBuilder.Entity<setting>()
                .Property(e => e.setting_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sign>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<sms_contact_groups>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<sms_contact_groups>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<sms_contact_groups>()
                .HasMany(e => e.sms_contacts)
                .WithRequired(e => e.sms_contact_groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sms_contacts>()
                .Property(e => e.surname)
                .IsUnicode(false);

            modelBuilder.Entity<sms_contacts>()
                .Property(e => e.other_names)
                .IsUnicode(false);

            modelBuilder.Entity<sms_contacts>()
                .Property(e => e.telephone)
                .IsUnicode(false);

            modelBuilder.Entity<sms_contacts>()
                .Property(e => e.email_address)
                .IsUnicode(false);

            modelBuilder.Entity<sms_inbox>()
                .Property(e => e.text_body)
                .IsUnicode(false);

            modelBuilder.Entity<sms_inbox>()
                .Property(e => e.telephone)
                .IsFixedLength();

            modelBuilder.Entity<sms_outbox>()
                .Property(e => e.text_body)
                .IsUnicode(false);

            modelBuilder.Entity<sms_outbox>()
                .Property(e => e.telephone)
                .IsUnicode(false);

            modelBuilder.Entity<sms_outbox>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<sms_outbox>()
                .Property(e => e.message)
                .IsUnicode(false);

            modelBuilder.Entity<sms_parameters>()
                .Property(e => e.com_port)
                .IsUnicode(false);

            modelBuilder.Entity<sms_purchases>()
                .Property(e => e.rate)
                .HasPrecision(18, 0);

            modelBuilder.Entity<sms_purchases>()
                .Property(e => e.token_inputed)
                .IsUnicode(false);

            modelBuilder.Entity<sms_settings>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<sms_settings>()
                .Property(e => e.apikey)
                .IsUnicode(false);

            modelBuilder.Entity<sms_settings>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<sms_settings>()
                .Property(e => e.edited_by)
                .IsUnicode(false);

            modelBuilder.Entity<sms_templates>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<sms_templates>()
                .Property(e => e.name)
                .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_NO)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_surname)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_other_names)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.National_id_NO)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_email)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_telephone)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_post_address)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_post_address_code)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_town_city)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.updated)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_password)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.travel_distance_from_home)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.mode_of_transport)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.transit_time_to_center)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_title)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.staff_role)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.marital_status)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.emergency_contact_NO)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.intern)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.active)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.locked)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.punchin_shift)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.security_answer)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.volume_counter)
            //    .HasPrecision(18, 0);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.intensity_tracker)
            //    .HasPrecision(18, 0);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.value_tracker)
            //    .HasPrecision(18, 0);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.emergency_contact_relationship)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.PIN)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.NHIF)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .Property(e => e.NSSF)
            //    .IsUnicode(false);

            //modelBuilder.Entity<staff>()
            //    .HasMany(e => e.surgery_details_trans)
            //    .WithOptional(e => e.staff)
            //    .HasForeignKey(e => e.staff_id);

            //modelBuilder.Entity<staff>()
            //    .HasMany(e => e.surgery_details_trans1)
            //    .WithOptional(e => e.staff1)
            //    .HasForeignKey(e => e.staff_id);

            //modelBuilder.Entity<staff>()
            //    .HasMany(e => e.surgical_team)
            //    .WithOptional(e => e.staff)
            //    .HasForeignKey(e => e.staff_id);

            //modelBuilder.Entity<staff>()
            //    .HasMany(e => e.surgical_team1)
            //    .WithOptional(e => e.staff1)
            //    .HasForeignKey(e => e.staff_id);

            modelBuilder.Entity<staff_category>()
                .Property(e => e.staff_category_desc)
                .IsUnicode(false);

            modelBuilder.Entity<staff_department>()
                .Property(e => e.active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<staff_incidents>()
                .Property(e => e.staff_incident_desc)
                .IsUnicode(false);

            modelBuilder.Entity<staff_incidents_category>()
                .Property(e => e.staff_incident_category_desc)
                .IsUnicode(false);

            modelBuilder.Entity<staff_leave>()
                .Property(e => e.reason)
                .IsUnicode(false);

            modelBuilder.Entity<staff_leave>()
                .Property(e => e.staff_leave_type)
                .IsUnicode(false);

            modelBuilder.Entity<staff_leave>()
                .Property(e => e.authorised_by)
                .IsUnicode(false);

            modelBuilder.Entity<staff_leave>()
                .Property(e => e.cancelled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<staff_leave>()
                .Property(e => e.cancelled_by)
                .IsUnicode(false);

            modelBuilder.Entity<staff_loyalty_points>()
                .Property(e => e.loyalty_point_token)
                .IsUnicode(false);

            modelBuilder.Entity<staff_loyalty_points>()
                .Property(e => e.tel_no)
                .IsUnicode(false);

            modelBuilder.Entity<staff_shift>()
                .Property(e => e.shift_completed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<staff_shift>()
                .Property(e => e.prepared_by)
                .IsUnicode(false);

            modelBuilder.Entity<staff_shift>()
                .Property(e => e.shift_cancelled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<staff_user_rights>()
                .Property(e => e.allowed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<staff_user_rights>()
                .Property(e => e.licenced)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stock_f>()
                .Property(e => e.product_short_desc)
                .IsUnicode(false);

            modelBuilder.Entity<stock_f>()
                .Property(e => e.product_long_Desc)
                .IsUnicode(false);

            modelBuilder.Entity<stock_f>()
                .Property(e => e.Uom_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stock_f>()
                .Property(e => e.product_scancode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stock_f>()
                .Property(e => e.product_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stock_f>()
                .Property(e => e.unit_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<stock_f>()
                .Property(e => e.suom_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stock_f>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stock_f>()
                .HasMany(e => e.trans_file)
                .WithRequired(e => e.stock_f)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.os_balance)
                .HasPrecision(18, 0);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.total_receipts)
                .HasPrecision(18, 0);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.total_dispensed)
                .HasPrecision(18, 0);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.adjustments)
                .HasPrecision(18, 0);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.losses)
                .HasPrecision(18, 0);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.cs_balance)
                .HasPrecision(18, 0);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.order_quantity)
                .HasPrecision(18, 0);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.order_cost)
                .HasPrecision(18, 0);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.order_Ref)
                .IsUnicode(false);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.cancelled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.approved)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<stock_order>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<sub_case_type>()
                .Property(e => e.sub_case_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_diseases>()
                .Property(e => e.sub_disease_description)
                .IsUnicode(false);

            modelBuilder.Entity<sub_diseases>()
                .Property(e => e.disease_code)
                .IsUnicode(false);

            modelBuilder.Entity<sub_history>()
                .Property(e => e.sub_history_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_lab_test>()
                .Property(e => e.sub_lab_test_description)
                .IsUnicode(false);

            modelBuilder.Entity<sub_lab_test>()
                .Property(e => e.underfive)
                .IsUnicode(false);

            modelBuilder.Entity<sub_lab_test>()
                .Property(e => e.maleadult)
                .IsUnicode(false);

            modelBuilder.Entity<sub_lab_test>()
                .Property(e => e.femaleadult)
                .IsUnicode(false);

            modelBuilder.Entity<sub_lab_test>()
                .Property(e => e.unit)
                .IsUnicode(false);

            modelBuilder.Entity<sub_lab_test>()
                .Property(e => e.underfive_critical)
                .IsUnicode(false);

            modelBuilder.Entity<sub_lab_test>()
                .Property(e => e.maleadult_critical)
                .IsUnicode(false);

            modelBuilder.Entity<sub_lab_test>()
                .Property(e => e.femaleadult_critical)
                .IsUnicode(false);

            modelBuilder.Entity<sub_los_analysis>()
                .Property(e => e.sub_los_analysis_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_maternity_apgars>()
                .Property(e => e.sub_maternity_apgars_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_maternity_labour_management>()
                .Property(e => e.sub_maternity_labour_management_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_patient_symptoms>()
                .Property(e => e.sub_patient_symptom_description)
                .IsUnicode(false);

            modelBuilder.Entity<sub_patient_treatment>()
                .Property(e => e.sub_patient_treatment_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_postoperative>()
                .Property(e => e.sub_postoperative_description)
                .IsUnicode(false);

            modelBuilder.Entity<sub_preoperative>()
                .Property(e => e.sub_preoperative_description)
                .IsUnicode(false);

            modelBuilder.Entity<sub_preoperative_nursing>()
                .Property(e => e.sub_preoperative_nursing_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_que_details>()
                .Property(e => e.sub_que_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_que_details>()
                .Property(e => e.IfSpecial)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<sub_service_items>()
                .Property(e => e.sub_service_item_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_services>()
                .Property(e => e.sub_service_description)
                .IsUnicode(false);

            modelBuilder.Entity<sub_staff_incidents>()
                .Property(e => e.sub_staff_incident_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_surgery_details>()
                .Property(e => e.sub_surgery_details_desc)
                .IsUnicode(false);

            modelBuilder.Entity<sub_surgery_details>()
                .HasMany(e => e.surgery_details_trans)
                .WithOptional(e => e.sub_surgery_details)
                .HasForeignKey(e => e.sub_surgery_details_id);

            modelBuilder.Entity<sub_surgery_details>()
                .HasMany(e => e.surgery_details_trans1)
                .WithOptional(e => e.sub_surgery_details1)
                .HasForeignKey(e => e.sub_surgery_details_id);

            modelBuilder.Entity<supplier_payment_logs>()
                .Property(e => e.amount_paid)
                .HasPrecision(18, 0);

            modelBuilder.Entity<supplier_payments>()
                .Property(e => e.supplier_payment_amount)
                .HasPrecision(38, 2);

            modelBuilder.Entity<supplier_payments>()
                .Property(e => e.supplier_payment_comment)
                .IsUnicode(false);

            modelBuilder.Entity<supplier_payments>()
                .Property(e => e.supplier_payment_ref)
                .IsUnicode(false);

            modelBuilder.Entity<supplier_trans>()
                .Property(e => e.supplier_trans_amount)
                .HasPrecision(38, 2);

            modelBuilder.Entity<supplier_trans>()
                .Property(e => e.supplier_trans_comment)
                .IsUnicode(false);

            modelBuilder.Entity<supplier_trans>()
                .Property(e => e.supplier_trans_ref)
                .IsUnicode(false);

            modelBuilder.Entity<supplier_trans>()
                .Property(e => e.items_trans_id)
                .IsUnicode(false);

            modelBuilder.Entity<supplier_trans>()
                .Property(e => e.item_type)
                .IsUnicode(false);

            modelBuilder.Entity<supplier_trans>()
                .Property(e => e.full_item_description)
                .IsUnicode(false);

            modelBuilder.Entity<supplier_trans>()
                .Property(e => e.supplier_payment_ref)
                .IsUnicode(false);

            modelBuilder.Entity<supplier_trans_type>()
                .Property(e => e.supplier_trans_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<supplier_trans_type>()
                .Property(e => e.supplier_trans_type_sign)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<supplier_trans_type>()
                .HasMany(e => e.supplier_trans)
                .WithOptional(e => e.supplier_trans_type)
                .HasForeignKey(e => e.supplier_trans_type_id);

            modelBuilder.Entity<supplier_trans_type>()
                .HasMany(e => e.supplier_trans1)
                .WithOptional(e => e.supplier_trans_type1)
                .HasForeignKey(e => e.supplier_trans_type_id);

            modelBuilder.Entity<supplier_trans_type>()
                .HasMany(e => e.supplier_trans2)
                .WithOptional(e => e.supplier_trans_type2)
                .HasForeignKey(e => e.supplier_trans_type_id);

            modelBuilder.Entity<supplier>()
                .Property(e => e.supplier_name)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.supplier_address)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.supplier_contact)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.supplier_account)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.account_balance)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.total_invoice)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.total_payment)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.first_bank_name)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.first_acc_name)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.first_acc_no)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.second_bank_name)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.second_acc_name)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.second_acc_no)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.KRAPIN)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.id_number)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.supplier_contact_person)
                .IsUnicode(false);

            modelBuilder.Entity<suppliers_category>()
                .Property(e => e.supplier_category_desc)
                .IsUnicode(false);

            modelBuilder.Entity<surgery>()
                .Property(e => e.done)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<surgery>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<surgery>()
                .Property(e => e.pending)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<surgery>()
                .Property(e => e.patient_alive)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<surgery>()
                .Property(e => e.discharge_note)
                .IsUnicode(false);

            modelBuilder.Entity<surgery>()
                .Property(e => e.paid)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<surgery>()
                .Property(e => e.surgery_approved)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<surgery_details>()
                .Property(e => e.surgery_details_name)
                .IsUnicode(false);

            modelBuilder.Entity<surgery_details_category>()
                .Property(e => e.surgery_details_categ_desc)
                .IsUnicode(false);

            modelBuilder.Entity<surgery_details_trans>()
                .Property(e => e.updated)
                .IsUnicode(false);

            modelBuilder.Entity<surgery_details_trans>()
                .Property(e => e.remarks)
                .IsUnicode(false);

            modelBuilder.Entity<surgery_visit_type>()
                .Property(e => e.surgery_visit_type_description)
                .IsUnicode(false);

            modelBuilder.Entity<surgical_team>()
                .Property(e => e.staff_role)
                .IsUnicode(false);

            modelBuilder.Entity<survey_answers>()
                .Property(e => e.survey_answer_desc)
                .IsUnicode(false);

            modelBuilder.Entity<survey_questions>()
                .Property(e => e.survey_question_desc)
                .IsUnicode(false);

            modelBuilder.Entity<survey_trans>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<symptom>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<symptom>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<symptom>()
                .Property(e => e.duration)
                .IsUnicode(false);

            modelBuilder.Entity<symptom>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<symptom>()
                .Property(e => e.chief_complaint)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tag>()
                .Property(e => e.tag_description)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.test_internal)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.result_positive)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.done)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.sample)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.samplecondition)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.samplelocation)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.samplecomment)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.labcomment)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.sample_quantity)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.test_cancelled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.external_facility)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.fromexternal_facility)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.cancellation_comment)
                .IsUnicode(false);

            modelBuilder.Entity<test>()
                .Property(e => e.verified_comment)
                .IsUnicode(false);

            modelBuilder.Entity<test_results>()
                .Property(e => e.labcomment)
                .IsUnicode(false);

            modelBuilder.Entity<test_results>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<test_results>()
                .Property(e => e.done)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<test_results>()
                .Property(e => e.verified_comment)
                .IsUnicode(false);

            modelBuilder.Entity<test_results>()
                .Property(e => e.cancellation_comment)
                .IsUnicode(false);

            modelBuilder.Entity<test_results>()
                .Property(e => e.ref_from)
                .IsUnicode(false);

            modelBuilder.Entity<test_results>()
                .Property(e => e.ref_to)
                .IsUnicode(false);

            modelBuilder.Entity<test_results>()
                .Property(e => e.critical_from)
                .IsUnicode(false);

            modelBuilder.Entity<test_results>()
                .Property(e => e.critical_to)
                .IsUnicode(false);

            modelBuilder.Entity<test_sample_collection>()
                .Property(e => e.samplecondition)
                .IsUnicode(false);

            modelBuilder.Entity<test_sample_collection>()
                .Property(e => e.samplelocation)
                .IsUnicode(false);

            modelBuilder.Entity<test_sample_collection>()
                .Property(e => e.cancellation_comment)
                .IsUnicode(false);

            modelBuilder.Entity<test_sample_collection>()
                .Property(e => e.sample)
                .IsUnicode(false);

            modelBuilder.Entity<test_sample_collection>()
                .Property(e => e.sample_quantity)
                .IsUnicode(false);

            modelBuilder.Entity<test_sample_collection>()
                .Property(e => e.samplecomment)
                .IsUnicode(false);

            modelBuilder.Entity<test_sample_collection>()
                .Property(e => e.test_serial_no)
                .IsUnicode(false);

            modelBuilder.Entity<test_serial_numbers>()
                .Property(e => e.code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<theatre>()
                .Property(e => e.theatre_description)
                .IsUnicode(false);

            modelBuilder.Entity<training>()
                .Property(e => e.training_type)
                .IsUnicode(false);

            modelBuilder.Entity<training>()
                .Property(e => e.diagnostic_category)
                .IsUnicode(false);

            modelBuilder.Entity<training>()
                .Property(e => e.training_organiser)
                .IsUnicode(false);

            modelBuilder.Entity<training>()
                .Property(e => e.training_level)
                .IsUnicode(false);

            modelBuilder.Entity<training>()
                .Property(e => e.training_location)
                .IsUnicode(false);

            modelBuilder.Entity<training>()
                .Property(e => e.training_name)
                .IsUnicode(false);

            modelBuilder.Entity<training>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<training>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.trans_reference)
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.uom_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.batch_no)
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.trans_comment)
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.complete)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.prescription)
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.unit_price)
                .HasPrecision(10, 2);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.total)
                .HasPrecision(20, 2);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.cancelled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.approved)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.vatcode)
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.vatvalue)
                .HasPrecision(38, 2);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.batch_sequence)
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.invoice_no)
                .IsUnicode(false);

            modelBuilder.Entity<trans_file>()
                .Property(e => e.sage_sale_order)
                .IsUnicode(false);

            modelBuilder.Entity<trans_type>()
                .Property(e => e.trans_type_desc)
                .IsUnicode(false);

            modelBuilder.Entity<trans_type>()
                .Property(e => e.trans_type_sign)
                .IsUnicode(false);

            modelBuilder.Entity<trans_type>()
                .HasMany(e => e.trans_file)
                .WithRequired(e => e.trans_type)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<treatment>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<treatment>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<treatment>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<uom>()
                .Property(e => e.Uom_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<uom>()
                .Property(e => e.Uom_name)
                .IsUnicode(false);

            modelBuilder.Entity<uom>()
                .Property(e => e.uom_favorite)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<uom>()
                .HasMany(e => e.trans_file)
                .WithRequired(e => e.uom)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<uom_conversion>()
                .Property(e => e.from_uom_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<uom_conversion>()
                .Property(e => e.to_uom_code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<user_rights>()
                .Property(e => e.right_description)
                .IsUnicode(false);

            modelBuilder.Entity<user_rights>()
                .Property(e => e.user_right_title)
                .IsUnicode(false);

            modelBuilder.Entity<vat>()
                .Property(e => e.vatcode)
                .IsUnicode(false);

            modelBuilder.Entity<vat>()
                .Property(e => e.vat_description)
                .IsUnicode(false);

            modelBuilder.Entity<vat>()
                .Property(e => e.vat_percentage)
                .HasPrecision(38, 2);

            modelBuilder.Entity<vat>()
                .Property(e => e.vat_active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_category>()
                .Property(e => e.visit_charge_category_description)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.checked_allowed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.item_id)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.item_description)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.cancelled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.cancellation_comment)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.sage_invoice_no)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.unit_price)
                .HasPrecision(38, 2);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.full_item_description)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.invoice_number)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.request_comment)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.rejected_reason)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_items>()
                .Property(e => e.item_code)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_price>()
                .Property(e => e.visit_charge_price_description)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_price>()
                .Property(e => e.price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<visit_charge_price>()
                .Property(e => e.item_id)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_price>()
                .Property(e => e.corporate_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<visit_charge_price>()
                .Property(e => e.item_code)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charge_price>()
                .Property(e => e.sage_cost_account)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charges>()
                .Property(e => e.charge_ref)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charges>()
                .Property(e => e.charge_total)
                .HasPrecision(18, 0);

            modelBuilder.Entity<visit_charges>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<visit_charges>()
                .Property(e => e.amount_paid)
                .HasPrecision(18, 0);

            modelBuilder.Entity<visit_charges>()
                .Property(e => e.balance_due)
                .HasPrecision(18, 0);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.cycle_completed)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.pending_page_title)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.re_visit)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.chw_name)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.over_five)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.chw_cell_no)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.patient_facility_number)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.patient_support_number)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.marital_status)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.occupation)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.education_level)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.proffession)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.telephone)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.sub_location)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.village)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.distance_travelled)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.ambulance_comment)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.ambulance_no)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.case_under_review)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.stay_duration)
                .HasPrecision(18, 0);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.new_to_los)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.inpatient)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.discharged)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.discharge_mode)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.skip_triage)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.payment_method_id)
                .HasPrecision(36, 2);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.admission_type)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.doctor_discharge_mode)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.referral_type)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.referring_entity)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .Property(e => e.referral_location)
                .IsUnicode(false);

            modelBuilder.Entity<visit_cycle>()
                .HasMany(e => e.surgery_details_trans)
                .WithOptional(e => e.visit_cycle)
                .HasForeignKey(e => e.cycle_id);

            modelBuilder.Entity<visit_cycle>()
                .HasMany(e => e.surgery_details_trans1)
                .WithOptional(e => e.visit_cycle1)
                .HasForeignKey(e => e.cycle_id);

            modelBuilder.Entity<visit_cycle>()
                .HasMany(e => e.vitals)
                .WithRequired(e => e.visit_cycle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<visit_service_register>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_service_register>()
                .Property(e => e.done)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_service_register>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<visit_service_register>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<visit_service_register>()
                .Property(e => e.paid)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<visit_service_register>()
                .Property(e => e.servicecomment)
                .IsUnicode(false);

            modelBuilder.Entity<visit_service_register>()
                .Property(e => e.cancellation_comment)
                .IsUnicode(false);

            modelBuilder.Entity<vital>()
                .Property(e => e.bp)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vital>()
                .Property(e => e.oxygen_saturation)
                .HasPrecision(12, 2);

            modelBuilder.Entity<vital>()
                .Property(e => e.bmi)
                .HasPrecision(12, 2);

            modelBuilder.Entity<vital>()
                .Property(e => e.updated)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vital>()
                .Property(e => e.edd)
                .IsUnicode(false);

            modelBuilder.Entity<vital>()
                .Property(e => e.edd_uncertain)
                .IsUnicode(false);

            modelBuilder.Entity<vital>()
                .Property(e => e.visual_acuity)
                .IsUnicode(false);

            modelBuilder.Entity<vital>()
                .Property(e => e.right_eye)
                .IsUnicode(false);

            modelBuilder.Entity<vital>()
                .Property(e => e.left_eye)
                .IsUnicode(false);

            modelBuilder.Entity<vital>()
                .Property(e => e.BSA)
                .HasPrecision(38, 2);

            modelBuilder.Entity<ward_bed_price>()
                .Property(e => e.price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ward_beds>()
                .Property(e => e.ward_bed_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ward_beds>()
                .Property(e => e.ward_bed_occupancy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ward_beds>()
                .Property(e => e.item_code)
                .IsUnicode(false);

            modelBuilder.Entity<ward_beds>()
                .Property(e => e.account_id)
                .IsUnicode(false);

            modelBuilder.Entity<ward_beds>()
                .Property(e => e.sage_cost_account)
                .IsUnicode(false);

            modelBuilder.Entity<reciept_compare>()
                .Property(e => e.reason_not_posted)
                .IsUnicode(false);

            modelBuilder.Entity<scheduled_sms_types>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<scheduled_sms_types>()
                .Property(e => e.body)
                .IsUnicode(false);

            modelBuilder.Entity<sysarticle>()
                .Property(e => e.schema_option)
                .IsFixedLength();

            modelBuilder.Entity<syspublication>()
                .Property(e => e.snapshot_jobid)
                .IsFixedLength();

            modelBuilder.Entity<syspublication>()
                .Property(e => e.min_autonosync_lsn)
                .IsFixedLength();

            modelBuilder.Entity<sysschemaarticle>()
                .Property(e => e.schema_option)
                .IsFixedLength();

            modelBuilder.Entity<syssubscription>()
                .Property(e => e.distribution_jobid)
                .IsFixedLength();

            modelBuilder.Entity<syssubscription>()
                .Property(e => e.timestamp)
                .IsFixedLength();

            modelBuilder.Entity<systranschema>()
                .Property(e => e.startlsn)
                .IsFixedLength();

            modelBuilder.Entity<systranschema>()
                .Property(e => e.endlsn)
                .IsFixedLength();

            modelBuilder.Entity<user_rights_category>()
                .Property(e => e.right_category_desc)
                .IsUnicode(false);
        }
    }
}