using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class visit_charge_price
    {
        [Key] public Guid visit_charge_price_id { get; set; }

        [StringLength(250)] public string visit_charge_price_description { get; set; }

        public int? visit_charge_category_id { get; set; }

        public decimal? price { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public int? edited_by { get; set; }

        public DateTime? date_created { get; set; }

        public DateTime? date_edited { get; set; }

        public Guid? Scheme_ID { get; set; }

        public int? active { get; set; }

        public Guid? case_type_id { get; set; }

        [StringLength(250)] public string item_id { get; set; }

        public decimal? corporate_price { get; set; }

        public int? account_id { get; set; }

        [StringLength(50)] public string item_code { get; set; }

        public byte is_consul_item { get; set; }

        public decimal? regular_retail_price { get; set; }

        public int? isvatable { get; set; }

        [StringLength(150)] public string sage_cost_account { get; set; }
    }
}