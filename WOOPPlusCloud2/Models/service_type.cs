using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class service_type
    {
        [Key] public int service_type_id { get; set; }

        public int? line_of_service_id { get; set; }

        [StringLength(250)] public string service_type_desc { get; set; }

        public int? Active { get; set; }

        public int? account_id { get; set; }

        public int? center_id { get; set; }

        [StringLength(150)] public string sage_cost_account { get; set; }
    }
}