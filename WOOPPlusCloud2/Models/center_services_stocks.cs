using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_services_stocks
    {
        [Key] public Guid service_stock_id { get; set; }

        public int? service_id { get; set; }

        public Guid? center_product_id { get; set; }

        public int? quantity { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? center_id { get; set; }

        public int? Active { get; set; }

        public Guid? dispensing_location_id { get; set; }
    }
}