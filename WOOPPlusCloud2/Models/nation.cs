using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("nation")]
    public class nation
    {
        [Key] public int nation_id { get; set; }

        [StringLength(250)] public string nation_name { get; set; }

        public int province_count { get; set; }
    }
}