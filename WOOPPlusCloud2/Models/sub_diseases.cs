using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_diseases
    {
        [Key] public int sub_disease_id { get; set; }

        [Required] [StringLength(250)] public string sub_disease_description { get; set; }

        public int? disease_id { get; set; }

        [StringLength(250)] public string disease_code { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public int? IsDeleted { get; set; }

        public int? center_id { get; set; }
    }
}