using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_lab_test
    {
        [Key] public int sub_lab_test_id { get; set; }

        [StringLength(250)] public string sub_lab_test_description { get; set; }

        public int? lab_test_id { get; set; }

        [StringLength(50)] public string underfive { get; set; }

        [StringLength(50)] public string maleadult { get; set; }

        [StringLength(50)] public string femaleadult { get; set; }

        [StringLength(50)] public string unit { get; set; }

        public int? center_id { get; set; }

        public int? IsDeleted { get; set; }

        [StringLength(250)] public string underfive_critical { get; set; }

        [StringLength(250)] public string maleadult_critical { get; set; }

        [StringLength(250)] public string femaleadult_critical { get; set; }
    }
}