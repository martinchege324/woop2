using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_users
    {
        [Key] public Guid center_user_id { get; set; }

        public int? staff_id { get; set; }

        public int? center_id { get; set; }

        [StringLength(1)] public string allowed { get; set; }
    }
}