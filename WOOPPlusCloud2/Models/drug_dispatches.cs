using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class drug_dispatches
    {
        [Key] public Guid drug_dispatch_id { get; set; }

        public string order_delivery_location { get; set; }

        public DateTime? order_recieved_on { get; set; }

        public string trans_type { get; set; }

        public decimal? total_charge { get; set; }

        public string drug_dispatch_comment { get; set; }

        public int? rider { get; set; }

        public int? signed_invoice { get; set; }

        public DateTime? signed_invoice_date { get; set; }

        public int? original_prescription { get; set; }

        public DateTime? original_prescription_date { get; set; }

        public DateTime? prescription_date { get; set; }

        public string prescription_facility { get; set; }

        public int? claim_form { get; set; }

        public DateTime? claim_form_date { get; set; }

        public int? smart { get; set; }

        public DateTime? smart_date { get; set; }

        public string after_disptach_comment { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public DateTime? edited_on { get; set; }

        public int? edited_by { get; set; }

        [StringLength(250)] public string diagnosis { get; set; }

        public virtual center center { get; set; }
    }
}