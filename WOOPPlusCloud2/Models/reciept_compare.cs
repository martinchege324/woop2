using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class reciept_compare
    {
        [Key] [Column(Order = 0)] public int sage_failed_transaction_id { get; set; }

        [Key] [Column(Order = 1)] public Guid visit_cycle_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int is_cash_transaction { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(150)]
        public string reason_not_posted { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int reposting_successful { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int center_id { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cannot_repost { get; set; }
    }
}