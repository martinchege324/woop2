using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("payment")]
    public class payment
    {
        [Key] public Guid payment_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        public Guid cycle_id { get; set; }

        [Required] [StringLength(1)] public string patient_able_to_pay { get; set; }

        [Column(TypeName = "numeric")] public decimal chargeable_amount { get; set; }

        [Column(TypeName = "numeric")] public decimal cash_paid { get; set; }

        [Column(TypeName = "numeric")] public decimal voucher_amount { get; set; }

        [Column(TypeName = "numeric")] public decimal nhif_amount { get; set; }

        [Column(TypeName = "numeric")] public decimal payment_balance { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [Column(TypeName = "numeric")] public decimal deposit { get; set; }

        [StringLength(250)] public string commment { get; set; }
    }
}