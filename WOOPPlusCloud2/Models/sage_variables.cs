using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sage_variables
    {
        [Key] public int variable_id { get; set; }

        [Required] [StringLength(50)] public string description { get; set; }

        [StringLength(50)] public string value { get; set; }

        public int? center_id { get; set; }
    }
}