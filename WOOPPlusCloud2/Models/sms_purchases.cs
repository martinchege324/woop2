using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sms_purchases
    {
        [Key] public int sms_purchase_id { get; set; }

        public int? smses_bought { get; set; }

        public decimal? rate { get; set; }

        public string token_inputed { get; set; }

        public DateTime? purchase_date { get; set; }

        public int? purchased_by { get; set; }

        public int? center_id { get; set; }
    }
}