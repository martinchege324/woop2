using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class maternity_apgars
    {
        [Key] public int maternity_apgars_id { get; set; }

        [StringLength(250)] public string maternity_apgars_desc { get; set; }

        public int? maternity_apgars_category_id { get; set; }
    }
}