using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class decision_support
    {
        [Key] public Guid decision_support_id { get; set; }

        public int? decision_support_type_id { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public int? updated { get; set; }

        public DateTime? date { get; set; }
    }
}