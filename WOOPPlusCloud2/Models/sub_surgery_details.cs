using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class sub_surgery_details
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sub_surgery_details()
        {
            surgery_details_trans = new HashSet<surgery_details_trans>();
            surgery_details_trans1 = new HashSet<surgery_details_trans>();
        }

        [Key] public int sub_surgery_details_id { get; set; }

        [Required] [StringLength(250)] public string sub_surgery_details_desc { get; set; }

        public int? surgery_details_id { get; set; }

        public virtual surgery_details surgery_details { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<surgery_details_trans> surgery_details_trans { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<surgery_details_trans> surgery_details_trans1 { get; set; }
    }
}