using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class incidents_reviews
    {
        [Key] public Guid incident_review_id { get; set; }

        public string incident_ref { get; set; }

        public int? servere_outcome { get; set; }

        public int? staff_id { get; set; }

        public string comment { get; set; }

        public DateTime? review_date { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }
    }
}