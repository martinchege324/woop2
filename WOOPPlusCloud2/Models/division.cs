using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("division")]
    public class division
    {
        [Key] public int division_id { get; set; }

        [StringLength(250)] public string division_name { get; set; }

        public int center_count { get; set; }

        public int? constituency_id { get; set; }
    }
}