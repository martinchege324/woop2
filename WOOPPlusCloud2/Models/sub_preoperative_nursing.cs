using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_preoperative_nursing
    {
        [Key] public int sub_preoperative_nursing_id { get; set; }

        [Required] [StringLength(250)] public string sub_preoperative_nursing_desc { get; set; }

        public int? preoperative_nursing_id { get; set; }
    }
}