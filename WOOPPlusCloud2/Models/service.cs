using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class service
    {
        [Key] public int service_id { get; set; }

        [Required] [StringLength(250)] public string service_desc { get; set; }

        public int? service_type_id { get; set; }

        public int? service_price { get; set; }

        public int? updatedby_staff_id { get; set; }

        public DateTime? update_date { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public int? done_by { get; set; }

        [StringLength(1)] public string checklist_item { get; set; }

        [StringLength(250)] public string services_code { get; set; }

        public int? Active { get; set; }

        public int? center_id { get; set; }

        public int? isvatable { get; set; }
    }
}