using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class preoperative_nursing
    {
        [Key] public int preoperative_nursing_id { get; set; }

        [StringLength(250)] public string preoperative_nursing_name { get; set; }

        public int? preoperative_nursing_categ_id { get; set; }
    }
}