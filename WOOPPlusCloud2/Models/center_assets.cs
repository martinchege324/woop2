using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class center_assets
    {
        [Key] public Guid center_asset_id { get; set; }

        public Guid asset_id { get; set; }

        public int center_id { get; set; }

        [StringLength(50)] public string center_asset_sn { get; set; }

        [StringLength(250)] public string comment { get; set; }

        [StringLength(50)] public string center_asset_make { get; set; }

        [StringLength(50)] public string center_asset_type { get; set; }

        [Column(TypeName = "date")] public DateTime? center_asset_dop { get; set; }

        [Column(TypeName = "date")] public DateTime? center_asset_dos { get; set; }

        [StringLength(1)] public string active { get; set; }

        public int? staff_id { get; set; }

        public DateTime? date { get; set; }
    }
}