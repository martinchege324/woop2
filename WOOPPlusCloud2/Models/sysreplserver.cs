using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sysreplserver
    {
        [Key] public string srvname { get; set; }

        public int? srvid { get; set; }
    }
}