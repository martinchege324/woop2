using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class center_service_price
    {
        [Key] public Guid center_service_price_id { get; set; }

        [Column(TypeName = "numeric")] public decimal? center_service_price_amount { get; set; }

        public int? center_id { get; set; }

        public int? service_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public Guid? Scheme_ID { get; set; }

        public int? Active { get; set; }

        public decimal? center_service_corporate_price { get; set; }

        public int? allow_price_adjustment { get; set; }
    }
}