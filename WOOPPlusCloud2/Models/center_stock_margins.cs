using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_stock_margins
    {
        [Key] public Guid center_stock_margin_id { get; set; }

        public decimal? margin { get; set; }

        public decimal? margincorporate { get; set; }

        public decimal? marginhospital { get; set; }

        public decimal? marginretail { get; set; }

        public int? center_id { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public int? updatedby_staff_id { get; set; }

        public DateTime? update_date { get; set; }

        public int? active { get; set; }

        public decimal? walkin_discount { get; set; }

        public int? VAT { get; set; }
    }
}