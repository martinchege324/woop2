using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class survey_answers
    {
        [Key] public Guid survey_answer_id { get; set; }

        public string survey_answer_desc { get; set; }

        public Guid? survey_question_id { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? edited_by { get; set; }

        public int? center_id { get; set; }
    }
}