using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_labtest_stocks
    {
        [Key] public Guid labtest_stock_id { get; set; }

        public int? lab_test_id { get; set; }

        public Guid? center_product_id { get; set; }

        public decimal quantity { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? center_id { get; set; }

        public int? Active { get; set; }

        public Guid? dispensing_location_id { get; set; }

        public decimal? cost_per_test { get; set; }
    }
}