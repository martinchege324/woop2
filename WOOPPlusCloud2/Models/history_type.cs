using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class history_type
    {
        [Key] public int history_type_id { get; set; }

        [Required] [StringLength(250)] public string history_type_description { get; set; }

        public int? history_category_id { get; set; }

        public int? center_id { get; set; }

        public int? history_group { get; set; }
    }
}