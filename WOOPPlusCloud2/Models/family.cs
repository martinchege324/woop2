using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("family")]
    public class family
    {
        [Key] public Guid family_id { get; set; }

        public string family_name { get; set; }

        public string phone_no { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        [StringLength(50)] public string family_facility_number { get; set; }
    }
}