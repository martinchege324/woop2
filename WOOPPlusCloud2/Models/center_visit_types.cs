//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WOOPPlusCloud2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class center_visit_types
    {
        [Key]
        public int visit_type_id { get; set; }
        public string visit_type_desc { get; set; }
        public Nullable<int> center_id { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public Nullable<int> updated_by { get; set; }
        public Nullable<int> active { get; set; }
        public Nullable<int> facility_id { get; set; }
    }
}
