using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class operation_technique
    {
        [Key] public Guid operation_technique_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        public Guid? cycle_id { get; set; }

        public int? staff_id { get; set; }

        public int? center_id { get; set; }

        public int? incision_type_id { get; set; }

        public int? incision_site_id { get; set; }

        public int? incision_position_id { get; set; }

        public int? updated { get; set; }

        [StringLength(250)] public string incision_type_other { get; set; }

        [StringLength(250)] public string incision_site_other { get; set; }

        [StringLength(250)] public string incision_position_other { get; set; }

        [StringLength(250)] public string comment { get; set; }
    }
}