using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class protocols_stock_mapping
    {
        [Key] public Guid protocol_stock_maping_id { get; set; }

        public Guid protocol_id { get; set; }

        public Guid center_product_id { get; set; }

        public int quantity { get; set; }

        public int active { get; set; }

        public int requirebsa { get; set; }

        public int center_id { get; set; }

        public int created_by { get; set; }

        public DateTime created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public string comments { get; set; }

        public int? strength { get; set; }
    }
}