using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("training")]
    public class training
    {
        [Key] public Guid training_id { get; set; }

        public int? line_of_service_id { get; set; }

        [StringLength(250)] public string training_type { get; set; }

        [StringLength(250)] public string diagnostic_category { get; set; }

        [StringLength(250)] public string training_organiser { get; set; }

        [StringLength(250)] public string training_level { get; set; }

        [StringLength(250)] public string training_location { get; set; }

        [Column(TypeName = "date")] public DateTime? training_start_date { get; set; }

        [Column(TypeName = "date")] public DateTime? training_end_date { get; set; }

        public DateTime? Update_date { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(250)] public string training_name { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(1)] public string updated { get; set; }

        [StringLength(250)] public string comment { get; set; }
    }
}