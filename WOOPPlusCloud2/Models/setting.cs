using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class setting
    {
        [Key] public Guid setting_id { get; set; }

        public string setting_desc { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }
    }
}