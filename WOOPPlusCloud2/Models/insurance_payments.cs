using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class insurance_payments
    {
        [Key] public Guid insurance_payment_id { get; set; }

        public Guid? payment_account_id { get; set; }

        public decimal? payment_amount { get; set; }

        public decimal? unallocated_amount { get; set; }

        public int? allocation_complete { get; set; }

        public DateTime? allocation_complete_date { get; set; }

        public int? center_id { get; set; }

        public DateTime? payment_date { get; set; }

        public Guid? bank_trans_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? active { get; set; }

        [StringLength(250)] public string payment_reff { get; set; }

        [StringLength(250)] public string payment_comment { get; set; }

        public Guid? payment_method_id { get; set; }

        public virtual bank_transactions bank_transactions { get; set; }

        public virtual payment_accounts payment_accounts { get; set; }
    }
}