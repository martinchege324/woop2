using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class doctor_order
    {
        [Key] public Guid doctor_order_id { get; set; }

        [StringLength(250)] public string doctor_order_desc { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public int? edited_by { get; set; }

        public int? done { get; set; }

        public DateTime? date_created { get; set; }

        public DateTime? date_done { get; set; }

        public int? done_by { get; set; }

        public int? isdeleted { get; set; }
    }
}