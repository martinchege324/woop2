using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class survey_questions
    {
        [Key] public Guid survey_question_id { get; set; }

        public string survey_question_desc { get; set; }

        public int? module_id { get; set; }

        public int? center_id { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? edited_by { get; set; }
    }
}