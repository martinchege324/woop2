using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class hr_departments
    {
        [Key] public Guid hr_department_id { get; set; }

        [StringLength(250)] public string hr_department_desc { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }
    }
}