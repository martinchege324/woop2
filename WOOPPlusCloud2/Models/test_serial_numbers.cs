using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class test_serial_numbers
    {
        [Key] public Guid test_serial_no_id { get; set; }

        public int count { get; set; }

        [Required] [StringLength(3)] public string code { get; set; }

        public int center_id { get; set; }
    }
}