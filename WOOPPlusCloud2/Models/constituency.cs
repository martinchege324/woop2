using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("constituency")]
    public class constituency
    {
        [Key] public int constituency_id { get; set; }

        [StringLength(250)] public string constituency_name { get; set; }

        public int division_count { get; set; }

        public int? district_id { get; set; }
    }
}