//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WOOPPlusCloud2.Models
{
    using System;
    using System.ComponentModel;

    public partial class PatientsReport_Result
    {
        public System.Guid cycle_id { get; set; }
        [DisplayName("Date")]
        public string DATE { get; set; }
        [DisplayName("Staff")]
        public string Staff { get; set; }
        [DisplayName("Invoice")]
        public string INVOICE { get; set; }
        public string OPNO { get; set; }
        [DisplayName("Names")]
        public string NAMES { get; set; }
        [DisplayName("Gender")]
        public string gender { get; set; }
        [DisplayName("Age")]
        public Nullable<int> AGE { get; set; }
        [DisplayName("InsuranceCode")]
        public string INSURANCECODE { get; set; }
        [DisplayName("Insurance")]
        public string INSURANCE { get; set; }
        [DisplayName("SchemeCode")]
        public string SCHEMECODE { get; set; }
        [DisplayName("Scheme")]
        public string SCHEME { get; set; }
        [DisplayName("Visit")]
        public string VISITTYPE { get; set; }
        [DisplayName("Package")]
        public string Package { get; set; }
        public Nullable<int> invoice_closed { get; set; }
        public string CENTER { get; set; }
    }
}
