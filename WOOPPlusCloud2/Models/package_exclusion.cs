using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class package_exclusion
    {
        [Key] public Guid package_exclusion_id { get; set; }

        [StringLength(50)] public string package_exclusion_desc { get; set; }

        public string package_exclusion_item_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        public Guid? package_item_id { get; set; }

        public int? center_id { get; set; }

        public int? active { get; set; }

        public byte? inclusion { get; set; }
    }
}