using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class patient_reminders
    {
        [Key] public Guid patient_reminder_id { get; set; }

        public string patient_reminder_comment { get; set; }

        public DateTime? patient_reminder_date { get; set; }

        public TimeSpan? patient_reminder_time { get; set; }

        public DateTime? reminder_sent_date { get; set; }

        public int? active { get; set; }

        public int? assigned_doctor { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? update_date { get; set; }

        public Guid? patient_id { get; set; }

        public int? center_id { get; set; }

        public int? patient_reminder_type_id { get; set; }

        public int? patient_reminder_resolution_id { get; set; }

        [StringLength(150)] public string reminder_resolution_comment { get; set; }

        public int? sms_sent { get; set; }

        public DateTime? sms_sent_time { get; set; }
    }
}