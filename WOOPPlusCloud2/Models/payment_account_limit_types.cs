using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class payment_account_limit_types
    {
        [Key] public Guid payment_account_limit_type_id { get; set; }

        [StringLength(50)] public string payment_account_limit_type_desc { get; set; }

        public DateTime? created_on { get; set; }

        public int? created_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? edited_by { get; set; }
    }
}