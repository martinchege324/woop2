using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class patient_symptoms
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public patient_symptoms()
        {
            symptoms_diagnosis = new HashSet<symptoms_diagnosis>();
        }

        [Key] public int patient_symptom_id { get; set; }

        [StringLength(250)] public string patient_symptom_description { get; set; }

        public int? counter { get; set; }

        public int? center_id { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<symptoms_diagnosis> symptoms_diagnosis { get; set; }
    }
}