using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class surgery_details_trans
    {
        [Key] public Guid surgery_details_trans_id { get; set; }

        public DateTime? date { get; set; }

        public DateTime? time { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public int? surgery_details_id { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public int? sub_surgery_details_id { get; set; }

        public DateTime? start_time { get; set; }

        public DateTime? end_time { get; set; }

        public Guid? surgeries_id { get; set; }

        public int? selected_staff_id { get; set; }

        [StringLength(250)] public string remarks { get; set; }

        public virtual center center { get; set; }

        public virtual center center1 { get; set; }

        public virtual staff staff { get; set; }

        public virtual staff staff1 { get; set; }

        public virtual sub_surgery_details sub_surgery_details { get; set; }

        public virtual sub_surgery_details sub_surgery_details1 { get; set; }

        public virtual surgery_details surgery_details { get; set; }

        public virtual visit_cycle visit_cycle { get; set; }

        public virtual visit_cycle visit_cycle1 { get; set; }
    }
}