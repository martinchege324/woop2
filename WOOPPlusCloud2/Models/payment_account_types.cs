using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class payment_account_types
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int payment_account_type_id { get; set; }

        [StringLength(50)] public string payment_account_type_desc { get; set; }
    }
}