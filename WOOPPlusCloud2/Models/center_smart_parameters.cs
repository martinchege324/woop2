using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class center_smart_parameters
    {
        [Key] public Guid smart_parameters_id { get; set; }

        public string postman_token { get; set; }

        public string token { get; set; }

        public string smart_key { get; set; }

        public string globalIdentifier1 { get; set; }

        public string globalIdentifier2 { get; set; }

        public string globalIdentifier3 { get; set; }

        public string initialize_url { get; set; }

        public string member_details_url { get; set; }

        public string member_benefits_url { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? center_id { get; set; }

        public string userId { get; set; }

        [StringLength(250)] public string smartdbconnection { get; set; }
    }
}