using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("counselling")]
    public class counselling
    {
        [Key] public Guid counselling_id { get; set; }

        public int? counselling_type_id { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public int? updated { get; set; }

        public DateTime? date { get; set; }
    }
}