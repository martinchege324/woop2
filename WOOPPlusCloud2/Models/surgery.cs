using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class surgery
    {
        [Key] public Guid surgeries_id { get; set; }

        public DateTime? scheduled_date { get; set; }

        public int? scheduled_by { get; set; }

        [StringLength(1)] public string done { get; set; }

        public int? service_id { get; set; }

        public Guid? cycle_id { get; set; }

        public int? sub_service_id { get; set; }

        public int? center_id { get; set; }

        public int? surgeon_id { get; set; }

        public int? prescribed_by { get; set; }

        public int? wheeled_in_by { get; set; }

        public int? wheeled_out_by { get; set; }

        public int? surgery_visit_type_id { get; set; }

        public DateTime? start_time { get; set; }

        public DateTime? end_time { get; set; }

        [StringLength(250)] public string comment { get; set; }

        [StringLength(1)] public string pending { get; set; }

        public DateTime? pending_start_time { get; set; }

        public DateTime? pending_end_time { get; set; }

        public Guid? theatre_id { get; set; }

        public DateTime? date_done { get; set; }

        [StringLength(1)] public string patient_alive { get; set; }

        [StringLength(250)] public string discharge_note { get; set; }

        [StringLength(1)] public string paid { get; set; }

        [StringLength(1)] public string surgery_approved { get; set; }

        public int? approved_by { get; set; }
    }
}