using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class asset
    {
        [Key] public Guid asset_id { get; set; }

        public int asset_type_id { get; set; }

        [Required] [StringLength(250)] public string asset_description { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public DateTime? date { get; set; }
    }
}