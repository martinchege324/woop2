using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class bank_transactions
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public bank_transactions()
        {
            insurance_payments = new HashSet<insurance_payments>();
        }

        [Key] public Guid bank_trans_id { get; set; }

        public Guid? bank_id { get; set; }

        public decimal? amount { get; set; }

        public int? center_id { get; set; }

        public int? posted_by { get; set; }

        public DateTime? posted_on { get; set; }

        public int? accounting_trans_type_id { get; set; }

        public virtual accounting_trans_types accounting_trans_types { get; set; }

        public virtual bank bank { get; set; }

        public virtual center center { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<insurance_payments> insurance_payments { get; set; }
    }
}