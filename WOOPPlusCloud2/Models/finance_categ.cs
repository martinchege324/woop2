using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class finance_categ
    {
        [Key] public int finance_categ_id { get; set; }

        [StringLength(250)] public string finance_Categ_desc { get; set; }

        public int? categ_active { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public int? center_id { get; set; }
    }
}