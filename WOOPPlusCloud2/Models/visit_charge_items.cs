using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class visit_charge_items
    {
        [Key] public Guid charge_item_id { get; set; }

        public Guid charge_id { get; set; }

        public decimal? charge_amount { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public Guid? cycle_id { get; set; }

        public decimal? quantity { get; set; }

        [StringLength(1)] public string checked_allowed { get; set; }

        public DateTime? payment_date { get; set; }

        [StringLength(50)] public string item_id { get; set; }

        [StringLength(50)] public string item_description { get; set; }

        [StringLength(1)] public string cancelled { get; set; }

        public DateTime? cancelled_on { get; set; }

        public int? cancelled_by { get; set; }

        public Guid? deposit_id { get; set; }

        public decimal? paid_amount { get; set; }

        public decimal? initial_charge_amount { get; set; }

        public Guid? deposit_transaction_id { get; set; }

        public Guid? items_trans_id { get; set; }

        public Guid? payment_deposit_transaction_id { get; set; }

        public int? waived { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public int? inpatient { get; set; }

        [StringLength(250)] public string cancellation_comment { get; set; }

        public Guid? supplier_id { get; set; }

        [StringLength(50)] public string sage_invoice_no { get; set; }

        public int? sage_failed_transaction_id { get; set; }

        public Guid? payment_made_by_id { get; set; }

        public decimal? unit_price { get; set; }

        public DateTime? charge_date { get; set; }

        [StringLength(250)] public string full_item_description { get; set; }

        public int? proforma { get; set; }

        public int? isinsurance { get; set; }

        public int? isinpatient { get; set; }

        [StringLength(250)] public string invoice_number { get; set; }

        public DateTime? item_created_date { get; set; }

        public int? insurance_invoice_closed { get; set; }

        public int? item_created_by { get; set; }

        public int? refund_requested { get; set; }

        public int? requested_by { get; set; }

        public DateTime? requested_date { get; set; }

        [StringLength(250)] public string request_comment { get; set; }

        public int? refund_approved { get; set; }

        public int? approved_by { get; set; }

        public DateTime? approved_date { get; set; }

        public int? refund_rejected { get; set; }

        public int? rejected_by { get; set; }

        public DateTime? rejected_date { get; set; }

        [StringLength(250)] public string rejected_reason { get; set; }

        public decimal? total_avgretail_price { get; set; }

        public decimal? savings { get; set; }

        public int? invoice_validated_by { get; set; }

        public DateTime? invoice_validated_date { get; set; }

        public decimal? regular_retail_price { get; set; }

        public decimal? vat_amount { get; set; }

        public int? is_vatable { get; set; }

        public decimal? unit_cost { get; set; }

        [StringLength(250)] public string item_code { get; set; }
    }
}