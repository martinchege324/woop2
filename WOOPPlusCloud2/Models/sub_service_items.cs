using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_service_items
    {
        [Key] public int sub_service_item_id { get; set; }

        [Required] [StringLength(250)] public string sub_service_item_desc { get; set; }

        public int? sub_service_id { get; set; }

        public int? center_id { get; set; }
    }
}