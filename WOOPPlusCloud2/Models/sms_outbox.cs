using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sms_outbox
    {
        [Key] public Guid outbox_id { get; set; }

        public DateTime date_time_sent { get; set; }

        [Required] [StringLength(1000)] public string text_body { get; set; }

        [StringLength(250)] public string telephone { get; set; }

        public Guid? contact_id { get; set; }

        [StringLength(50)] public string status { get; set; }

        public int? center_id { get; set; }

        public int? sent_by { get; set; }

        public string message { get; set; }

        public int? sms_template_id { get; set; }

        public Guid? cycle_id { get; set; }
    }
}