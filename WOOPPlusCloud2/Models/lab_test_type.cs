using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class lab_test_type
    {
        [Key] public int lab_test_type_id { get; set; }

        [StringLength(250)] public string lab_test_type_desc { get; set; }

        public int? lab_test_specimen_id { get; set; }

        public int? center_id { get; set; }

        public int account_id { get; set; }

        public int? sample_active { get; set; }

        [StringLength(150)] public string sage_cost_account { get; set; }

        public virtual lab_test_specimens lab_test_specimens { get; set; }
    }
}