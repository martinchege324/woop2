﻿using System;
using System.ComponentModel;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WOOPPlusCloud2.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [DisplayName("Roles")]
        public string user_roles
        {
            get
            {
                UserStore<ApplicationUser> userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
                UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(userStore);
                System.Collections.Generic.IList<string> rolesForUser = userManager.GetRoles(this.Id);
                string userRoles = "";
                foreach (string role in rolesForUser)
                {
                    userRoles = (userRoles == "") ? role.ToString() : userRoles + " - " + role.ToString();
                }
                return userRoles;
            }
           
 
        }
        [DisplayName("Active")]
        public Boolean Is_Active { get; set; }
        [DisplayName("Approved")]
        public Boolean Is_Approved { get; set; }
        //public string Password { set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            ClaimsIdentity userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie).ConfigureAwait(false);
            // Add custom user claims here
          
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("ZiDiModels", false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}