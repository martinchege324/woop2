using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class ward_bed_price
    {
        [Key] public Guid ward_bed_price_id { get; set; }

        public Guid? ward_bed_id { get; set; }

        public decimal? price { get; set; }

        public int? center_id { get; set; }

        public int? created_by { get; set; }

        public int? edited_by { get; set; }

        public DateTime? date_created { get; set; }

        public DateTime? date_edited { get; set; }

        public Guid? Scheme_ID { get; set; }

        public int? Active { get; set; }

        public decimal? corporate_ward_bed_price { get; set; }
    }
}