using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class case_type_category
    {
        [Key] public Guid case_type_id { get; set; }

        [StringLength(250)] public string case_type_desc { get; set; }

        public int? center_id { get; set; }
    }
}