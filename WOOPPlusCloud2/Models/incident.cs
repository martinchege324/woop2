using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class incident
    {
        [Key] public Guid incident_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public DateTime? date { get; set; }

        [StringLength(250)] public string value { get; set; }

        [StringLength(250)] public string admin_id { get; set; }

        public Guid? incident_review_id { get; set; }

        public Guid? sub_staff_incident_id { get; set; }

        public Guid? staff_incident_id { get; set; }
    }
}