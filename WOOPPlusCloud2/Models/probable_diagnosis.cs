using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class probable_diagnosis
    {
        [Key] public Guid probable_diagnosis_id { get; set; }

        public int? disease_id { get; set; }

        [StringLength(250)] public string disease_name { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public DateTime? Date { get; set; }
    }
}