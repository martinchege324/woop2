using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class preoperative_category
    {
        [Key] public int preoperative_category_id { get; set; }

        [StringLength(250)] public string preoperative_category_description { get; set; }
    }
}