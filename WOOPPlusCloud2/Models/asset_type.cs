using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class asset_type
    {
        [Key] public int asset_type_id { get; set; }

        [Required] [StringLength(250)] public string asset_type_description { get; set; }
    }
}