using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class losfollowup_type
    {
        [Key] public int losfollowup_type_id { get; set; }

        [StringLength(250)] public string losfollowup_type_desc { get; set; }

        public int? line_of_service_id { get; set; }

        public int? center_id { get; set; }

        [StringLength(50)] public string bgcolor { get; set; }
    }
}