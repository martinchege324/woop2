using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class inpatient_queue
    {
        [Key] public Guid inpatient_queue_id { get; set; }

        public int? q_level { get; set; }

        public Guid? cycle_id { get; set; }

        public int? que_details_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public int? sub_que_details_id { get; set; }

        [StringLength(1)] public string done { get; set; }

        [StringLength(1)] public string discharge { get; set; }

        public DateTime? time { get; set; }

        public Guid? consultation_id { get; set; }

        public int? pend_que { get; set; }

        public int? service_type_id { get; set; }

        public DateTime? queue_closed_date { get; set; }

        public DateTime? queue_closed_by { get; set; }
    }
}