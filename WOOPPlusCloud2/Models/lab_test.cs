using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class lab_test
    {
        [Key] public int lab_test_id { get; set; }

        public int lab_test_categ_id { get; set; }

        [Required] [StringLength(250)] public string lab_test_desc { get; set; }

        [Column(TypeName = "numeric")] public decimal lab_test_price { get; set; }

        [StringLength(1)] public string referred { get; set; }

        [StringLength(50)] public string lab_test_code { get; set; }

        public int? updatedby_staff_id { get; set; }

        public DateTime? update_date { get; set; }

        [StringLength(1)] public string special { get; set; }

        public int? center_id { get; set; }

        public int? IsDeleted { get; set; }

        public int? isvatable { get; set; }

        public decimal? unit_cost { get; set; }
    }
}