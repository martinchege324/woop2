using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sign
    {
        [Key] public Guid sign_id { get; set; }

        public int? sign_value { get; set; }

        public Guid? cycle_id { get; set; }

        public int? sub_patient_sign_id { get; set; }

        public int? patient_sign_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public DateTime? date { get; set; }
    }
}