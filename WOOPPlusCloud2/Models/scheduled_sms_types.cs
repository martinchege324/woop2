using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class scheduled_sms_types
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long scheduled_sms_type_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int center_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string name { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(160)]
        public string body { get; set; }
    }
}