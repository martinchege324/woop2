using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("treatment")]
    public class treatment
    {
        [Key] public Guid treatment_id { get; set; }

        public int patient_treatment_id { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        public int? sub_patient_treatment_id { get; set; }

        public Guid? cycle_id { get; set; }

        [StringLength(250)] public string value { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(250)] public string comment { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        public virtual center center { get; set; }

        public virtual patient_treatment patient_treatment { get; set; }

        public virtual staff staff { get; set; }

        public virtual sub_patient_treatment sub_patient_treatment { get; set; }

        public virtual visit_cycle visit_cycle { get; set; }
    }
}