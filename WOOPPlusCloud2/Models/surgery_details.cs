using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class surgery_details
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public surgery_details()
        {
            sub_surgery_details = new HashSet<sub_surgery_details>();
            surgery_details_trans = new HashSet<surgery_details_trans>();
        }

        [Key] public int surgery_details_id { get; set; }

        [StringLength(250)] public string surgery_details_name { get; set; }

        public int? surgery_details_categ_id { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sub_surgery_details> sub_surgery_details { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<surgery_details_trans> surgery_details_trans { get; set; }
    }
}