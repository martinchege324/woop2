using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sms_contacts
    {
        [Key] public Guid contact_id { get; set; }

        [StringLength(50)] public string surname { get; set; }

        [Required] [StringLength(50)] public string other_names { get; set; }

        [StringLength(50)] public string telephone { get; set; }

        public Guid contact_group_id { get; set; }

        public int? created_by { get; set; }

        [StringLength(250)] public string email_address { get; set; }

        public int? contact_active { get; set; }

        public int? center_id { get; set; }

        public virtual sms_contact_groups sms_contact_groups { get; set; }
    }
}