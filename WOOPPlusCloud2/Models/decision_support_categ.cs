using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class decision_support_categ
    {
        [Key] public int decision_support_categ_id { get; set; }

        [Required] [StringLength(250)] public string decision_support_categ_desc { get; set; }

        public int? line_of_service_id { get; set; }
    }
}