using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class losfollowup_trans
    {
        [Key] public Guid losfollowup_trans_id { get; set; }

        public int? losfollowup_id { get; set; }

        public DateTime? date { get; set; }

        [StringLength(250)] public string comment { get; set; }

        [StringLength(250)] public string value { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public int? losfollowup_category_id { get; set; }

        public int? losfollowup_type_id { get; set; }

        [StringLength(250)] public string visit { get; set; }
    }
}