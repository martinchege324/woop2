using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class staff_category
    {
        [Key] public int staff_category_id { get; set; }

        [Required] [StringLength(250)] public string staff_category_desc { get; set; }
    }
}