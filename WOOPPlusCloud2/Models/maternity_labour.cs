using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class maternity_labour
    {
        [Key] public Guid maternity_labour_id { get; set; }

        public int? maternity_labour_management_id { get; set; }

        public int sub_maternity_labour_management_id { get; set; }

        [StringLength(250)] public string value { get; set; }

        [StringLength(1)] public string updated { get; set; }

        public Guid? cycle_id { get; set; }

        public int? staff_id { get; set; }

        public int? center_id { get; set; }

        [StringLength(250)] public string comment { get; set; }
    }
}