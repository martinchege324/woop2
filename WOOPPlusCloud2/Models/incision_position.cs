using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class incision_position
    {
        [Key] public int incision_position_id { get; set; }

        [StringLength(250)] public string incision_position_desc { get; set; }

        public double? operation_charge { get; set; }
    }
}