using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class sub_patient_symptoms
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sub_patient_symptoms()
        {
            symptoms_diagnosis = new HashSet<symptoms_diagnosis>();
        }

        [Key] public int sub_patient_symptom_id { get; set; }

        [Required] [StringLength(250)] public string sub_patient_symptom_description { get; set; }

        public int? patient_symptom_id { get; set; }

        public int? center_id { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<symptoms_diagnosis> symptoms_diagnosis { get; set; }
    }
}