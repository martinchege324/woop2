using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_que_details
    {
        [Key] public int sub_que_details_id { get; set; }

        [Required] [StringLength(250)] public string sub_que_desc { get; set; }

        public int que_details_id { get; set; }

        [StringLength(1)] public string IfSpecial { get; set; }
    }
}