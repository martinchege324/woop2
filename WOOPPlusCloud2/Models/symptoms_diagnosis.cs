using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class symptoms_diagnosis
    {
        [Key] public int symptoms_diagnosis_id { get; set; }

        public int? sub_patient_symptom_id { get; set; }

        public int? patient_symptom_id { get; set; }

        public int? disease_id { get; set; }

        public virtual disease disease { get; set; }

        public virtual patient_symptoms patient_symptoms { get; set; }

        public virtual sub_patient_symptoms sub_patient_symptoms { get; set; }
    }
}