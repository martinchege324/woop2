using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class supplier_payments
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public supplier_payments()
        {
            supplier_payment_logs = new HashSet<supplier_payment_logs>();
        }

        [Key] public Guid supplier_payment_trans_id { get; set; }

        public DateTime? supplier_payment_date { get; set; }

        public int? supplier_trans_type_id { get; set; }

        [Column(TypeName = "numeric")] public decimal? supplier_payment_amount { get; set; }

        [StringLength(250)] public string supplier_payment_comment { get; set; }

        [StringLength(50)] public string supplier_payment_ref { get; set; }

        public Guid? supplier_id { get; set; }

        public int? center_id { get; set; }

        public Guid? payment_method_id { get; set; }

        public int? active { get; set; }

        public DateTime? updated_date { get; set; }

        public int? updated_by { get; set; }

        public decimal? vat_ex_amount { get; set; }

        public decimal? vatvalue { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<supplier_payment_logs> supplier_payment_logs { get; set; }
    }
}