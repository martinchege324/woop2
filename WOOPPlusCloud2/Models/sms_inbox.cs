using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sms_inbox
    {
        [Key] public Guid inbox_id { get; set; }

        public DateTime date_time_arrived { get; set; }

        [Required] [StringLength(1000)] public string text_body { get; set; }

        [StringLength(10)] public string telephone { get; set; }

        public Guid? contact_id { get; set; }
    }
}