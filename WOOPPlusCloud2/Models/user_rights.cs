using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class user_rights
    {
        [Key] public int right_id { get; set; }

        [Required] [StringLength(250)] public string right_description { get; set; }

        public int user_right_master_id { get; set; }

        [Required] [StringLength(250)] public string user_right_title { get; set; }

        public int? right_category_id { get; set; }
    }
}