using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class referral_trans
    {
        [Key] public Guid referral_trans_id { get; set; }

        public Guid? referral_data_id { get; set; }

        public Guid? referral_id { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        public string value { get; set; }

        public int? center_id { get; set; }
    }
}