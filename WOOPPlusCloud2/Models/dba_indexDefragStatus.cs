using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class dba_indexDefragStatus
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int databaseID { get; set; }

        [Required] [StringLength(128)] public string databaseName { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int objectID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int indexID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short partitionNumber { get; set; }

        public double fragmentation { get; set; }

        public int page_count { get; set; }

        public long range_scan_count { get; set; }

        [StringLength(128)] public string schemaName { get; set; }

        [StringLength(128)] public string objectName { get; set; }

        [StringLength(128)] public string indexName { get; set; }

        public DateTime scanDate { get; set; }

        public DateTime? defragDate { get; set; }

        public bool printStatus { get; set; }

        public int exclusionMask { get; set; }
    }
}