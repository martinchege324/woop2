using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_history
    {
        [Key] public int sub_history_id { get; set; }

        [StringLength(250)] public string sub_history_desc { get; set; }

        public int? history_id { get; set; }

        public int? center_id { get; set; }
    }
}