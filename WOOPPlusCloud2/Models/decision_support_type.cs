using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class decision_support_type
    {
        [Key] public int decision_support_type_id { get; set; }

        [Required] [StringLength(50)] public string decision_support_type_desc { get; set; }

        public int? decision_support_categ_id { get; set; }
    }
}