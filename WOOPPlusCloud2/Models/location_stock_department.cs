using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class location_stock_department
    {
        [Key] public Guid location_stock_department_id { get; set; }

        [StringLength(250)] public string location_stock_department_description { get; set; }

        public int? center_id { get; set; }

        public DateTime? created_date { get; set; }

        public int? created_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? updated_by { get; set; }

        [StringLength(1)] public string active { get; set; }
    }
}