using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class department_shift
    {
        [Key] public Guid department_shift_id { get; set; }

        public Guid? hr_department_id { get; set; }

        [StringLength(250)] public string department_shift_desc { get; set; }

        public DateTime? start_time { get; set; }

        public DateTime? end_time { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_on { get; set; }

        [StringLength(1)] public string active { get; set; }
    }
}