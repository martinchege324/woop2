using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    [Table("preoperative")]
    public class preoperative
    {
        [Key] public int preoperative_id { get; set; }

        [StringLength(250)] public string preoperative_name { get; set; }

        public int? preoperative_category_id { get; set; }
    }
}