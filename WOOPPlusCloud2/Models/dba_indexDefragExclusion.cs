using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class dba_indexDefragExclusion
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int databaseID { get; set; }

        [Required] [StringLength(128)] public string databaseName { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int objectID { get; set; }

        [Required] [StringLength(128)] public string objectName { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int indexID { get; set; }

        [Required] [StringLength(128)] public string indexName { get; set; }

        public int exclusionMask { get; set; }
    }
}