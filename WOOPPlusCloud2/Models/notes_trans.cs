using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class notes_trans
    {
        [Key] public Guid notes_id { get; set; }

        public string notes_desc { get; set; }

        public DateTime? notes_date { get; set; }

        public Guid? cycle_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        [StringLength(1)] public string kardex_notes { get; set; }

        [StringLength(1)] public string pnote { get; set; }

        public int? isdeleted { get; set; }
    }
}