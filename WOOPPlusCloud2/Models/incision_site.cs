using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class incision_site
    {
        [Key] public int incision_site_id { get; set; }

        [StringLength(250)] public string incision_site_desc { get; set; }
    }
}