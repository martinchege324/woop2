using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class line_of_service_trans
    {
        [Key] public Guid line_of_service_trans_id { get; set; }

        public Guid? cycle_id { get; set; }

        public int? line_of_service_id { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public DateTime? created_date { get; set; }

        [StringLength(1)] public string completed { get; set; }

        public DateTime? case_start_time { get; set; }

        public DateTime? case_end_time { get; set; }

        public DateTime? case_restart_starttime { get; set; }

        public DateTime? case_restart_endtime { get; set; }

        public Guid? department_id { get; set; }

        public int? case_started_by { get; set; }

        public int? case_restarted_by { get; set; }

        public int? case_closed_by { get; set; }

        public int? case_reclosed_by { get; set; }

        public Guid? case_type_id { get; set; }

        public int? service_type_id { get; set; }
    }
}