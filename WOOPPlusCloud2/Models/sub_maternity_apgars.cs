using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sub_maternity_apgars
    {
        [Key] public int sub_maternity_apgars_id { get; set; }

        [StringLength(250)] public string sub_maternity_apgars_desc { get; set; }

        public int? maternity_apgars_id { get; set; }
    }
}