using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class insurance_types
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int insurance_type_id { get; set; }

        public string insurance_type_name { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }
    }
}