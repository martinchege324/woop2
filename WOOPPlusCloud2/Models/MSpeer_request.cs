using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class MSpeer_request
    {
        [Key] [Column(Order = 0)] public int id { get; set; }

        [Key] [Column(Order = 1)] public string publication { get; set; }

        public DateTime? sent_date { get; set; }

        [StringLength(4000)] public string description { get; set; }
    }
}