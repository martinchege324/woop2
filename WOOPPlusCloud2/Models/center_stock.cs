using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class center_stock
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public center_stock()
        {
            trans_file = new HashSet<trans_file>();
        }

        [Key] public Guid center_product_id { get; set; }

        [Required] [StringLength(3)] public string centre_Uom_code { get; set; }

        public int? product_id { get; set; }

        public double? center_product_quantity { get; set; }

        public double? center_product_max_quantity { get; set; }

        public double? center_product_min_quantity { get; set; }

        public int center_id { get; set; }

        public double? center_product_reorder_level { get; set; }

        [Required] [StringLength(50)] public string product_code { get; set; }

        [StringLength(250)] public string center_product_description { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        public decimal? product_price { get; set; }

        public int? updatedby_staff_id { get; set; }

        public DateTime? update_date { get; set; }

        public decimal? cost_price { get; set; }

        public decimal? margin { get; set; }

        [StringLength(1)] public string vatcode { get; set; }

        public Guid? location_stock_department_id { get; set; }

        [Column(TypeName = "numeric")] public decimal? physical_count { get; set; }

        public int? physical_count_by { get; set; }

        public DateTime? physical_count_date { get; set; }

        public string status { get; set; }

        public decimal? corporate_price { get; set; }

        public int? active { get; set; }

        public decimal? regular_retail_price { get; set; }

        public decimal? other_price { get; set; }

        public Guid? supplier_id { get; set; }

        [StringLength(250)] public string dose { get; set; }

        public decimal? margincorporate { get; set; }

        public int? strength { get; set; }

        public int? pack_size { get; set; }

        public decimal? marginhospital { get; set; }

        public decimal? marginretail { get; set; }

        public int? stocktake { get; set; }

        public decimal? trade_price { get; set; }

        public decimal? pack_cash_price { get; set; }

        public decimal? park_corporate_price { get; set; }

        public int? isvatable { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trans_file> trans_file { get; set; }
    }
}