using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class suppliers_category
    {
        [Key] public int supplier_category_id { get; set; }

        public string supplier_category_desc { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? active { get; set; }
    }
}