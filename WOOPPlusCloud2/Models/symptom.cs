using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class symptom
    {
        [Key] public Guid symptoms_id { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        public Guid? cycle_id { get; set; }

        public int? sub_patient_symptom_id { get; set; }

        public int? patient_symptom_id { get; set; }

        [StringLength(50)] public string value { get; set; }

        public int? center_id { get; set; }

        public int? staff_id { get; set; }

        public DateTime? date { get; set; }

        [StringLength(50)] public string duration { get; set; }

        [StringLength(250)] public string comment { get; set; }

        [StringLength(1)] public string chief_complaint { get; set; }
    }
}