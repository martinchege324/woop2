using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class drug_admin
    {
        [Key] public Guid drug_admin_id { get; set; }

        public Guid? trans_id { get; set; }

        public int? center_id { get; set; }

        public Guid? cycle_id { get; set; }

        public DateTime? date_given { get; set; }

        public int? given_by { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_on { get; set; }

        public DateTime? edited_on { get; set; }

        public int? edited_by { get; set; }

        public int? active { get; set; }

        public decimal? quantity { get; set; }

        public decimal? balance { get; set; }

        public string comment { get; set; }
    }
}