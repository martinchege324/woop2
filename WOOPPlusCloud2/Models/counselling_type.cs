using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class counselling_type
    {
        [Key] public int counselling_type_id { get; set; }

        [StringLength(250)] public string counselling_type_description { get; set; }

        public int? counselling_category_id { get; set; }
    }
}