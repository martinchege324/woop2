using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class referral_categories
    {
        [Key] public Guid referral_category_id { get; set; }

        public string referral_category_desc { get; set; }

        public int? created_by { get; set; }

        public DateTime created_on { get; set; }

        public int? edited_by { get; set; }

        public DateTime? edited_on { get; set; }

        public int? center_id { get; set; }
    }
}