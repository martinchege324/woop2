using System;
using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class sms_settings
    {
        [Key] public int sms_setting_id { get; set; }

        [StringLength(50)] public string username { get; set; }

        [StringLength(250)] public string apikey { get; set; }

        public DateTime? created_date { get; set; }

        [StringLength(50)] public string created_by { get; set; }

        public DateTime? edited_date { get; set; }

        [StringLength(50)] public string edited_by { get; set; }
    }
}