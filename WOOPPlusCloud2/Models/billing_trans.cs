using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOOPPlusCloud2.Models
{
    public class billing_trans
    {
        [Key] public int billing_trans_id { get; set; }

        [Column(TypeName = "numeric")] public decimal? billing_trans_amount { get; set; }

        public int? center_id { get; set; }

        public int? billing_type_id { get; set; }

        [Required] [StringLength(250)] public string billing_trans_comment { get; set; }

        public int? created_by { get; set; }

        public DateTime? created_date { get; set; }

        public int? updated_by { get; set; }

        public DateTime? updated_date { get; set; }

        public int? points { get; set; }

        [Column(TypeName = "numeric")] public decimal? per_patient_amount { get; set; }

        [StringLength(1)] public string client_notified { get; set; }

        [StringLength(1)] public string center_updated { get; set; }

        public int? center_updated_by { get; set; }

        public DateTime? center_updated_on { get; set; }

        [StringLength(250)] public string trans_reference { get; set; }
    }
}