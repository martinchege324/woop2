using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace WOOPPlusCloud2.Models
{
    public class stock_f
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public stock_f()
        {
            trans_file = new HashSet<trans_file>();
            treatment_diagnosis = new HashSet<treatment_diagnosis>();
        }

        [Key] public int product_id { get; set; }

        [Required] [StringLength(250)] public string product_short_desc { get; set; }

        [StringLength(250)] public string product_long_Desc { get; set; }

        [Required] [StringLength(3)] public string Uom_code { get; set; }

        public double? product_quantity { get; set; }

        public double? product_max_quantity { get; set; }

        public double? product_min_quantity { get; set; }

        public double? product_reorder_level { get; set; }

        public int category_id { get; set; }

        public int center_id { get; set; }

        [Required] [StringLength(20)] public string product_scancode { get; set; }

        [Required] [StringLength(10)] public string product_code { get; set; }

        [Column(TypeName = "numeric")] public decimal? unit_price { get; set; }

        [Required] [StringLength(3)] public string suom_code { get; set; }

        [Required] [StringLength(1)] public string updated { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trans_file> trans_file { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<treatment_diagnosis> treatment_diagnosis { get; set; }
    }
}