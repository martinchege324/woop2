using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class test_diagnosis
    {
        [Key] public int test_diagnosis_id { get; set; }

        public int? lab_test_id { get; set; }

        public int? disease_id { get; set; }
    }
}