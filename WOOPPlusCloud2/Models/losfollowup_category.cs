using System.ComponentModel.DataAnnotations;

namespace WOOPPlusCloud2.Models
{
    public class losfollowup_category
    {
        [Key] public int losfollowup_category_id { get; set; }

        [StringLength(250)] public string losfollowup_category_desc { get; set; }

        public int? losfollowup_type_id { get; set; }

        public int? center_id { get; set; }
    }
}